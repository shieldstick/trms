<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AccessLevel extends Model
{
    protected $fillable = [
        'title',
    ];

    public static function administrator()
    {
    	return static::where(['title' => 'Administrator'])->first();
    }

    public static function developer()
    {
    	return static::where(['title' => 'Developer'])->first();
    }

    public static function qa()
    {
    	return static::where(['title' => 'QA'])->first();
    }

    public static function requestor()
    {
    	return static::where(['title' => 'Requestor'])->first();
    }
    
}
