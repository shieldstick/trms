<?php

namespace App;

use App\Filters\QueryFilter;
use App\Traits\RecordsActivity;
use Illuminate\Database\Eloquent\Model;
use App\Notifications\TaskHasBeenAssigned;

class Task extends Model
{
    use RecordsActivity;

    protected $guarded = [];

    protected $dates = ['due_date'];

    protected $with = ['assignee', 'creator'];

    protected static function boot()
    {
        parent::boot();

        static::created(function($task) {
            $task->recordActivity($task->ticket_id, 'created');
            $task->assignee->notify(new TaskHasBeenAssigned($task));
        });
    }

    public function scopeFilter($query, QueryFilter $filters)
    {
        return $filters->apply($query);
    }

    public function ticket()
    {
    	return $this->belongsTo('App\Ticket');
    }

    public function assignee()
    {
    	return $this->belongsTo('App\User');
    }

    public function creator()
    {
    	return $this->belongsTo('App\User');
    }
    
}
