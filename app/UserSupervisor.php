<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserSupervisor extends Model
{
    protected $guarded = [];
}
