<?php

namespace App\Events;

use App\Attachment;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;

class CreatedAttachment
{
    use Dispatchable, SerializesModels;

    public $attachment;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Attachment $attachment)
    {
        $this->attachment = $attachment;
    }

}
