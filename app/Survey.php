<?php

namespace App;

use App\Notifications\SurveyWasCreated;
use Illuminate\Database\Eloquent\Model;

class Survey extends Model
{
    protected $guarded = [];

    protected $with = ['ticket', 'owner'];

    protected static function boot()
    {
        parent::boot();

        static::created(function($survey) {
            $survey->owner->notify(new SurveyWasCreated($survey));
        });
    }

    public function ticket()
    {
    	return $this->belongsTo('App\Ticket');
    }

    public function owner()
    {
    	return $this->belongsTo('App\User', 'user_id')->withTrashed();
    }
    
}
