<?php

namespace App\Filters;

use App\Status;
use App\Filters\QueryFilter;

class TicketFilters extends QueryFilter
{

	public function developer($id)
	{
		return $this->builder->where('developer_id', $id);
	}

	public function qa($id)
	{
		return $this->builder->where('qa_id', $id);
	}

	public function creator($id)
	{
		return $this->builder->where('creator_id', $id);
	}

	public function requester($id)
	{
		return $this->builder->where('requester_id', $id);
	}

	public function client($id)
	{
		return $this->builder->where('client_id', $id);
	}

	public function module($id)
	{
		return $this->builder->where('module_id', $id);
	}

	public function status($id)
	{
		return $this->builder->where('status_id', $id);
	}

	public function priority($id)
	{
		return $this->builder->where('priority_id', $id);
	}

	public function open($open = true)
	{
		return $this->builder->where('status_id', '!=', Status::resolved()->id)
							->Where('status_id', '!=', Status::cancelled()->id)
							->orderBy('created_at', 'desc');
	}

	public function resolved($resolved = true)
	{
		return $this->builder->where('status_id', '=', Status::resolved()->id)
							->orWhere('status_id', '=', Status::cancelled()->id)
							->orderBy('created_at', 'desc');
	}


	public function involved($id)
	{
		return $this->builder->where('requester_id', $id)
							 ->orWhere('creator_id', $id)
							 ->orWhere('developer_id', $id)
							 ->orWhere('qa_id', $id);
	}
	
	
}