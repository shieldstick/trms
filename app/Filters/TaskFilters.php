<?php

namespace App\Filters;

use App\Filters\QueryFilter;

class TaskFilters extends QueryFilter
{

	public function assigned($assigned = true)
	{
		return $this->builder->where('assignee_id', '=', auth()->id());
	}

	public function delegated($delegated = true)
	{
		return $this->builder->where('creator_id', '=', auth()->id());
	}
	
}