<?php

namespace App\Traits;

use App\Attachment;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

trait HasAttachments
{
	public function attachments()
    {
        return $this->morphMany(Attachment::class, 'attachable')
                        ->orderBy('created_at', 'desc');
    }

    /**
     * Persist attachments to db and public storage
     * Refer to config/filesystems.php for
     * list of available file drivers
     *
     * @param Array $attachments
     * @return  void
     */
    public function addAttachments(Array $attachments)
    {
    	foreach($attachments as $file)
    	{
			$this->disk()->put('/attachments/'.$file->hashName(), File::get($file));

	        $this->attachments()->create([
                    'file_name'         => $file->getClientOriginalName(),
	        		'hash_name'			=> $file->hashName(),
	        		'extension'	        => $file->extension(),
                    'storage_path'      => (new Attachment)->storagePath(),
	        		'creator_id'		=> auth()->id(),
	        	]);
    	}
    }

    protected function disk()
    {
        return Storage::disk('public');
    }

}