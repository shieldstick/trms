<?php

namespace App\Traits;

trait HasCollaborators
{
    public function creator()
    {
        return $this->belongsTo('App\User', 'creator_id')->withTrashed();
    }
	
    public function requester()
    {
    	return $this->belongsTo('App\User', 'requester_id')->withTrashed();
    }

    public function supervisor()
    {
    	return $this->belongsTo('App\User', 'supervisor_id')->withTrashed();
    }

    public function developer()
    {
    	return $this->belongsTo('App\User', 'developer_id')->withTrashed();
    }

    public function qa()
    {
    	return $this->belongsTo('App\User', 'qa_id')->withTrashed();
    }


}