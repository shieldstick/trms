<?php

namespace App\Traits;

use App\User;
use App\Status;
use App\Ticket;
use Illuminate\Support\Facades\Auth;

trait ChecksForSurvey
{

	public function checkForSurvey(Ticket $ticket)
	{
		if(!$this->checkIfResolved($ticket)) {
			return false;
		}

		return $this->needsSurvey($ticket, Auth::user());
	}

    public function needsSurvey(Ticket $ticket, User $user)
    {
        if(!$ticket->surveyable($user)) {
            return false;
        }

        return $this->hasSurvey($ticket, $user);
    }


    public function hasSurvey(Ticket $ticket, User $user)
    {
    	if($ticket->surveyExist(auth()->user())) {
            return false;
        }

        return redirect('/tickets/'.$ticket->id.'/surveys/create');
    }

    public function checkIfResolved(Ticket $ticket)
    {
        return $ticket->status_id !== Status::resolved()->id;
    }
    
}