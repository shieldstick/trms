<?php

namespace App\Traits;

use App\TicketSubscription;
use App\Notifications\TicketWasCreated;

trait Subscribable 
{
	/**
     * Get subscriptions relationship
     *
     * @return Illuminate\Database\Eloquent\Model
     */
    public function subscriptions()
    {
        return $this->hasMany(TicketSubscription::class);
    }

    /**
     * Subscribe all collaborators to the ticket
     *
     * @return $this
     */


    /**
     * Subscribe a user to a ticket
     *
     * @param  int $userId
     * @return $this
     */
    public function subscribe($userId)
    {
        if(!$this->isSubscribedBy($userId)) {
            return $this->subscriptions()->create([
                'user_id'   => $userId,
            ]);
        }
        return $this;
    }

    /**
     * Check if user is subscribed to the ticket
     *
     * @param  int  $userId
     * @return boolean
     */
    public function isSubscribedBy($userId)
    {
        return !! $this->subscriptions()->where('user_id', $userId)->first();
    }

    /**
     * Notify subscribers of this ticket.
     *
     * @return $this
     */
    // public function notifySubscribers($notification)
    // {
    //     foreach($this->subscriptions as $subscription) {
    //         if(auth()->user()->id != $subscription->user->id) {
    //             $subscription->user->notify(new $notification($subscription->user, $this));
    //         }
    //     }
    //     // return $this;
    // }
}