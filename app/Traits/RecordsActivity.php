<?php

namespace App\Traits;

trait RecordsActivity
{

    public function activities()
    {
        return $this->morphMany('App\Activity', 'subject');
    }

	public function recordActivity($ticket_id, $event, $before = null, $after = null)
    {
        $this->activities()->create([
            'user_id'       =>  auth()->id(),
            'ticket_id'     =>  $ticket_id,
            'type'          =>  $this->getActivityType($event),
            'before'        =>  $before,
            'after'         =>  $after,
        ]);
    }


    protected function getActivityType($event)
    {
        $type = strtolower((new \ReflectionClass($this))->getShortName());

        return "{$event}_{$type}";
    }
}