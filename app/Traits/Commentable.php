<?php

namespace App\Traits;

use App\Comment;

trait Commentable
{
	public function comments()
    {
        return $this->morphMany(Comment::class, 'commentable')
                    ->orderBy('created_at', 'asc');
    }

    public function addComment($comment)
    {
        return $this->comments()->create($comment);
    }

    public function hasComments()
    {
    	return !! $this->comments->first();
    }

}