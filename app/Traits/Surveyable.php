<?php

namespace App\Traits;

use App\User;

trait Surveyable
{
    public function surveyExist($user)
    {
        return $this->surveys()->where('user_id', $user->id)->exists();
    }

    public function surveyable($user)
    {
        return $this->requester_id == $user->id || $this->supervisor_id == $user->id;
    }

    public function createSurvey(User $user)
    {
        return $this->surveys()->create([
                'ticket_id' =>  $this->id,
                'user_id'   =>  $user->id, 
                'completed' =>  false,
            ]);
    }
}