<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Ticket'    =>  'App\Policies\TicketPolicy',
        // 'App\Ticket'    =>  'App\Policies\SubTicketPolicy',
        'App\User'    =>  'App\Policies\UserPolicy',
        'App\Comment'    =>  'App\Policies\CommentPolicy',
        'App\Survey'    =>  'App\Policies\SurveyPolicy',
        // 'App\Task'    =>  'App\Policies\TaskPolicy',
        
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
    }
}
