<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\CreatedTicket' => [
            'App\Listeners\SubscribeTicketCollaborators',
            'App\Listeners\SendTicketWasCreatedNotification',
            'App\Listeners\RecordCreatedTicketActivity',
        ],
        'App\Events\UpdatedTicket' => [

            // Developer assignment listeners
            'App\Listeners\SubscribeAssignedDeveloper',
            'App\Listeners\SendDeveloperWasAssignedNotification',
            'App\Listeners\RecordUpdatedTicketDeveloperActivity',

            // QA assignment listeners
            'App\Listeners\SubscribeAssignedQA',
            'App\Listeners\SendQAWasAssignedNotification',
            'App\Listeners\RecordUpdatedTicketQaActivity',
            
            // Status change listeners
            'App\Listeners\TicketStatusSetToResolved',

            // Priority change listeners
            'App\Listeners\RecordUpdatedTicketPriorityActivity',

            // Request type change listeners
            'App\Listeners\RecordUpdatedTicketRequestTypeActivity',

            // Module change listeners
            'App\Listeners\RecordUpdatedTicketModuleActivity',

            // Target date change listeners
            'App\Listeners\RecordUpdatedTicketTargetDateActivity',

            // Description change listeners
            'App\Listeners\RecordUpdatedTicketDescriptionActivity',

            // Dev dates change listeners
            'App\Listeners\RecordUpdatedTicketDevStartDateActivity',
            'App\Listeners\RecordUpdatedTicketDevCompletionDateActivity',

            // QA dates change listeners
            'App\Listeners\RecordUpdatedTicketQaStartDateActivity',
            'App\Listeners\RecordUpdatedTicketQaCompletionDateActivity',

            // Notifications
            'App\Listeners\SendTicketStatusWasUpdatedNotification',
            'App\Listeners\SendTicketWasEndorsedToRequesterNotification',

            // Record activities listeners
            'App\Listeners\RecordUpdatedTicketStatusActivity',
        ],

        'App\Events\CreatedComment' => [
            'App\Listeners\RecordCreatedCommentActivity',
            'App\Listeners\SendCommentHasBeenCreatedNotification',
        ],

        'App\Events\UpdatedComment' => [
            'App\Listeners\RecordUpdatedCommentActivity',
        ],

        'App\Events\CreatedAttachment' => [
            'App\Listeners\RecordCreatedTicketAttachmentActivity',
        ],

        'App\Events\DeletedAttachment' => [
            'App\Listeners\RecordDeletedTicketAttachmentActivity',
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
