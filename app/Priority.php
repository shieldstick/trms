<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Priority extends Model
{
    protected $guarded = [];


    public static function normal()
    {
    	return self::where(['title' => 'Normal'])->first();
    }

    public static function high()
    {
    	return self::where(['title' => 'High'])->first();
    }

    public static function quickFix()
    {
    	return self::where(['title' => 'Quick Fix'])->first();
    }

    public static function priorityZero()
    {
    	return self::where(['title' => 'Priority 0'])->first();
    }
    
}
