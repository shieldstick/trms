<?php

namespace App;

use App\Traits\RecordsActivity;
use App\Events\CreatedAttachment;
use App\Events\DeletedAttachment;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Attachment extends Model
{
    use RecordsActivity, SoftDeletes;
    
    protected $guarded = [];

    protected $dates = ['created_at','updated_at','deleted_at'];

    protected $with = ['creator'];

    protected static function boot()
    {
        parent::boot();

        static::created(function($attachment){
            event(new CreatedAttachment($attachment));
        });

        static::deleted(function($attachment){
            event(new DeletedAttachment($attachment));
        });
    }

    public function storagePath()
    {
        return storage_path('app/public') . '/attachments';
    }

    public function creator()
    {
    	return $this->belongsTo('App\User', 'creator_id')->withTrashed();
    }

    public function attachable()
    {
        return $this->morphTo();
    }
}
