<?php

namespace App;

use Illuminate\Support\Facades\Auth;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Notifications\ResetPasswordNotification;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable, SoftDeletes;

    protected $fillable = ['first_name', 'last_name', 'email', 'password',  'access_level', 'user_group', 'timezone'];

    protected $hidden = ['password', 'remember_token'];

    protected $with = ['accessLevel', 'userGroup'];

    protected $dates = ['created_at','updated_at','deleted_at'];

    protected $appends = ['full_name'];

    /**
     * Send the password reset notification.
     * Overrides laravel default
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token, $this));
    }

    public function getFullNameAttribute()
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    public function accessLevel()
    {
    	return $this->belongsTo(AccessLevel::class, 'access_level');
    }

    public function userGroup()
    {
    	return $this->belongsTo(UserGroup::class, 'user_group');
    }

    public function activity()
    {
        return $this->hasMany('App\Activity');
    }

    public function isAdministrator()
    {
        return $this->accessLevel->title == 'Administrator' ? true : false;
    }

    public function isRequestor()
    {
        return $this->accessLevel->title == 'Requestor' ? true : false;
    }

    public function developers()
    {
        return User::all()->filter(function($user){
            return $user->accessLevel->title == 'Developer' && 
                   $user->userGroup->title == 'IT';
        });
    }

    public function qas()
    {
        return User::all()->filter(function($user){
            return $user->accessLevel->title == 'QA' && 
                   $user->userGroup->title == 'IT';
        });
    }

    /*
     * Tasks are deprecated
     * 
     * public function assignedTask()
     * {
     *     return $this->hasMany('App\Task', 'assignee_id');
     * }

     * public function createdTask()
     * {
     *     return $this->hasMany('App\Task', 'creator_id');
     * }
     */
    
}
