<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class TicketHasBeenResolved extends Notification implements ShouldQueue
{
    private $ticket;

    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($ticket)
    {
        $this->ticket = $ticket;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [
                'mail',
                // 'database'
                ];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $url = is_null($this->ticket->ticket_id) 
                ? url('/tickets/' . $this->ticket->id)
                : url('/sub-tickets/' . $this->ticket->id);

        return (new MailMessage)
                    ->subject('Ticket #' . $this->ticket->identification . ' has been marked as resolved!')
                    ->greeting('Hey ' . $notifiable->first_name . '!')
                    ->line('The status of "' . $this->ticket->subject . '" has been set to resolved! Please click on button below to answer our ticket survey..')
                    ->action('View Ticket', $url)
                    ->line('Thank you for using TRMS!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'message'           => 'Ticket#' . sprintf("%06s", $this->ticket->id) . ' has been resolved!',
            'ticketUrl'         => is_null($this->ticket->ticket_id)
                                   ? '/tickets/' . $this->ticket->id
                                   : '/sub-tickets/' . $this->ticket->id,
            'notificationUrl'   => '/users/'  .$notifiable->id . '/notifications',
        ];
    }
}
