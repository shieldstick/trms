<?php

namespace App\Notifications;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use App\Notifications\Interfaces\MailNotificationInterface;

class TicketWasCreated extends Notification implements ShouldQueue
{
    use Queueable;

    private $ticket;
    
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($ticket)
    {
        $this->ticket = $ticket;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [
            'mail', 
            'database',
            ];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $url = is_null($this->ticket->ticket_id) 
                ? url('/tickets/' . $this->ticket->id)
                : url('/sub-tickets/' . $this->ticket->id);

        return (new MailMessage)
                    ->subject('New TRMS Ticket - Ticket #' . $this->ticket->identification . ' - ' . $this->ticket->priority->title)
                    ->greeting('Hey ' . $notifiable->first_name . '!')
                    ->line('You have been added to a ticket. Click on the button below to view it.')
                    ->line('Subject: ' . $this->ticket->subject)
                    ->line('Details: ' . $this->ticket->description)
                    ->action('View Ticket', $url)
                    ->line('Thank you for using TRMS!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'message'           => 'Ticket #' . $this->ticket->identification . ' has been created!',
            'ticketUrl'         => is_null($this->ticket->ticket_id)
                                   ? '/tickets/'.$this->ticket->id
                                   : '/sub-tickets/'.$this->ticket->id,
            'notificationUrl'   => '/users/' . $notifiable->id . '/notifications',
        ];
    }
}
