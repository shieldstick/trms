<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class TicketWasEndorsedToRequester extends Notification implements ShouldQueue
{
    use Queueable;

    private $ticket;
    
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($ticket)
    {
        $this->ticket = $ticket;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [
                'mail',
                // 'database',
                ];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $url = is_null($this->ticket->ticket_id) 
                ? url('/tickets/' . $this->ticket->id)
                : url('/sub-tickets/' . $this->ticket->id);

        return (new MailMessage)
                    ->subject('Ticket #' . $this->ticket->identification .' is now endorsed to requester!')
                    ->greeting('Hey ' . $notifiable->first_name . '!')
                    ->line('The status of "'. $this->ticket->subject . '" has been set to endorsed to requester! Please click on button below to follow through.')
                    ->line('Please set the status to "Needs Rework" if you think the we missed something,')
                    ->line('otherwise set the status to "For Deployment" so we can deploy to our live server.')
                    ->action('View Ticket', $url)
                    ->line('Thank you for using TRMS!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
