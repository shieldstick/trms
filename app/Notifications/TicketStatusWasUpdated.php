<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class TicketStatusWasUpdated extends Notification implements ShouldQueue
{
    private $ticket;

    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($ticket)
    {
        $this->ticket = $ticket;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [
                'mail',
                'database',
                ];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $url = is_null($this->ticket->ticket_id) 
                ? url('/tickets/' . $this->ticket->id)
                : url('/sub-tickets/' . $this->ticket->id);

        return (new MailMessage)
                    ->subject('Ticket #' . $this->ticket->identification .' status has been updated')
                    ->greeting('Hey ' . $notifiable->first_name . '!')
                    ->line('The status of "'. $this->ticket->subject . '" has been set to "' . $this->ticket->status->title .'". Click on the button below to view it.')
                    ->action('View Ticket', $url)
                    ->line('Thank you for using TRMS!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'message'           => 'Ticket #' . $this->ticket->identification .' status has been set to ' . $this->ticket->status->title,
            'ticketUrl'         => is_null($this->ticket->ticket_id)
                                   ? '/tickets/'.$this->ticket->id
                                   : '/sub-tickets/'.$this->ticket->id,
            'notificationUrl'   => '/users/' . $notifiable->id . '/notifications',
        ];
    }
}
