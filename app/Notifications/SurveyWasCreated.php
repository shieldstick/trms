<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class SurveyWasCreated extends Notification implements ShouldQueue
{
    private $survey;

    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($survey)
    {
        $this->survey = $survey;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [
                'mail',
                // 'database'
                ];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $url = is_null($this->ticket->ticket_id) 
                ? url('/tickets/' . $this->ticket->id)
                : url('/sub-tickets/' . $this->ticket->id);

        return (new MailMessage)
                    ->subject('Please answer a quick survey for ticket # ' . $this->survey->ticket->identification .'!')
                    ->greeting('Hey ' . $notifiable->first_name . '!')
                    ->line('A survey in Ticket# ' . $this->survey->ticket->identification . ' has been prepared for you. Please click on the link below to answer it.')
                    ->action('View Ticket', $url)
                    ->line('Thank you for using TRMS!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
