<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use App\Notifications\Interfaces\MailNotificationInterface;

class MadeAComment extends Notification implements ShouldQueue
{
    private $ticket;
    private $user;

    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($user, $ticket, $comment)
    {
        $this->ticket = $ticket;
        $this->user = $user;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [
                // 'mail',
                'database',
                ];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $url = is_null($this->ticket->ticket_id) 
                ? url('/tickets/' . $this->ticket->id)
                : url('/sub-tickets/' . $this->ticket->id);

        return (new MailMessage)
                    ->subject($this->user->first_name . ' made a comment to Ticket # ' . $this->ticket->identification)
                    ->greeting('Hey ' . $notifiable->first_name . '!')
                    ->line('A comment has been made to '. $this->ticket->subject . '. Click on the button below to view it.')
                    ->action('View Ticket', $url)
                    ->line('Thank you for using TRMS!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'message'           => $this->user->first_name . ' made a comment to Ticket # ' . $this->ticket->identification,
            'ticketUrl'         => is_null($this->ticket->ticket_id)
                                   ? '/tickets/'.$this->ticket->id
                                   : '/sub-tickets/'.$this->ticket->id,
            'notificationUrl'   => '/users/' . $notifiable->id.'/notifications',
        ];
    }
}
