<?php

namespace App;

use App\Traits\HasAttachments;
use Illuminate\Database\Eloquent\Model;

class IncidentReport extends Model
{
    use HasAttachments;

    protected $guarded = [];

    protected $dates = ['target_date', 'incident_date', 'created_at', 'updated_at'];


    public function creator()
    {
    	return $this->belongsTo('App\User', 'creator_id');
    }
    
    public function requester()
    {
    	return $this->belongsTo('App\User', 'requester_id');
    }

    public function supervisor()
    {
        return $this->belongsTo('App\User', 'supervisor_id');
    }
    
}
