<?php

namespace App;

use Carbon\Carbon;

class TargetDate 
{
	public static function make($priority)
	{
		switch($priority) {
            case 1:
                return null;
            break;

            case 2:
                return $request->has('target_date') ? strtotime($request->target_date) : null;
            break;

            default:
                return Carbon::now();
        }
	}
	
}