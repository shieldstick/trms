<?php

namespace App;

use App\Traits\Surveyable;
use App\Traits\Commentable;
use App\Filters\QueryFilter;
use App\Traits\Subscribable;
use App\Events\CreatedTicket;
use App\Events\UpdatedTicket;
use App\Traits\HasAttachments;
use App\Traits\RecordsActivity;
use App\Traits\HasCollaborators;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Ticket extends Model
{
    use Commentable, HasAttachments,  Surveyable, SoftDeletes, 
        HasCollaborators, Subscribable, RecordsActivity;

    protected $dates = ['target_date', 'created_at', 'updated_at', 'dev_start_date',  
                        'dev_completion_date', 'qa_start_date', 'qa_completion_date',
                        'first_developer_assignment_at',
                        'resolved_date'];

    protected $with = [ 'creator', 'requester', 'developer', 'qa','client', 'module', 
                        'priority', 'status', 'requestType', 'children'];

    protected $appends = ['identification', 'children_count', 
                          'first_developer_assignment', 'first_developer_assignment_by', 'first_developer_assignment_at',
                          'first_qa_assignment', 'first_qa_assignment_by', 'first_qa_assignment_at'];
                        
    protected $guarded = [];

    protected static function boot()
    {
        parent::boot();

        static::created(function($ticket) {
            event(new CreatedTicket($ticket));
        });

        static::updated(function($ticket) {
            event(new UpdatedTicket($ticket));
        });

        static::addGlobalScope('comments', function($builder){
            $builder->with('comments');
        });

        static::addGlobalScope('attachments', function($builder){
            $builder->with('attachments');
        });

        static::addGlobalScope('activity', function($builder){
            $builder->with('activity');
        });
    }

    public function getIdentificationAttribute()
    {
        return !is_null($this->ticket_id)
               ? sprintf("%06s", $this->ticket_id) . ' - ' . $this->number
               : sprintf("%06s", $this->id);
    }

    public function getChildrenCountAttribute()
    {
        return $this->children->count();
    }

    public function getFirstDeveloperAssignmentAttribute()
    {
        return $this->getFirstDeveloperAssignment() 
            ? $this->getFirstDeveloperAssignment()->after
            : null;
    }

    public function getFirstDeveloperAssignmentByAttribute()
    {
        return $this->getFirstDeveloperAssignment() 
            ? $this->getFirstDeveloperAssignment()->creator->full_name
            : null;
    }

    public function getFirstDeveloperAssignmentAtAttribute()
    {
        return $this->getFirstDeveloperAssignment() 
            ? $this->getFirstDeveloperAssignment()->created_at
            : null;
    }
    

    public function getFirstDeveloperAssignment()
    {
        return ($this->activity) 
                ? $this->activity
                ->where('type', 'updated_developer_ticket')
                ->last()
                : null;
    }

    public function getFirstQaAssignmentAttribute()
    {
        return $this->getFirstQaAssignment() 
            ? $this->getFirstQaAssignment()->after
            : null;
    }

    public function getFirstQaAssignmentByAttribute()
    {
        return $this->getFirstQaAssignment() 
            ? $this->getFirstQaAssignment()->creator->full_name
            : null;
    }

    public function getFirstQaAssignmentAtAttribute()
    {
        return $this->getFirstQaAssignment() 
            ? $this->getFirstQaAssignment()->created_at
            : null;
    }

    public function getFirstQaAssignment()
    {
        return ($this->activity) 
                ? $this->activity
                ->where('type', 'updated_qa_ticket')
                ->last()
                : null;
    }

    public function scopeFilter($query, QueryFilter $filters)
    {
        return $filters->apply($query);
    }

    public function parent()
    {
        return $this->belongsTo('App\Ticket', 'ticket_id');
    }

    public function children()
    {
        return $this->hasMany('App\Ticket', 'ticket_id')
                        ->withoutGlobalScopes()
                        ->orderBy('number', 'asc');
    }

    public function client()
    {
    	return $this->belongsTo('App\Client', 'client_id');
    }

    public function module()
    {
    	return $this->belongsTo('App\Module', 'module_id');
    }

    public function priority()
    {
    	return $this->belongsTo('App\Priority', 'priority_id');
    }

    public function status()
    {
    	return $this->belongsTo(Status::class, 'status_id');
    }

    public function requestType()
    {
        return $this->belongsTo(RequestType::class, 'request_type_id')
                    ->withTrashed();
    }

    public function activity()
    {
        return $this->hasMany('App\Activity')
                    ->orderBy('created_at', 'desc');
    }

    public function surveys()
    {
        return $this->hasMany(Survey::class, 'ticket_id');
    }

}
