<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserGroup extends Model
{
    protected $guarded = [];


    public static function cs()
    {
    	return static::where('title','CS')->first();
    }

    public static function it()
    {
    	return static::where('title','IT')->first();
    }

    public static function marketing()
    {
    	return static::where('title','Marketing')->first();
    }

    public static function oneNow()
    {
    	return static::where('title','OneNow')->first();
    }
    
}
