<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RequestType extends Model
{
	use SoftDeletes;
	
    protected $guarded = [];

    protected $dates = ['deleted_at'];

    public static function newFeature()
    {
    	return self::where(['title' => 'New Feature'])->first();
    }

    public static function maintenance()
    {
    	return self::where(['title' => 'Maintenance'])->first();
    }
}
