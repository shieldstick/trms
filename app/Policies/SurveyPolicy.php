<?php

namespace App\Policies;

use App\User;
use App\Survey;
use Illuminate\Auth\Access\HandlesAuthorization;

class SurveyPolicy
{
    use HandlesAuthorization;


    public function before($user, $ability)
    {
        if($user->isAdministrator() && ($ability != 'update')) {
            return true;
        }
    }
    
    public function update(User $user, Survey $survey)
    {
        return $user->id == $survey->user_id;
    }

    public function view(User $user, Survey $survey)
    {
    	return $user->id == $survey->user_id;
    }
}
