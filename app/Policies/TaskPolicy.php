<?php

namespace App\Policies;

use App\Task;
use App\User;
use App\UserGroup;
use Illuminate\Auth\Access\HandlesAuthorization;

class TaskPolicy
{
    use HandlesAuthorization;

    
    public function view(User $user, Task $task)
    {
        return $user->id == $task->assignee_id ||
                $user->id == $task->creator_id;
    }


    public function create(User $user, Task $task)
    {
        return $user->userGroup->title == 'IT';
    }
    

    public function update(User $user, Task $task)
    {
    	return $user->id == $task->assignee_id ||
                $user->id == $task->creator_id;
    }
    
    
}
