<?php

namespace App\Policies;

use App\User;
use App\Ticket;
use App\UserGroup;
use Illuminate\Auth\Access\HandlesAuthorization;

class TicketPolicy
{
    use HandlesAuthorization;

    /**
     * Handles checking if user is administrator
     * returns void if not
     *
     * @param  App\User $user
     * @param  $this->methods
     * @return true or void
     */
    public function before($user, $ability)
    {
        if($user->isAdministrator()) {
            return true;
        }
    }

    public function update(User $user, Ticket $ticket)
    {
        return ($user->id == $ticket->creator_id)    || 
               ($user->id == $ticket->requester_id)  ||
               ($user->id == $ticket->supervisor_id) ||
               ($user->id == $ticket->developer_id) ||
               ($user->id == $ticket->qa_id)||
               ($user->userGroup->title == 'IT');
    }

    public function view(User $user, Ticket $ticket)
    {
        return is_null($ticket->ticket_id);
    }

    public function create(User $user, Ticket $ticket)
    {
        return is_null($ticket->ticket_id);
    }

    public function index(User $user, Ticket $ticket)
    {
        return ($user->id == $ticket->creator_id) ||
               ($user->id == $ticket->requester_id)  ||
               ($user->id == $ticket->supervisor_id) ||
               ($user->id == $ticket->developer_id) ||
               ($user->id == $ticket->qa_id);
    }
}
