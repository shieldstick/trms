<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    /**
     * Handles checking if user is administrator
     * returns void if not
     *
     * @param  App\User $user
     * @param  $this->methods
     * @return true or void
     */
    public function before($user, $ability)
    {
        if($user->isAdministrator()) {
            return true;
        }
    }

    public function index(User $user)
    {
        return $user->isAdministrator();
    }

    public function create(User $user)
    {
        return $user->isAdministrator();
    }

    public function update(User $user)
    {
        return $user->isAdministrator();
    }

    public function edit(User $user)
    {
        return $user->isAdministrator();
    }

    public function delete(User $user)
    {
        return $user->isAdministrator();
    }

    public function restore(User $user)
    {
        return $user->isAdministrator();
    }
}
