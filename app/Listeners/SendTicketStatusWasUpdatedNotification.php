<?php

namespace App\Listeners;

use App\Events\UpdatedTicket;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Notifications\TicketStatusWasUpdated;

class SendTicketStatusWasUpdatedNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UpdatedTicket  $event
     * @return void
     */
    public function handle(UpdatedTicket $event)
    {
        if($event->ticket->isDirty('status_id')) {
            $ticket = $event->ticket->fresh();

            $ticket->subscriptions->filter(function($sub){
                return auth()->id() != $sub->user_id;
            })->each(function($sub) use ($ticket) {
                $sub->user->notify(new TicketStatusWasUpdated($ticket));
            });
        }
    }
}
