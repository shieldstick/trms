<?php

namespace App\Listeners;

use App\Events\UpdatedTicket;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class RecordUpdatedTicketPriorityActivity
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UpdatedTicket  $event
     * @return void
     */
    public function handle(UpdatedTicket $event)
    {
        if($event->ticket->isDirty('priority_id')) {
            $old = $event->ticket->priority->title;
            $new = $event->ticket->fresh()->priority->title;

            $event->ticket->recordActivity($event->ticket->id, 'updated_priority', $old, $new);
        }
    }
}
