<?php

namespace App\Listeners;

use App\Events\UpdatedTicket;
use Illuminate\Queue\InteractsWithQueue;
use App\Notifications\AssignedAsDeveloper;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendDeveloperWasAssignedNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  $event
     * @return void
     */
    public function handle($event)
    {
        if($event->ticket->isDirty('developer_id')) {
            $ticket = $event->ticket->fresh();
            $ticket->developer->notify(new AssignedAsDeveloper($ticket->developer, $ticket));
        }
    }
}
