<?php

namespace App\Listeners;

use App\Events\UpdatedComment;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class RecordUpdatedCommentActivity
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UpdatedComment  $event
     * @return void
     */
    public function handle(UpdatedComment $event)
    {
        $comment = $event->comment;

        $newComment = $comment->fresh();
        if($comment->isDirty('body')){
            $comment->recordActivity($comment->commentable->id, 'updated', $comment->getOriginal('body'), $newComment->body);
        }
    }
}
