<?php

namespace App\Listeners;

use App\Events\UpdatedTicket;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class RecordUpdatedTicketDescriptionActivity
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UpdatedTicket  $event
     * @return void
     */
    public function handle(UpdatedTicket $event)
    {
        if($event->ticket->isDirty('description')) {
            $old = $event->ticket->getOriginal('description');
            $new = $event->ticket->fresh()->description;

            $ticket = $event->ticket->fresh();
            $event->ticket->recordActivity($event->ticket->id, 'updated_description', $old, $new);
        }
    }
}
