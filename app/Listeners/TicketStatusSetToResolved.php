<?php

namespace App\Listeners;

use App\Status;
use Carbon\Carbon;
use App\Events\UpdatedTicket;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Notifications\TicketHasBeenResolved;

class TicketStatusSetToResolved
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UpdatedTicket  $event
     * @return void
     */
    public function handle(UpdatedTicket $event)
    {
        $ticket = $event->ticket->fresh();
        if($event->ticket->isDirty('status_id') && $ticket->status_id == Status::resolved()->id) {
            $ticket->update(['resolved_date' => Carbon::now()]);

            $ticket->subscriptions->filter(function($sub){
                return auth()->id() != $sub->user_id;
            })
            ->each(function($sub) use ($ticket) {
                $sub->user->notify(new TicketHasBeenResolved($ticket));
            });

            $ticket->createSurvey($ticket->creator);
            $ticket->createSurvey($ticket->supervisor);
        }
    }
}
