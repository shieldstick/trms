<?php

namespace App\Listeners;

use App\Events\UpdatedTicket;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class RecordUpdatedTicketTargetDateActivity
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UpdatedTicket  $event
     * @return void
     */
    public function handle(UpdatedTicket $event)
    {
        if($event->ticket->isDirty('target_date')) {
            $old = !is_null($event->ticket->getOriginal('target_date')) ? 
                    \Carbon::parse($event->ticket->getOriginal('target_date'))->format('M d, Y') : null;

            $new = ($event->ticket->fresh()->target_date) ? 
                    $event->ticket->fresh()->target_date->format('M d, Y') : null;

            $event->ticket->recordActivity($event->ticket->id, 'updated_target_date', $old, $new);
        }
    }
}
