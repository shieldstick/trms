<?php

namespace App\Listeners;

use App\Notifications\MadeAComment;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendCommentHasBeenCreatedNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  CreatedComment  $event
     * @return void
     */
    public function handle($event)
    {
        $ticket = $event->comment->commentable;
        $ticket->subscriptions->filter(function($sub){
                return auth()->id() != $sub->user_id;
            })->each(function($sub) use ($ticket, $event) {
                $sub->user->notify(new MadeAComment($event->comment->creator, $ticket, $event->comment));
            });
    }
}
