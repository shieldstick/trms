<?php

namespace App\Listeners;

use App\Events\UpdatedTicket;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class RecordUpdatedTicketRequestTypeActivity
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UpdatedTicket  $event
     * @return void
     */
    public function handle(UpdatedTicket $event)
    {
        if($event->ticket->isDirty('request_type_id')) {
            $old = $event->ticket->requestType->title;
            $new = $event->ticket->fresh()->requestType->title;

            $event->ticket->recordActivity($event->ticket->id, 'updated_request_type', $old, $new);
        }
    }
}
