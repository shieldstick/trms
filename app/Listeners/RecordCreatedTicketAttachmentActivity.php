<?php

namespace App\Listeners;

use App\Events\CreatedAttachment;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class RecordCreatedTicketAttachmentActivity
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  CreatedAttachment  $event
     * @return void
     */
    public function handle(CreatedAttachment $event)
    {
        $attachment = $event->attachment;
        
        if($attachment->attachable_type == 'App\Ticket') {
            $attachment->recordActivity($attachment->attachable->id, 'created', null, $attachment->file_name);
        }
        
    }
}
