<?php

namespace App\Listeners;

use App\Status;
use App\Events\UpdatedTicket;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Notifications\TicketWasEndorsedToRequester;

class SendTicketWasEndorsedToRequesterNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UpdatedTicket  $event
     * @return void
     */
    public function handle(UpdatedTicket $event)
    {
        $ticket = $event->ticket->fresh();
        if($event->ticket->isDirty('status_id') && $ticket->status_id == Status::endorsedToRequester()->id) {
            $ticket->requester->notify(new TicketWasEndorsedToRequester($ticket));
        }
    }
}
