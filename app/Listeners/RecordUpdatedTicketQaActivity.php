<?php

namespace App\Listeners;

use App\Events\UpdatedTicket;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class RecordUpdatedTicketQaActivity
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UpdatedTicket  $event
     * @return void
     */
    public function handle(UpdatedTicket $event)
    {
        if($event->ticket->isDirty('qa_id')) {
            $oldQa = ($event->ticket->qa) ? $event->ticket->qa->full_name : null;
            $newQa = $event->ticket->fresh()->qa->full_name;

            $ticket = $event->ticket->fresh();
            $event->ticket->recordActivity($event->ticket->id, 'updated_qa', $oldQa, $newQa);
        }
    }
}
