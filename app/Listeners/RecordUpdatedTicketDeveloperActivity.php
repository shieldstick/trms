<?php

namespace App\Listeners;

use App\Events\UpdatedTicket;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class RecordUpdatedTicketDeveloperActivity
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UpdatedTicket  $event
     * @return void
     */
    public function handle(UpdatedTicket $event)
    {
        if($event->ticket->isDirty('developer_id')) {
            $oldDeveloper = ($event->ticket->developer) ? $event->ticket->developer->full_name : null;
            $newDeveloper = $event->ticket->fresh()->developer->full_name;

            $ticket = $event->ticket->fresh();
            $event->ticket->recordActivity($event->ticket->id, 'updated_developer', $oldDeveloper, $newDeveloper);
        }
    }
}
