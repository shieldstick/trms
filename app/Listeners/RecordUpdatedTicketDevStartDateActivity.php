<?php

namespace App\Listeners;

use App\Events\UpdatedTicket;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class RecordUpdatedTicketDevStartDateActivity
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UpdatedTicket  $event
     * @return void
     */
    public function handle(UpdatedTicket $event)
    {
        if($event->ticket->isDirty('dev_start_date')) {
            $old = !is_null($event->ticket->getOriginal('dev_start_date')) ? 
                    \Carbon::parse($event->ticket->getOriginal('dev_start_date'))->format('M d, Y') : null;

            $new = ($event->ticket->fresh()->dev_start_date) ? 
                    $event->ticket->fresh()->dev_start_date->format('M d, Y') : null;

            $event->ticket->recordActivity($event->ticket->id, 'updated_dev_start_date', $old, $new);
        }
    }
}
