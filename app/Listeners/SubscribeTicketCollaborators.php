<?php

namespace App\Listeners;

use App\Events\CreatedTicket;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SubscribeTicketCollaborators
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  $event
     * @return void
     */
    public function handle($event)
    {
        $collaborators = collect(['creator', 'requester', 'supervisor', 'developer', 'qa']);

        $presentCollaborators = collect();

        $collaborators = $collaborators->filter(function($collaborator) use ($event, $presentCollaborators){
            if(!is_null($event->ticket->$collaborator)){
                $presentCollaborators->push($event->ticket->$collaborator);
            }
            return true;
        });
       
        $presentCollaborators->each(function($collaborator) use ($event){
            if(!$event->ticket->isSubscribedBy($collaborator->id)){
                $event->ticket->subscribe($collaborator->id);
            }
        });
    }
}
