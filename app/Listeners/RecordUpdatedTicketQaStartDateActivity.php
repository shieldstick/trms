<?php

namespace App\Listeners;

use App\Events\UpdatedTicket;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class RecordUpdatedTicketQaStartDateActivity
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UpdatedTicket  $event
     * @return void
     */
    public function handle(UpdatedTicket $event)
    {
        if($event->ticket->isDirty('qa_start_date')) {
            $old = !is_null($event->ticket->getOriginal('qa_start_date')) ? 
                    \Carbon::parse($event->ticket->getOriginal('qa_start_date'))->format('M d, Y') : null;

            $new = ($event->ticket->fresh()->qa_start_date) ? 
                    $event->ticket->fresh()->qa_start_date->format('M d, Y') : null;

            $event->ticket->recordActivity($event->ticket->id, 'updated_qa_start_date', $old, $new);
        }
    }
}
