<?php

namespace App\Listeners;

use App\Events\CreatedTicket;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class RecordCreatedTicketActivity
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  CreatedTicket  $event
     * @return void
     */
    public function handle(CreatedTicket $event)
    {
        $ticket = $event->ticket;
        $ticket->recordActivity($ticket->id, 'created');
    }
}
