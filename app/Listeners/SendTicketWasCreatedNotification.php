<?php

namespace App\Listeners;

use App\Events\CreatedTicket;
use App\Notifications\TicketWasCreated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendTicketWasCreatedNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param $event
     * @return void
     */
    public function handle($event)
    {
        $ticket = $event->ticket->fresh();
        
        $ticket->subscriptions->each(function($sub) use ($ticket) {
                $sub->user->notify(new TicketWasCreated($ticket));
        });
    }
}
