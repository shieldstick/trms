<?php

namespace App\Listeners;

use App\Events\UpdatedTicket;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class RecordUpdatedTicketStatusActivity
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UpdatedTicket  $event
     * @return void
     */
    public function handle(UpdatedTicket $event)
    {
        if($event->ticket->isDirty('status_id')) {
            $old = \App\Status::find($event->ticket->getOriginal('status_id'))->title;
            $new = $event->ticket->fresh()->status->title;
            
            $event->ticket->recordActivity($event->ticket->id, 'updated_status', $old, $new);
        }
    }
}
