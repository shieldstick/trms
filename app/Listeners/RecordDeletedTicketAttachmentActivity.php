<?php

namespace App\Listeners;

use App\Events\DeletedAttachment;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class RecordDeletedTicketAttachmentActivity
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  DeletedAttachment  $event
     * @return void
     */
    public function handle(DeletedAttachment $event)
    {
        $attachment = $event->attachment;
        
        if($attachment->attachable_type == 'App\Ticket') {
            $attachment->recordActivity($attachment->attachable->id, 'deleted', $attachment->file_name, null);
        }
    }
}
