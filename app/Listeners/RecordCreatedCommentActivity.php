<?php

namespace App\Listeners;

use App\Events\CreatedComment;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class RecordCreatedCommentActivity
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  CreatedComment  $event
     * @return void
     */
    public function handle(CreatedComment $event)
    {
        $comment = $event->comment;

        $comment->recordActivity($comment->commentable->id, 'created', null, $comment->body);
    }
}
