<?php

namespace App\Listeners;

use App\Events\UpdatedTicket;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class RecordUpdatedTicketDevCompletionDateActivity
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UpdatedTicket  $event
     * @return void
     */
    public function handle(UpdatedTicket $event)
    {
        if($event->ticket->isDirty('dev_completion_date')) {
            $old = !is_null($event->ticket->getOriginal('dev_completion_date')) ? 
                    \Carbon::parse($event->ticket->getOriginal('dev_completion_date'))->format('M d, Y') : null;

            $new = ($event->ticket->fresh()->dev_completion_date) ? 
                    $event->ticket->fresh()->dev_completion_date->format('M d, Y') : null;

            $event->ticket->recordActivity($event->ticket->id, 'updated_dev_completion_date', $old, $new);
        }
    }
}
