<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{
    protected $guarded = [];

    protected $dates = ['created_at', 'updated_at'];

    protected $with = ['creator'];

    public function creator()
    {
    	return $this->belongsTo(User::class, 'user_id')->withTrashed();
    }

    public function ticket()
    {
    	return $this->belongsTo(Ticket::class, 'ticket_id');
    }

    public function subject()
    {
    	return $this->morphTo();
    }
    
}
