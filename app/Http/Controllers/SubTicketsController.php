<?php

namespace App\Http\Controllers;

use App\User;
use App\Client;
use App\Module;
use App\Status;
use App\Ticket;
use App\Priority;
use App\RequestType;
use Illuminate\Http\Request;

class SubTicketsController extends Controller
{
	protected $users;
    protected $statuses;
    protected $modules;
    protected $clients;
    protected $priorities;
    protected $requestTypes;
    protected $developers;

    public function __construct(User $users, Status $statuses, Module $modules, Client $clients, Priority $priorities, RequestType $requestTypes)
    {
        $this->middleware('auth');

        $this->users            = $users;
        $this->statuses         = $statuses;
        $this->modules          = $modules;
        $this->clients          = $clients;
        $this->priorities       = $priorities;
        $this->requestTypes     = $requestTypes;
    }
	
    public function create(Ticket $ticket)
    {
        $this->authorize('create', $ticket);

    	return view('sub-tickets.create')
    			->with(['ticket'  => $ticket,
    					'modules' => $this->modules->all(),
    			]);
    }


    public function store(Ticket $ticket, Request $request)
    {
        $this->validate($request,[
                'module'        =>  'required',
                'description'   =>  'required',
        ]);

    	$subTicket = Ticket::create([
                'subject'                =>  $ticket->subject,
                'priority_id'            =>  $ticket->priority_id,
                'target_date'            =>  $ticket->target_date,
                'client_id'              =>  $ticket->client_id,
                'module_id'              =>  $request->module,
                'request_type_id'        =>  $ticket->request_type_id,
                'requester_id'           =>  $ticket->requester_id,
                'creator_id'             =>  $ticket->creator_id,
                'supervisor_id'          =>  (User::where('email', 'teena.delmundo@comgateway.com')->first())->id,  // hard coding for now since no structure yet
                'description'            =>  $request->description,
                'ticket_id'              =>  $ticket->id,
                'number'                 =>  ($ticket->children->count() + 1),
        ]);

        if($request->hasFile('attachments')) $subTicket->addAttachments($request->attachments);

        return redirect('/tickets/' . $ticket->id . '#sub-tickets')
                ->with(['flash_message' => 'Sub-ticket #' . $subTicket->identification . ' has been created!']);
    }


    public function show(Ticket $ticket)
    {
        return view('sub-tickets.show')
                ->with([
                        'ticket'        => $ticket,
                        'users'         => $this->users->all(),
                        'developers'    => $this->users->developers(),
                        'qas'           => $this->users->qas(),
                        'statuses'      => $this->statuses->all(),
                        'modules'       => $this->modules->all(),
                        'clients'       => $this->clients->all(),
                        'priorities'    => $this->priorities->all(),
                        'requestTypes'  => $this->requestTypes->all(),
                    ]);
    }

    public function update(Ticket $ticket, Request $request)
    {
        $this->authorize('update', $ticket);
        
        $this->validate($request,[
                // 'subject'       =>  'required',
                // 'priority'      =>  'required',
                // 'client'        =>  'required',
                'module'        =>  'required',
                // 'request_type'  =>  'required',
                // 'requester'     =>  'required',
                'description'   =>  'required',
        ]);

        $ticket->update([
                'subject'               =>  $request->subject,
                // 'priority_id'           =>  $request->priority,
                'target_date'           =>  $request->target_date ? strtotime($request->target_date) : null,
                // 'client_id'             =>  $request->client,
                'module_id'             =>  $request->module,
                // 'request_type_id'       =>  $request->request_type,
                'status_id'             =>  $request->status ?? $ticket->status_id,
                'developer_id'          =>  $request->developer ?? $ticket->developer_id,
                'qa_id'                 =>  $request->qa ?? $ticket->qa_id,
                'dev_start_date'        =>  $request->dev_start_date ? strtotime($request->dev_start_date) : $ticket->dev_start_date,
                'dev_completion_date'   =>  $request->dev_completion_date ? strtotime($request->dev_completion_date) : $ticket->dev_completion_date,
                'qa_start_date'         =>  $request->qa_start_date ? strtotime($request->qa_start_date) : $ticket->qa_start_date,
                'qa_completion_date'    =>  $request->qa_completion_date ? strtotime($request->qa_completion_date) : $ticket->qa_completion_date,
                // 'requester_id'          =>  $request->requester,
                'description'           =>  $request->description,
        ]);

        if($request->hasFile('attachments')) $ticket->addAttachments($request->attachments);

        return redirect('/sub-tickets/' . $ticket->id)
                ->with(['flash_message' => 'Sub-ticket #'. $ticket->identification .' has been updated!']);
    }
    
}
