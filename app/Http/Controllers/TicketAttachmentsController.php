<?php

namespace App\Http\Controllers;

use App\Ticket;
use App\Attachment;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class TicketAttachmentsController extends Controller
{   
    public function show(Ticket $ticket, Attachment $attachment)
    {
    	$file = $attachment->storage_path.'/'.$attachment->hash_name;

    	return file_exists($file) ? 
    			response()->download($file) : abort('404', 'File does not exist.');
    }

    public function delete(Ticket $ticket, Attachment $attachment)
    {
    	$attachment->delete();

    	session()->flash('flash_message', 'Attachment has been deleted!');
        return back();
    }
    
}
