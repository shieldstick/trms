<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PagesController extends Controller
{
    
    public function index()
    {
    	return redirect('/login');
    }


    public function home()
    {
    	// return view('pages.home');
    	return redirect('/tickets');
    }
    
    
}
