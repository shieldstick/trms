<?php

namespace App\Http\Controllers;

use App\Ticket;
use Illuminate\Http\Request;

class SubTicketCommentsController extends Controller
{
    public function __construct()
	{
		$this->middleware('auth');
	}
	
    
    public function store(Ticket $ticket)
    {
    	$comment = $ticket->addComment([
    			'body'		=> request('body'),
    			'user_id'	=> auth()->id(),
    	]);

        if(request()->expectsJson())
        {
            return $comment->load('creator');
        }

    	session()->flash('flash_message', 'Comment has been added!');
        return redirect('/tickets/' . $ticket->id);
    }
}
