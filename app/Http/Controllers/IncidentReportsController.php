<?php

namespace App\Http\Controllers;

use App\User;
use App\Client;
use App\Module;
use App\Status;
use App\Priority;
use Carbon\Carbon;
use App\TargetDate;
use App\RequestType;
use App\IncidentReport;
use Illuminate\Http\Request;

class IncidentReportsController extends Controller
{
	public function __construct(User $users, Status $statuses, Module $modules, Client $clients, Priority $priorities, RequestType $requestTypes)
	{
		$this->middleware('auth');

        $this->users            = $users;
        $this->statuses         = $statuses;
        $this->modules          = $modules;
        $this->clients          = $clients;
        $this->priorities       = $priorities;
        $this->requestTypes     = $requestTypes;
	}
	
    /**
     * Show incident report creation form
     *
     * @return view
     */
    public function create()
    {
    	return view('incident-reports.create')
                ->with([
                        'priorities'    => $this->priorities->all(),
                        'clients'       => $this->clients->all(),
                        'modules'       => $this->modules->all(),
                        'users'         => $this->users->all(),
                    ]);
    }

    public function store(Request $request)
    {
    	$this->validate($request, [
                'subject'                   =>  'required',
                'priority'                  =>  'required',
                'client'                    =>  'required',
                'module'                    =>  'required',
                'requester'                 =>  'required',
                'incident_date'             =>  'required',
                'portals_affected'          =>  'required',
                'details'                   =>  'required',
                'impact'                    =>  'required',
        ]);
        // return dd($request->start_time);
        // return dd(Carbon::createFromFormat('h:i A', $request->start_time)->format('H:i:s'));

        $report = IncidentReport::create([
                'subject'                =>  $request->subject,
                'priority_id'            =>  $request->priority,
                'target_date'            =>  TargetDate::make($request->priority),
                'client_id'              =>  $request->client,
                'module_id'              =>  $request->module,
                'requester_id'           =>  $request->requester,
                'creator_id'             =>  auth()->user()->id,
                'supervisor_id'          =>  (User::where('email', 'teena.delmundo@comgateway.com')->first())->id,
                'incident_date'          =>  strtotime($request->incident_date),
                'start_time'             =>  Carbon::createFromFormat('h:i A', $request->start_time)->format('H:i:s'),
                'portals_affected'       =>  $request->portals_affected,
                'details'                =>  $request->details,
                'impact'                 =>  $request->impact,
                'references'             =>  $request->references ?: null,
            ]);

        if($request->hasFile('attachments')) $report->addAttachments($request->attachments);

        return redirect('/tickets?open=true')
                ->with(['flash_message' => 'Incident report has been created!']);
    }
    
    
}
