<?php

namespace App\Http\Controllers;

use App\User;
use App\Client;
use App\Module;
use App\Status;
use App\Ticket;
use App\Priority;
use Carbon\Carbon;
use App\TargetDate;
use App\RequestType;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Filters\TicketFilters;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\AttachmentsController;

class TicketsController extends Controller
{
    protected $users;
    protected $statuses;
    protected $modules;
    protected $clients;
    protected $priorities;
    protected $requestTypes;
    protected $developers;

    public function __construct(User $users, Status $statuses, Module $modules, Client $clients, Priority $priorities, RequestType $requestTypes)
    {
    	$this->middleware('auth');

        $this->users            = $users;
        $this->statuses         = $statuses;
        $this->modules          = $modules;
        $this->clients          = $clients;
        $this->priorities       = $priorities;
        $this->requestTypes     = $requestTypes;
    }

    /**
     * Show tickets table
     *
     * @param  TicketFilters $filters
     * @return view
     */
    public function index(TicketFilters $filters)
    {
    	$tickets = Ticket::withoutGlobalScopes()
                         ->filter($filters)
                         ->has('parent', '=', 0)
                         ->get();

    	return view('tickets.index')
    			->with([
                        'tickets'       => $tickets,
                        'users'         => $this->users->all(),
                        'statuses'      => $this->statuses->all(),
                        'modules'       => $this->modules->all(),
                        'clients'       => $this->clients->all(),
                        'priorities'    => $this->priorities->all(),
                        'requestTypes'  => $this->requestTypes->all(),
                    ]);
    }
    
    /**
     * Show ticket details
     *
     * @param  Ticket $ticket
     * @return view
     */
    public function show(Ticket $ticket)
    {   
        $this->authorize('view', $ticket);

        return view('tickets.show')
                ->with([
                        'ticket'        => $ticket,
                        'users'         => $this->users->all(),
                        'developers'    => $this->users->developers(),
                        'qas'           => $this->users->qas(),
                        'statuses'      => $this->statuses->all(),
                        'modules'       => $this->modules->all(),
                        'clients'       => $this->clients->all(),
                        'priorities'    => $this->priorities->all(),
                        'requestTypes'  => $this->requestTypes->withTrashed()->get(),
                    ]);
    }

    /**
     * Show ticket creation form
     *
     * @return view
     */
    public function create()
    {
        return view('tickets.create')
                ->with([
                        'users'         => $this->users->all(),
                        'statuses'      => $this->statuses->all(),
                        'modules'       => $this->modules->all(),
                        'clients'       => $this->clients->all(),
                        'priorities'    => $this->priorities->all(),
                        'requestTypes'  => $this->requestTypes->all(),
                    ]);
    }

    /**
     * Save a new ticket
     *
     * @param  Request $request
     * @return Redirect
     */
    public function store(Request $request)
    {
        $this->validate($request,[
                'subject'       =>  'required',
                'priority'      =>  'required',
                'client'        =>  'required',
                'module'        =>  'required',
                'request_type'  =>  'required',
                'requester'     =>  'required',
                'description'   =>  'required',
        ]);

        $ticket = Ticket::create([
                'subject'                =>  $request->subject,
                'priority_id'            =>  $request->priority,
                'target_date'            =>  TargetDate::make($request->priority),
                'client_id'              =>  $request->client,
                'module_id'              =>  $request->module,
                'request_type_id'        =>  $request->request_type,
                'requester_id'           =>  $request->requester,
                'creator_id'             =>  auth()->user()->id,
                'supervisor_id'          =>  (User::where('email', 'teena.delmundo@comgateway.com')->first())->id,  // hard coding for now since no structure yet
                'description'            =>  $request->description,
        ]);

        if($request->hasFile('attachments')) $ticket->addAttachments($request->attachments);

        return redirect('/tickets?open=true')
                ->with(['flash_message' => 'Ticket #' . $ticket->identification . ' has been created!']);
    }

    /**
     * Update an existing ticket
     *
     * @param  Ticket  $ticket
     * @param  Request $request
     * @return Redirect
     */
    public function update(Ticket $ticket, Request $request)
    {
        // return dd($request);

        $this->authorize('update', $ticket);
        
        $this->validate($request,[
                'subject'       =>  'required',
                'priority'      =>  'required',
                'client'        =>  'required',
                'module'        =>  'required',
                // 'request_type'  =>  'required',
                'requester'     =>  'required',
                'description'   =>  'required',
        ]);

        $ticket->update([
                'subject'               =>  $request->subject,
                'priority_id'           =>  $request->priority,
                'target_date'           =>  $request->target_date ? strtotime($request->target_date) : null,
                'client_id'             =>  $request->client,
                'module_id'             =>  $request->module,
                'request_type_id'       =>  $request->request_type ?? $ticket->request_type_id,
                'status_id'             =>  $request->status ?? $ticket->status_id,
                'developer_id'          =>  $request->developer ?? $ticket->developer_id,
                'qa_id'                 =>  $request->qa ?? $ticket->qa_id,
                'dev_start_date'        =>  $request->dev_start_date ? strtotime($request->dev_start_date) : $ticket->dev_start_date,
                'dev_completion_date'   =>  $request->dev_completion_date ? strtotime($request->dev_completion_date) : $ticket->dev_completion_date,
                'qa_start_date'         =>  $request->qa_start_date ? strtotime($request->qa_start_date) : $ticket->qa_start_date,
                'qa_completion_date'    =>  $request->qa_completion_date ? strtotime($request->qa_completion_date) : $ticket->qa_completion_date,
                'requester_id'          =>  $request->requester,
                'description'           =>  $request->description,
        ]);

        if($request->hasFile('attachments')) $ticket->addAttachments($request->attachments);

        return redirect('/tickets/' . $ticket->id)
                ->with(['flash_message' => 'Ticket #' . $ticket->identification . ' has been updated!']);
    }

}
