<?php

namespace App\Http\Controllers;

use App\Survey;
use App\Ticket;
use Illuminate\Http\Request;

class SurveysController extends Controller
{
    
    public function __construct()
    {
    	$this->middleware('auth');
    }


    public function index()
    {
    	$surveys = Survey::orderBy('completed', 'asc')->get();

    	return view('surveys.index')
    			->with(['surveys' => $surveys]);
    }

    public function show(Survey $survey)
    {
        $this->authorize('view', $survey);

        return view('surveys.show')
                ->with([
                        'ticket' => $survey->ticket, 
                        'survey' => $survey
                    ]);
    }
    
    
    public function update(Survey $survey, Request $request)
    {
        $this->authorize('update', $survey);

        $this->validate($request, [
                        'q1' => 'required',
                        'q2' => 'required',
                        'q3' => 'required',
            ]);

        $survey->update([
                    'q1'        => $request->q1,
                    'q2'        => $request->q2,
                    'q3'        => $request->q3,
                    'completed' => true,
            ]);

        $survey = $survey->fresh();

        return redirect('/surveys/' . $survey->id)
                ->with([
                        'ticket' => $survey->ticket, 
                        'survey' => $survey,
                        'flash_message' =>  'Thank you for completing our survey!'
                    ]);
    }
    
    
}
