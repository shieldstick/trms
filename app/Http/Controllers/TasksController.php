<?php

namespace App\Http\Controllers;

use App\Task;
use App\Ticket;
use App\Filters\TaskFilters;
use Illuminate\Http\Request;

class TasksController extends Controller
{
    
    public function __construct()
    {
    	$this->middleware('auth');
    }


    public function index(TaskFilters $filters)
    {
        $tasks = Task::filter($filters)
                         ->orderBy('created_at', 'desc')
                         ->orderBy('completed', 'asc')
                         ->get();

        return view('tasks.index')
                ->with(['tasks' =>  $tasks]);
    }
    


    public function store(Ticket $ticket, Request $request)
    {
    	$this->validate($request, [
    			'subject'	=>	'required',
    			'assignee'	=>	'required',
    			'details'	=>	'required',
    		]);

    	$ticket->tasks()->create([
    			'subject'		=>	$request->subject,
    			'assignee_id'	=>	$request->assignee,
    			'creator_id'	=>	auth()->user()->id,
    			'due_date'		=>	$request->due_date ? strtotime($request->due_date) : null,
                'completed'     =>  false,
    			'details'		=>	$request->details,
    			'number'	    =>	(int)$ticket->tasks()->count() + 1
    		]);

    	return redirect('/tickets/' . $ticket->id)
    			->with(['flash_message'	=>	'Task has been created!']);
    }
    
    
    public function complete(Task $task)
    {
        try {
            $this->authorize('update', $task);
            $task->update(['completed' => request('completed')]);
        } catch(\Exception $e) {
            return response('You are not authorized for this action!', 403);
        }
        
        return $task->fresh();
    }
    
}
