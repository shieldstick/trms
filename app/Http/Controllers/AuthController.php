<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function index()
    {
    	return view('auth.index');
    }

    public function login(Request $request)
    {
    	$this->validate($request, [
    		'email'				=> 	'email|required',
    		'password'			=> 	'required',
    	]);

        $user = User::withTrashed()->where('email', $request->email)->first();

        if(is_null($user) || $user->trashed())
        {
            return redirect('/login')
                    ->with(['flash_message' =>  'Account not active. Please contact your administrator.',
                            'flash_level'   =>  'Danger']);
        }

    	if(!auth()->attempt(['email' => $request->email, 'password' => $request->password])) 
    	{
            return redirect('/login')
                    ->with(['flash_message' =>  'Incorrect login credentials!',
                            'flash_level'   =>  'Danger']);
        }

        return redirect('/tickets?open=true');
    }

    public function logout()
    {
    	auth()->logout();
    	return redirect('/login');
    }
}
