<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class NotificationsController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
	}
	
	
    public function index(User $user)
    {
    	return $user->unreadNotifications;
    }


    public function delete(User $user, $notification)
    {
    	$user->notifications()->find($notification)->markAsRead();
    }
    
    
}
