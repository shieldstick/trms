<?php

namespace App\Http\Controllers;

use App\User;
use App\UserGroup;
use App\AccessLevel;
use Illuminate\Http\Request;

class UsersController extends Controller
{
    protected $accessLevel;
    protected $userGroup;
    
    public function __construct(AccessLevel $accessLevel, UserGroup $userGroup)
    {
    	$this->middleware('auth');
        $this->accessLevel = $accessLevel;
        $this->userGroup = $userGroup;
    }

    /**
     * Show table of Users
     *
     * @return view
     */
    public function index()
    {
        $this->authorize('index', User::class);
    	$users = User::withTrashed()->get();

    	return view('users.index')
    			->with(['users' => $users]);
    }

    /**
     * Show specific user
     *
     * @param  User $user
     * @return view
     */
    public function show(User $user)
    {
        return view('users.show')
                ->with(['user'  => $user]);        
    }
    
    /**
     * Show user creation form
     *
     * @return view
     */
    public function create()
    {
        $this->authorize('create', User::class);
        return view('users.create')
                ->with(['accessLevel'   => $this->accessLevel->all(),
                        'userGroup'     => $this->userGroup->all(),
                ]);
    }

    /**
     * Persist created user in database
     *
     * @param  Request $request
     * @return redirect
     */
    public function store(Request $request)
    {
        $this->authorize('create', User::class);
        $this->validate($request,[
                'first_name'    =>  'required|alpha',
                'last_name'     =>  'required|alpha',
                'email'         =>  'required|email|unique:users,email',
                'password'      =>  'required|confirmed|min:8',
                'access_level'  =>  'required',
                'user_group'    =>  'required',
                'timezone'      =>  'required',
        ]);

        User::create([
                'first_name'    => $request->first_name,
                'last_name'     => $request->last_name,
                'email'         => $request->email,
                'password'      => bcrypt($request->password),
                'access_level'  => $request->access_level,
                'user_group'    => $request->user_group,
                'timezone'      => $request->timezone,
        ]);

        session()->flash('flash_message', 'User account been created!');
        return redirect('/users');
    }
    
    /**
     * Show user edit form
     *
     * @param  User $user
     * @return view
     */
    public function edit(User $user)
    {
        $this->authorize('edit', User::class);
        
        return view('users.edit')
                ->with(['user'          => $user,
                        'accessLevel'   => $this->accessLevel->all(),
                        'userGroup'     => $this->userGroup->all(),
                ]);
    }

    /**
     * Update existing user in database
     *
     * @param  User    $user
     * @param  Request $request
     * @return redirect
     */
    public function update(User $user, Request $request)
    {
        $this->authorize('update', User::class);
        $this->validate($request,[
                'first_name'    =>  'required|alpha',
                'last_name'     =>  'required|alpha',
                'access_level'  =>  'required|integer',
                'user_group'    =>  'required|integer',
                'timezone'      =>  'required'
        ]);

        $user->update([
                'first_name'    => $request->first_name,
                'last_name'     => $request->last_name,
                'access_level'  => $request->access_level,
                'user_group'    => $request->user_group,
                'timezone'      => $request->timezone,
        ]);

        session()->flash('flash_message', 'User profile been updated!');
        return redirect('/users/' . $user->id);
    }
    
    /**
     * Soft delete a user
     *
     * @param  User    $user
     * @param  Request $request
     * @return redirect
     */
    public function delete(User $user, Request $request)
    {
        $this->authorize('delete', User::class);
        $user->delete();

        session()->flash('flash_message', 'User profile been deactivated!');
        return redirect('/users');

    }
    
    /**
     * Restore soft deleted user
     *
     * @param  User    $user
     * @param  Request $request
     * @return redirect
     */
    public function restore(User $user, Request $request)
    {
        $this->authorize('restore', User::class);
        $user->restore();

        session()->flash('flash_message', 'User profile been reactivated!');
        return redirect('/users');
    }

}
