<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Module extends Model
{
    protected $guarded = [];

    public static function frontEnd()
    {
    	return self::where(['title'	=>	'FrontEnd'])->first();
    }

    public static function sugar()
    {
    	return self::where(['title'	=>	'Sugar'])->first();
    }

    public static function flow()
    {
    	return self::where(['title'	=>	'Flow'])->first();
    }

    public static function liveChat()
    {
    	return self::where(['title'	=>	'LiveChat'])->first();
    }

    public static function liveQuote()
    {
    	return self::where(['title'	=>	'LiveQuote'])->first();
    }
}
