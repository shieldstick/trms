<?php

namespace App;

use App\Events\CreatedComment;
use App\Events\UpdatedComment;
use App\Traits\RecordsActivity;
use App\Notifications\MadeAComment;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    use RecordsActivity;

    protected $guarded = [];

    protected $with = ['creator'];

    protected static function boot()
    {
        parent::boot();

        static::created(function($comment){
            event(new CreatedComment($comment));
        });

        static::updated(function($comment) {
            event(new UpdatedComment($comment));
        });
    }

    public function commentable()
    {
        return $this->morphTo();
    }

    public function creator()
    {
    	return $this->belongsTo(User::class, 'user_id')->withTrashed();
    }

}
