<?php

namespace App;

use App\UserGroup;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    protected $guarded = [];

    protected static $options = ['New', 'In progress', 'QA Testing', 'Endorsed to requester',
                                 'Needs Rework','For Deployment', 'Resolved', 
                                 'Incomplete Brief', 'Cancelled'
                                 ];

    protected static $optionsForIT = ['New', 'In progress', 'QA Testing', 
                                      'Endorsed to requester','Incomplete Brief', 
                                      'Resolved',
                                 ];

    protected static $optionsForNonIT = ['Needs Rework','For Deployment'];


    public static function new()
    {
    	return static::where('title', 'New')->first();
    }

    public static function inProgress()
    {
    	return static::where('title', 'In progress')->first();
    }

    public static function qaTesting()
    {
    	return static::where('title', 'QA Testing')->first();
    }

    public static function incompleteBrief()
    {
    	return static::where('title', 'Incomplete Brief')->first();
    }

    public static function cancelled()
    {
    	return static::where('title', 'Cancelled')->first();
    }

    public static function endorsedToRequester()
    {
    	return static::where('title', 'Endorsed to requester')->first();
    }

    public static function needsRework()
    {
    	return static::where('title', 'Needs Rework')->first();
    }

    public static function forDeployment()
    {
    	return static::where('title', 'For Deployment')->first();
    }
    
    public static function resolved()
    {
    	return static::where('title', 'Resolved')->first();
    }

    public static function select($ticket, $user = null)
    {   
        $statuses = self::all();
        $user = ($user) ?: auth()->user();

        $select = '<select class="form-control" id="status" name="status">';

        foreach($statuses as $status){
            if(($user->id == $ticket->requester_id || $user->id == $ticket->creator_id) && 
                $user->userGroup->title != 'IT') {
                $select .= self::optionsForRequesterOrCreator($status->title, $status, $ticket);
            }
            elseif($user->userGroup->title == 'IT') {
                $select .= self::optionsForIT($status->title, $status, $ticket);
            }
            else {
                $select .= self::optionsForNonCollaborator($status->title, $status, $ticket);
            }
        }

        $select .= '</select>';
        return $select;
    }

    protected static function optionsForRequesterOrCreator($option, $status, $ticket)
    {
        $htmlOption = '';
        $endorsedToRequester = $ticket->status->title == 'Endorsed to requester';

        switch($option){
            case 'New':
                $htmlOption .= '<option value="'. $status->id . '" ';
                $htmlOption .= 'disabled ';
                $htmlOption .= $ticket->status_id == $status->id ? 'selected ' : '';
                $htmlOption .= '>' . $status->title;
                $htmlOption .= '</option>';
            break;

            case 'In progress':
                $htmlOption .= '<option value="'. $status->id . '" ';
                $htmlOption .= 'disabled ';
                $htmlOption .= $ticket->status_id == $status->id ? 'selected ' : '';
                $htmlOption .= '>' . $status->title;
                $htmlOption .= '</option>';
            break;

            case 'QA Testing':
                $htmlOption .= '<option value="'. $status->id . '" ';
                $htmlOption .= 'disabled ';
                $htmlOption .= $ticket->status_id == $status->id ? 'selected ' : '';
                $htmlOption .= '>' . $status->title;
                $htmlOption .= '</option>';
            break;

            case 'Endorsed to requester':
                $htmlOption .= '<option value="'. $status->id . '" ';
                $htmlOption .= 'disabled ';
                $htmlOption .= $ticket->status_id == $status->id ? 'selected ' : '';
                $htmlOption .= '>' . $status->title;
                $htmlOption .= '</option>';
            break;

            case 'Needs Rework':
                $htmlOption .= '<option value="'. $status->id . '" ';
                $htmlOption .= $endorsedToRequester ? 'enabled ' : 'disabled ';
                $htmlOption .= $ticket->status_id == $status->id ? 'selected ' : '';
                $htmlOption .= '>' . $status->title;
                $htmlOption .= '</option>';
            break;

            case 'For Deployment':
                $htmlOption .= '<option value="'. $status->id . '" ';
                $htmlOption .= $endorsedToRequester ? 'enabled ' : 'disabled ';
                $htmlOption .= $ticket->status_id == $status->id ? 'selected ' : '';
                $htmlOption .= '>' . $status->title;
                $htmlOption .= '</option>';
            break;

            case 'Resolved':
                $htmlOption .= '<option value="'. $status->id . '" ';
                $htmlOption .= $endorsedToRequester ? 'enabled ' : 'disabled ';
                $htmlOption .= $ticket->status_id == $status->id ? 'selected ' : '';
                $htmlOption .= '>' . $status->title;
                $htmlOption .= '</option>';
            break;

            case 'Incomplete Brief':
                $htmlOption .= '<option value="'. $status->id . '" ';
                $htmlOption .= 'disabled ';
                $htmlOption .= $ticket->status_id == $status->id ? 'selected ' : '';
                $htmlOption .= '>' . $status->title;
                $htmlOption .= '</option>';
            break;

            case 'Cancelled':
                $htmlOption .= '<option value="'. $status->id . '" ';
                $htmlOption .= 'enabled';
                $htmlOption .= $ticket->status_id == $status->id ? 'selected ' : '';
                $htmlOption .= '>' . $status->title;
                $htmlOption .= '</option>';
            break;
        }

        return $htmlOption;
    }

    protected static function optionsForIT($option, $status, $ticket)
    {
        $htmlOption = '';
        $endorsedToRequester = $ticket->status->title == 'Endorsed to requester';

        switch($option){
            case 'New':
                $htmlOption .= '<option value="'. $status->id . '" ';
                $htmlOption .= 'enabled ';
                $htmlOption .= $ticket->status_id == $status->id ? 'selected ' : '';
                $htmlOption .= '>' . $status->title;
                $htmlOption .= '</option>';
            break;

            case 'In progress':
                $htmlOption .= '<option value="'. $status->id . '" ';
                $htmlOption .= 'enabled ';
                $htmlOption .= $ticket->status_id == $status->id ? 'selected ' : '';
                $htmlOption .= '>' . $status->title;
                $htmlOption .= '</option>';
            break;

            case 'QA Testing':
                $htmlOption .= '<option value="'. $status->id . '" ';
                $htmlOption .= 'enabled ';
                $htmlOption .= $ticket->status_id == $status->id ? 'selected ' : '';
                $htmlOption .= '>' . $status->title;
                $htmlOption .= '</option>';
            break;

            case 'Endorsed to requester':
                $htmlOption .= '<option value="'. $status->id . '" ';
                $htmlOption .= $endorsedToRequester ? 'disabled ' : 'enabled ';
                $htmlOption .= $ticket->status_id == $status->id ? 'selected ' : '';
                $htmlOption .= '>' . $status->title;
                $htmlOption .= '</option>';
            break;

            case 'Needs Rework':
                $htmlOption .= '<option value="'. $status->id . '" ';
                $htmlOption .= 'disabled ';
                $htmlOption .= $ticket->status_id == $status->id ? 'selected ' : '';
                $htmlOption .= '>' . $status->title;
                $htmlOption .= '</option>';
            break;

            case 'For Deployment':
                $htmlOption .= '<option value="'. $status->id . '" ';
                $htmlOption .= 'disabled ';
                $htmlOption .= $ticket->status_id == $status->id ? 'selected ' : '';
                $htmlOption .= '>' . $status->title;
                $htmlOption .= '</option>';
            break;

            case 'Resolved':
                $htmlOption .= '<option value="'. $status->id . '" ';
                $htmlOption .= 'disabled ';
                $htmlOption .= $ticket->status_id == $status->id ? 'selected ' : '';
                $htmlOption .= '>' . $status->title;
                $htmlOption .= '</option>';
            break;

            case 'Incomplete Brief':
                $htmlOption .= '<option value="'. $status->id . '" ';
                $htmlOption .= 'enabled ';
                $htmlOption .= $ticket->status_id == $status->id ? 'selected ' : '';
                $htmlOption .= '>' . $status->title;
                $htmlOption .= '</option>';
            break;

            case 'Cancelled':
                $htmlOption .= '<option value="'. $status->id . '" ';
                $htmlOption .= 'disabled ';
                $htmlOption .= $ticket->status_id == $status->id ? 'selected ' : '';
                $htmlOption .= '>' . $status->title;
                $htmlOption .= '</option>';
            break;
        }

        return $htmlOption;
    }

    protected static function optionsForNonCollaborator($option, $status, $ticket)
    {
        $htmlOption = '';
        $endorsedToRequester = $ticket->status->title == 'Endorsed to requester';

        switch($option){
            case 'New':
                $htmlOption .= '<option value="'. $status->id . '" ';
                $htmlOption .= 'disabled ';
                $htmlOption .= $ticket->status_id == $status->id ? 'selected ' : '';
                $htmlOption .= '>' . $status->title;
                $htmlOption .= '</option>';
            break;

            case 'In progress':
                $htmlOption .= '<option value="'. $status->id . '" ';
                $htmlOption .= 'disabled ';
                $htmlOption .= $ticket->status_id == $status->id ? 'selected ' : '';
                $htmlOption .= '>' . $status->title;
                $htmlOption .= '</option>';
            break;

            case 'QA Testing':
                $htmlOption .= '<option value="'. $status->id . '" ';
                $htmlOption .= 'disabled ';
                $htmlOption .= $ticket->status_id == $status->id ? 'selected ' : '';
                $htmlOption .= '>' . $status->title;
                $htmlOption .= '</option>';
            break;

            case 'Endorsed to requester':
                $htmlOption .= '<option value="'. $status->id . '" ';
                $htmlOption .= 'disabled ';
                $htmlOption .= $ticket->status_id == $status->id ? 'selected ' : '';
                $htmlOption .= '>' . $status->title;
                $htmlOption .= '</option>';
            break;

            case 'Needs Rework':
                $htmlOption .= '<option value="'. $status->id . '" ';
                $htmlOption .= 'disabled ';
                $htmlOption .= $ticket->status_id == $status->id ? 'selected ' : '';
                $htmlOption .= '>' . $status->title;
                $htmlOption .= '</option>';
            break;

            case 'For Deployment':
                $htmlOption .= '<option value="'. $status->id . '" ';
                $htmlOption .= 'disabled ';
                $htmlOption .= $ticket->status_id == $status->id ? 'selected ' : '';
                $htmlOption .= '>' . $status->title;
                $htmlOption .= '</option>';
            break;

            case 'Resolved':
                $htmlOption .= '<option value="'. $status->id . '" ';
                $htmlOption .= 'disabled ';
                $htmlOption .= $ticket->status_id == $status->id ? 'selected ' : '';
                $htmlOption .= '>' . $status->title;
                $htmlOption .= '</option>';
            break;

            case 'Incomplete Brief':
                $htmlOption .= '<option value="'. $status->id . '" ';
                $htmlOption .= 'disabled ';
                $htmlOption .= $ticket->status_id == $status->id ? 'selected ' : '';
                $htmlOption .= '>' . $status->title;
                $htmlOption .= '</option>';
            break;

            case 'Cancelled':
                $htmlOption .= '<option value="'. $status->id . '" ';
                $htmlOption .= 'disabled ';
                $htmlOption .= $ticket->status_id == $status->id ? 'selected ' : '';
                $htmlOption .= '>' . $status->title;
                $htmlOption .= '</option>';
            break;
        }

        return $htmlOption;
    }
}
