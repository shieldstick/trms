<?php

Route::get('/', 'PagesController@index');
Route::get('/forgot-password', 'ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('/forgot-password', 'ForgotPasswordController@sendResetLinkEmail');
Route::get('/password/reset/{token}', 'ResetPasswordController@showResetForm')->name('password.reset');
Route::post('/password/reset', 'ResetPasswordController@reset');

Route::get('/login', 'AuthController@index')->name('login');
Route::post('/login', 'AuthController@login');
Route::get('/logout', 'AuthController@logout')->name('logout');

Route::get('/home', 'PagesController@home'); // temporary

/*
 * We need to override route model binding
 * remove GlobalScopes to reduce query
 */
Route::bind('clean_ticket', function($id) { return App\Ticket::withoutGlobalScopes()->find($id); });

Route::post('/tickets/create', 'TicketsController@store');
Route::get('/tickets/create', 'TicketsController@create');
Route::get('/tickets', 'TicketsController@index');
Route::post('/tickets/{ticket}/update', 'TicketsController@update');
Route::get('/tickets/{ticket}', 'TicketsController@show');

Route::get('/tickets/{clean_ticket}/sub-tickets/create', 'SubTicketsController@create');
Route::post('/tickets/{clean_ticket}/sub-tickets/create', 'SubTicketsController@store');
Route::post('/sub-tickets/{ticket}/update', 'SubTicketsController@update');
Route::get('/sub-tickets/{ticket}', 'SubTicketsController@show');

Route::get('/tickets/{ticket}/attachment/{attachment}', 'TicketAttachmentsController@show');
Route::get('/tickets/{ticket}/attachment/{attachment}/delete', 'TicketAttachmentsController@delete');


Route::post('/tickets/{ticket}/comments/create', 'CommentsController@store'); // Main ticket comments
Route::post('/sub-tickets/{ticket}/comments/create', 'SubTicketCommentsController@store'); // Sub-ticket comments
Route::patch('/comments/{comment}', 'CommentsController@update');

Route::get('/incident-reports/create', 'IncidentReportsController@create');
Route::post('/incident-reports/create', 'IncidentReportsController@store');

/*
 * Tasks are deprecated
 *
 * Route::get('/tasks', 'TasksController@index');
 * Route::post('/tickets/{ticket}/tasks/create', 'TasksController@store');
 * Route::patch('/tasks/{task}/complete', 'TasksController@complete');
 */
Route::get('/surveys', 'SurveysController@index');
Route::get('/surveys/{survey}', 'SurveysController@show');
Route::post('/surveys/{survey}/update', 'SurveysController@update');

Route::get('/users','UsersController@index');
Route::get('/users/create','UsersController@create');
Route::post('/users/create','UsersController@store');
Route::get('/users/{user}/edit','UsersController@edit');
Route::patch('/users/{user}/update', 'UsersController@update');
Route::delete('/users/{user}/delete', 'UsersController@delete');
Route::get('/users/{user}/notifications', 'NotificationsController@index');
Route::delete('/users/{user}/notifications/{notification}', 'NotificationsController@delete');

/*
 * We need to override route model binding
 * so we can query soft deleted entries
 */
Route::bind('all_users', function($id) { return App\User::withTrashed()->find($id); });

Route::get('/users/{all_users}','UsersController@show');
Route::patch('/users/{all_users}/restore', 'UsersController@restore');
