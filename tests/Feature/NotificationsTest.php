<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Notifications\AssignedAsQA;
use App\Notifications\MadeAComment;
use App\Notifications\SurveyWasCreated;
use App\Notifications\TicketWasCreated;
use Illuminate\Support\Facades\Artisan;
use App\Notifications\AssignedAsDeveloper;
use Illuminate\Support\Facades\Notification;
use App\Notifications\TicketStatusWasUpdated;
use App\Notifications\TicketWasEndorsedToRequester;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class NotificationsTest extends TestCase
{
    use DatabaseMigrations;

    public function setUp()
    {
        parent::setUp();

        Notification::fake();
    }

    /** @test */
    public function ticket_subscribers_are_notified_when_ticket_is_created()
    {
    	// Given
    	$creator    = factory('App\User')->create();
        $requester   = factory('App\User')->create();
        $supervisor  = factory('App\User')->create();

        $this->be($creator);
    	
    	// When
    	$ticket = factory('App\Ticket')
    				->create([
    					'creator_id'	=>	$creator->id,
    					'requester_id'	=>	$requester->id,
    					'supervisor_id'	=>	$supervisor->id,
    					'priority_id'	=>	factory('App\Priority')->create()->id,
    					]);

        //Then
    	Notification::assertSentTo($creator, TicketWasCreated::class);
        Notification::assertSentTo($requester, TicketWasCreated::class);
        Notification::assertSentTo($supervisor, TicketWasCreated::class);
    }

    /** @test */
    public function developer_is_notified_when_on_ticket_assignment()
    {
        // Given
        $creator    = factory('App\User')->create();
        $requester   = factory('App\User')->create();
        $supervisor  = factory('App\User')->create();
        
        $this->be($creator);

        // When
        $ticket = factory('App\Ticket')
                    ->create([
                        'creator_id'    =>  $creator->id,
                        'requester_id'  =>  $requester->id,
                        'supervisor_id' =>  $supervisor->id,
                        'priority_id'   =>  factory('App\Priority')->create()->id,
                    ]);

        $developer    = factory('App\User')->create();

        $ticket->update([
                'developer_id'   => $developer->id,
            ]);

        //Then
        Notification::assertSentTo($developer, AssignedAsDeveloper::class);
    }

    /** @test */
    public function qa_is_notified_on_ticket_assignment()
    {
        // Given
        $creator    = factory('App\User')->create();
        $requester   = factory('App\User')->create();
        $supervisor  = factory('App\User')->create();

        $this->be($creator);
        
        // When
        $ticket = factory('App\Ticket')
                    ->create([
                        'creator_id'    =>  $creator->id,
                        'requester_id'  =>  $requester->id,
                        'supervisor_id' =>  $supervisor->id,
                        'priority_id'   =>  factory('App\Priority')->create()->id,
                        ]);

        $qa    = factory('App\User')->create();

        $ticket->update([
                'qa_id'   => $qa->id,
            ]);

        //Then
        Notification::assertSentTo($qa, AssignedAsQA::class);
    }

    /** @test */
    public function subscribers_are_notified_when_ticket_is_updated()
    {
        // Given
        $creator        = factory('App\User')->create();
        $requester      = factory('App\User')->create();
        $supervisor     = factory('App\User')->create();
        $developer      = factory('App\User')->create();
        $qa             = factory('App\User')->create();

        $this->be($creator);

        Artisan::call('db:seed', ['--class' => 'StatusesTableSeeder']);

        // When
        $ticket = factory('App\Ticket')
                    ->create([
                        'creator_id'        =>  $creator->id,
                        'requester_id'      =>  $requester->id,
                        'supervisor_id'     =>  $supervisor->id,
                        'developer_id'      => $developer->id,
                        'qa_id'             => $qa->id,
                        'status_id'         => \App\Status::new()->id,
        ]);

        $ticket->update(['status_id' => \App\Status::inProgress()->id]);

        // Notification::assertSentTo($creator, TicketStatusWasUpdated::class); // No need to notify signed in user
        Notification::assertSentTo($requester, TicketStatusWasUpdated::class);
        Notification::assertSentTo($supervisor, TicketStatusWasUpdated::class);
        Notification::assertSentTo($developer, TicketStatusWasUpdated::class);
        Notification::assertSentTo($qa, TicketStatusWasUpdated::class);
    }

    /** @test */
    public function requester_is_notified_when_status_is_set_to_endorsed_to_requester()
    {
        // Given
        $creator        = factory('App\User')->create();
        $requester      = factory('App\User')->create();
        $supervisor     = factory('App\User')->create();
        $developer      = factory('App\User')->create();
        $qa             = factory('App\User')->create();

        $this->be($creator);

        Artisan::call('db:seed', ['--class' => 'StatusesTableSeeder']);

        // When
        $ticket = factory('App\Ticket')
                    ->create([
                        'creator_id'        =>  $creator->id,
                        'requester_id'      =>  $requester->id,
                        'supervisor_id'     =>  $supervisor->id,
                        'developer_id'      =>  $developer->id,
                        'qa_id'             =>  $qa->id,
                        'status_id'         =>  \App\Status::new()->id,
        ]);

        $ticket->update(['status_id'   => \App\Status::endorsedToRequester()->id]);

        Notification::assertSentTo($requester, TicketWasEndorsedToRequester::class);
    }

    /** @test */
    public function supervisor_and_creator_are_notified_when_a_survey_is_created()
    {
    	// Given
        $requester   = factory('App\User')->create();
        $supervisor  = factory('App\User')->create();
        $creator     = factory('App\User')->create();

        $this->be($creator);

         // When
        $ticket = factory('App\Ticket')
                    ->create([
                        'creator_id'    =>  $creator->id,
                        'requester_id'  =>  $requester->id,
                        'supervisor_id' =>  $supervisor->id,
                    ]);

        $survey1 = factory('App\Survey')->create(['ticket_id' => $ticket->id, 'user_id'	=>	$creator->id]);
        $survey2 = factory('App\Survey')->create(['ticket_id' => $ticket->id, 'user_id'	=>	$supervisor->id]);

        Notification::assertSentTo($creator, SurveyWasCreated::class);
        Notification::assertSentTo($supervisor, SurveyWasCreated::class);
    }

    /** @test */
    public function ticket_subscribers_are_notified_when_comment_is_created()
    {
        // Given
        $requester   = factory('App\User')->create();
        $supervisor  = factory('App\User')->create();
        $creator     = factory('App\User')->create();
        $developer   = factory('App\User')->create();
        $qa          = factory('App\User')->create();

        $this->be($creator);

        // When
        $ticket = factory('App\Ticket')
                    ->create([
                        'creator_id'        =>  $creator->id,
                        'requester_id'      =>  $requester->id,
                        'supervisor_id'     =>  $supervisor->id,
                        'developer_id'      =>  $developer->id,
                        'qa_id'             =>  $qa->id,
        ]);

        $ticket->addComment([
                'body'      => 'This is a comment',
                'user_id'   =>  $creator->id,
                ]);

        // Then
        Notification::assertSentTo($requester, MadeAComment::class);
        Notification::assertSentTo($supervisor, MadeAComment::class);
        Notification::assertSentTo($developer, MadeAComment::class);
        Notification::assertSentTo($qa, MadeAComment::class);
    }
}
