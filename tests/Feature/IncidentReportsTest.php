<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class IncidentReportsTest extends TestCase
{
    use DatabaseMigrations;

    public function setUp()
    {
        parent::setUp();
	}

    /** @test */
    public function an_authenticated_user_can_create_an_incident_report()
    {
    	// Given
    	$user = factory('App\User')->create();
		$this->be($user);

		// When
		$report = factory('App\IncidentReport')->create(['creator_id' => $user->id]);

		// Then
		$this->assertDatabaseHas('incident_reports', ['id' => $report->id]);
    }

    /** @test */
    public function non_authenticated_user_cannot_create_an_incident_report()
    {
        $response = $this->post('/incident-reports/create');

        // Then
        $response->assertRedirect('/login');
    }
    
}
