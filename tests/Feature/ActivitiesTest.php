<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Notification;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ActivitiesTest extends TestCase
{
    use DatabaseMigrations;

    public function setUp()
    {
        parent::setUp();

        Notification::fake();
    }

    /** @test */
    public function it_records_activities_when_a_ticket_is_created()
    {
    	// Given
    	$this->be(factory('App\User')->create());

    	// When
    	$ticket = factory('App\Ticket')->create();

    	// Then
    	$this->assertEquals(1, \App\Activity::all()->count());
    }

    /** @test */
    public function it_records_activities_when_a_comment_is_created_on_a_ticket()
    {
    	// Given
        $user = factory('App\User')->create();
        $this->be($user);

        // When
        $ticket = factory('App\Ticket')->create();

        $comment = $ticket->addComment([
                'body'      => 'This is a comment',
                'user_id'   => $user->id,
            ]);

    	// Then
    	$this->assertDatabaseHas('activities', ['subject_type' => 'App\Comment']);
    	$this->assertDatabaseHas('activities', ['after' => 'This is a comment']);
        $this->assertDatabaseHas('activities', ['type' => 'created_comment']);
    }

    /** @test */
    public function it_records_activities_when_a_comment_is_updated_on_a_ticket()
    {
        // Given
        $user = factory('App\User')->create();
        $this->be($user);

        // When
        $ticket = factory('App\Ticket')->create();

        $comment = $ticket->addComment([
                'body'      => 'This is a comment',
                'user_id'   => $user->id,
            ]);


        $this->assertDatabaseHas('activities', ['subject_type' => 'App\Comment']);
        $this->assertDatabaseHas('activities', ['after' => 'This is a comment']);
        $this->assertDatabaseHas('activities', ['type' => 'created_comment']);

        // Then
        $comment->update(['body' => 'Updated this comment']);

        $this->assertDatabaseHas('activities', ['subject_type' => 'App\Comment']);
        $this->assertDatabaseHas('activities', ['before' => 'This is a comment']);
        $this->assertDatabaseHas('activities', ['after' => 'Updated this comment']);
        $this->assertDatabaseHas('activities', ['type' => 'updated_comment']);
    }
    
    /** @test */
    public function it_records_activities_when_status_field_has_been_updated_on_a_ticket()
    {
        // Given
        $creator        = factory('App\User')->create();
        $requester      = factory('App\User')->create();
        $supervisor     = factory('App\User')->create();
        $developer      = factory('App\User')->create();
        $qa             = factory('App\User')->create();

        $this->be($creator);

        Artisan::call('db:seed', ['--class' => 'StatusesTableSeeder']);

        // When
        $ticket = factory('App\Ticket')
                    ->create([
                        'creator_id'        =>  $creator->id,
                        'requester_id'      =>  $requester->id,
                        'supervisor_id'     =>  $supervisor->id,
                        'developer_id'      => $developer->id,
                        'qa_id'             => $qa->id,
                        'status_id'         => \App\Status::new()->id,
        ]);

        $ticket->update(['status_id' => \App\Status::inProgress()->id]);

        $this->assertDatabaseHas('activities', ['subject_type' => 'App\Ticket']);
        $this->assertDatabaseHas('activities', ['type' => 'updated_status_ticket']);
        $this->assertDatabaseHas('activities', ['before' => 'New', 'after' => 'In progress']);
    }

    /** @test */
    public function it_records_activities_when_developer_is_assigned_to_a_ticket()
    {
        // Given
        $creator        = factory('App\User')->create();
        $requester      = factory('App\User')->create();
        $supervisor     = factory('App\User')->create();
        $developer      = factory('App\User')->create();
        $qa             = factory('App\User')->create();

        $this->be($creator);

        Artisan::call('db:seed', ['--class' => 'StatusesTableSeeder']);

        // When
        $ticket = factory('App\Ticket')
                    ->create([
                        'creator_id'        =>  $creator->id,
                        'requester_id'      =>  $requester->id,
                        'supervisor_id'     =>  $supervisor->id,
                        'developer_id'      =>  null,
                        'status_id'         =>  \App\Status::new()->id,
        ]);

        $ticket->update(['developer_id' => $developer->id]);

        // Then
        $this->assertDatabaseHas('activities', 
                                    ['subject_type' => 'App\Ticket',
                                     'type'         => 'updated_developer_ticket',
                                     'after'        =>  $developer->first_name . ' ' . $developer->last_name,
                                ]);
    }

    /** @test */
    public function it_records_activities_when_qa_is_assigned_to_a_ticket()
    {
        // Given
        $creator        = factory('App\User')->create();
        $requester      = factory('App\User')->create();
        $supervisor     = factory('App\User')->create();
        $developer      = factory('App\User')->create();
        $qa             = factory('App\User')->create();

        $this->be($creator);

        Artisan::call('db:seed', ['--class' => 'StatusesTableSeeder']);

        // When
        $ticket = factory('App\Ticket')
                    ->create([
                        'creator_id'        =>  $creator->id,
                        'requester_id'      =>  $requester->id,
                        'supervisor_id'     =>  $supervisor->id,
                        'qa_id'             =>  null,
                        'status_id'         =>  \App\Status::new()->id,
        ]);

        $ticket->update(['qa_id' => $qa->id]);

        // Then
        $this->assertDatabaseHas('activities', 
                ['subject_type' => 'App\Ticket',
                 'type'         => 'updated_qa_ticket',
                 'after'        =>  $qa->first_name . ' ' . $qa->last_name,
        ]);
    }

    /** @test */
    public function it_records_activities_when_ticket_description_has_been_updated()
    {
        // Given
        $user = factory('App\User')->create();
        $this->be($user);

        // When
        $ticket = factory('App\Ticket')->create();

        $originalDescription = $ticket->description;

        $ticket->update(['description'  => 'Description has been updated!']);

        // Then
        $this->assertDatabaseHas('activities', 
                ['subject_type' => 'App\Ticket',
                 'type'         => 'updated_description_ticket',
                 'before'       =>  $originalDescription,
                 'after'        =>  'Description has been updated!',
        ]);
    }

    /** @test */
    public function it_records_activities_when_ticket_priority_has_been_updated()
    {
        // Given
        $user = factory('App\User')->create();
        $this->be($user);

        Artisan::call('db:seed', ['--class' => 'PrioritiesTableSeeder']);

        // When
        $ticket = factory('App\Ticket')->create(['priority_id'  =>  \App\Priority::normal()->id]);

        $originalPriority = $ticket->priority->title;

        $ticket->update(['priority_id'  =>  \App\Priority::high()->id]);

        // Then
        $this->assertDatabaseHas('activities', [
                'subject_type' => 'App\Ticket',
                'type'         => 'updated_priority_ticket',
                'before'       =>  $originalPriority,
                'after'        =>  \App\Priority::high()->title,
        ]);
    }
    
    /** @test */
    public function it_records_activities_when_request_type_has_been_updated()
    {
        // Given
        $user = factory('App\User')->create();
        $this->be($user);

        Artisan::call('db:seed', ['--class' => 'RequestTypesTableSeeder']);

        // When
        $ticket = factory('App\Ticket')->create(['request_type_id'  =>  \App\RequestType::newFeature()->id]);

        $originalRequestType = $ticket->requestType->title;

        $ticket->update(['request_type_id'  =>  \App\RequestType::maintenance()->id]);

        // Then
        $this->assertDatabaseHas('activities', [
                'subject_type' => 'App\Ticket',
                'type'         => 'updated_request_type_ticket',
                'before'       =>  $originalRequestType,
                'after'        =>  \App\RequestType::maintenance()->title,
        ]);
    }
    
    /** @test */
    public function it_records_activities_when_module_has_been_updated()
    {
        // Given
        $user = factory('App\User')->create();
        $this->be($user);

        Artisan::call('db:seed', ['--class' => 'ModulesTableSeeder']);

        // When
        $ticket = factory('App\Ticket')->create(['module_id'  =>  \App\Module::frontEnd()->id]);

        $originalModule = $ticket->module->title;

        $ticket->update(['module_id'  =>  \App\Module::sugar()->id]);

        // Then
        $this->assertDatabaseHas('activities', [
                'subject_type' => 'App\Ticket',
                'type'         => 'updated_module_ticket',
                'before'       =>  $originalModule,
                'after'        =>  \App\Module::sugar()->title,
        ]);
    }

    /** @test */
    public function it_records_activities_when_target_date_has_been_updated()
    {
        // Given
        $user = factory('App\User')->create();
        $this->be($user);

        // When
        $ticket = factory('App\Ticket')->create();
        $ticket->update(['target_date'  => \Carbon::tomorrow()]);

        // Then
        $this->assertDatabaseHas('activities', [
                'subject_type' => 'App\Ticket',
                'type'         => 'updated_target_date_ticket',
                'before'       =>  \Carbon::now()->format('M d, Y'),
                'after'        =>  \Carbon::tomorrow()->format('M d, Y'),
        ]);
    }
    
    /** @test */
    public function it_records_activities_when_dev_start_date_has_been_updated()
    {
        // Given
        $user = factory('App\User')->create();
        $this->be($user);

        // When
        $ticket = factory('App\Ticket')->create(['dev_start_date' => \Carbon::now()]);
        $ticket->update(['dev_start_date'  => \Carbon::tomorrow()]);

        // Then
        $this->assertDatabaseHas('activities', [
                'subject_type' => 'App\Ticket',
                'type'         => 'updated_dev_start_date_ticket',
                'before'       =>  \Carbon::now()->format('M d, Y'),
                'after'        =>  \Carbon::tomorrow()->format('M d, Y'),
        ]);
    }

    /** @test */
    public function it_records_activities_when_dev_completion_date_has_been_updated()
    {
        // Given
        $user = factory('App\User')->create();
        $this->be($user);

        // When
        $ticket = factory('App\Ticket')->create(['dev_completion_date' => \Carbon::now()]);
        $ticket->update(['dev_completion_date'  => \Carbon::tomorrow()]);

        // Then
        $this->assertDatabaseHas('activities', [
                'subject_type' => 'App\Ticket',
                'type'         => 'updated_dev_completion_date_ticket',
                'before'       =>  \Carbon::now()->format('M d, Y'),
                'after'        =>  \Carbon::tomorrow()->format('M d, Y'),
        ]);
    }

    /** @test */
    public function it_records_activities_when_qa_start_date_has_been_updated()
    {
        // Given
        $user = factory('App\User')->create();
        $this->be($user);

        // When
        $ticket = factory('App\Ticket')->create(['qa_start_date' => \Carbon::now()]);
        $ticket->update(['qa_start_date'  => \Carbon::tomorrow()]);

        // Then
        $this->assertDatabaseHas('activities', [
                'subject_type' => 'App\Ticket',
                'type'         => 'updated_qa_start_date_ticket',
                'before'       =>  \Carbon::now()->format('M d, Y'),
                'after'        =>  \Carbon::tomorrow()->format('M d, Y'),
        ]);
    }

    /** @test */
    public function it_records_activities_when_qa_completion_date_has_been_updated()
    {
        // Given
        $user = factory('App\User')->create();
        $this->be($user);

        // When
        $ticket = factory('App\Ticket')->create(['qa_completion_date' => \Carbon::now()]);
        $ticket->update(['qa_completion_date'  => \Carbon::tomorrow()]);

        // Then
        $this->assertDatabaseHas('activities', [
                'subject_type' => 'App\Ticket',
                'type'         => 'updated_qa_completion_date_ticket',
                'before'       =>  \Carbon::now()->format('M d, Y'),
                'after'        =>  \Carbon::tomorrow()->format('M d, Y'),
        ]);
    }

    /** @test */
    public function it_records_activities_when_user_uploads_a_file()
    {
        // Given
        $user = factory('App\User')->create();
        $this->be($user);

        // When
        $ticket = factory('App\Ticket')->create();

        $attachment = $ticket->attachments()->create([
                    'file_name'     =>  'Attach1.txt',
                    'hash_name'     =>  'LhGyceYSf7w778SZo5bPQOYPiZjRxLzzqedD46se.txt',
                    'extension'     =>  'txt',
                    'storage_path'  =>  '/home/vagrant/Code/trms/storage/app/public/attachments',
                    'creator_id'    =>  auth()->id(),
        ]);

        // Then
        $this->assertDatabaseHas('activities', [
                'subject_type' => 'App\Attachment',
                'type'         => 'created_attachment',
                'after'        =>  $attachment->file_name,
        ]);
    }

     /** @test */
    public function it_records_activities_when_user_removes_an_attachment()
    {
        // Given
        $user = factory('App\User')->create();
        $this->be($user);

        // When
        $ticket = factory('App\Ticket')->create();

        $attachment = $ticket->attachments()->create([
                    'file_name'     =>  'Attach1.txt',
                    'hash_name'     =>  'LhGyceYSf7w778SZo5bPQOYPiZjRxLzzqedD46se.txt',
                    'extension'     =>  'txt',
                    'storage_path'  =>  '/home/vagrant/Code/trms/storage/app/public/attachments',
                    'creator_id'    =>  auth()->id(),
        ]);

        // Then
        $this->assertDatabaseHas('activities', [
                'subject_type' => 'App\Attachment',
                'type'         => 'created_attachment',
                'after'        =>  $attachment->file_name,
        ]);

        $attachment->delete();

        $this->assertDatabaseHas('activities', [
                'subject_type' => 'App\Attachment',
                'type'         => 'deleted_attachment',
                'before'       =>  $attachment->file_name,
        ]);
    }
    
    
}
