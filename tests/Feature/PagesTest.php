<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PagesTest extends TestCase
{
    
    /** @test */
    public function index_page_redirects_to_login()
    {
    	$response = $this->get('/');

    	$response->assertRedirect('/login');
    }

    /** @test */
    public function login_page()
    {
    	$response = $this->get('/login');

    	$response->assertSee('Login');
    }
    
    
}
