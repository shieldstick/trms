<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Notifications\TicketWasCreated;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Notification;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class TicketsTest extends TestCase
{
    use DatabaseMigrations;

    public function setUp()
    {
        parent::setUp();

        Notification::fake();
    }

    /** @test */
    public function it_creates_survey_for_creator_and_supervisor_when_status_is_set_to_resolved()
    {
        // Given
        $creator        = factory('App\User')->create();
        $requester      = factory('App\User')->create();
        $supervisor     = factory('App\User')->create();
        $developer      = factory('App\User')->create();
        $qa             = factory('App\User')->create();

        $this->be($creator);

        Artisan::call('db:seed', ['--class' => 'StatusesTableSeeder']);

        // When
        $ticket = factory('App\Ticket')
                    ->create([
                        'creator_id'        =>  $creator->id,
                        'requester_id'      =>  $requester->id,
                        'supervisor_id'     =>  $supervisor->id,
                        'developer_id'      =>  $developer->id,
                        'qa_id'             =>  $qa->id,
                        'status_id'         =>  \App\Status::new()->id,
        ]);

        $ticket->update(['status_id'   => \App\Status::resolved()->id]);

        $this->assertEquals(2, \App\Survey::all()->count());

        $this->assertDatabaseHas('surveys', ['user_id' => $creator->id]);
        $this->assertDatabaseHas('surveys', ['user_id' => $supervisor->id]);
    }
    
}
