<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Support\Facades\Notification;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CommentsTest extends TestCase
{
    use DatabaseMigrations;

    public function setUp()
    {
        parent::setUp();

        Notification::fake();
    }

    /** @test */
    public function it_has_a_creator()
    {
        $user = factory('App\User')->create();
        $this->be($user);

        $ticket = factory('App\Ticket')->create();

        $comment = $ticket->addComment([
                'body'      => 'This is a comment',
                'user_id'   =>  $user->id,
                ]);

        $this->assertInstanceOf('App\User', $comment->creator()->first());
    }
    
    
}
