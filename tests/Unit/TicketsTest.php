<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Notification;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class TicketsTest extends TestCase
{
    use DatabaseMigrations;

    public function setUp()
    {
        parent::setUp();

        Notification::fake();
    }

    /** @test */
    public function authenticated_users_can_add_comments()
    {
        // Given
        $user = factory('App\User')->create();
        $this->be($user);

        // When
        $ticket = factory('App\Ticket')->create();

        $comment = $ticket->addComment([
                'body'      => 'This is a comment',
                'user_id'   => $user->id,
            ]);

        // Then
        $this->assertDatabaseHas('comments', ['body' => 'This is a comment']);
    }

    /** @test */
    public function it_records_resolved_date_when_status_is_set_to_resolved()
    {
         // Given
        $creator        = factory('App\User')->create();
        $requester      = factory('App\User')->create();
        $supervisor     = factory('App\User')->create();
        $developer      = factory('App\User')->create();
        $qa             = factory('App\User')->create();

        $this->be($creator);

        Artisan::call('db:seed', ['--class' => 'StatusesTableSeeder']);
        
        // When
        $ticket = factory('App\Ticket')
                    ->create([
                        'creator_id'        =>  $creator->id,
                        'requester_id'      =>  $requester->id,
                        'supervisor_id'     =>  $supervisor->id,
                        'developer_id'      =>  $developer->id,
                        'qa_id'             =>  $qa->id,
                        'status_id'         =>  \App\Status::new()->id,
        ]);

        $ticket->update(['status_id' => \App\Status::resolved()->id]);

        // Then
        $this->assertTrue(!is_null($ticket->fresh()->resolved_date));
    }
    

}
