<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Support\Facades\Notification;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class IncidentReportsTest extends TestCase
{
    use DatabaseMigrations;

    public function setUp()
    {
        parent::setUp();

        Notification::fake();
    }

    /** @test */
    public function it_belongs_to_a_creator()
    {
    	$report = factory('App\IncidentReport')->create();

        $this->assertInstanceOf('App\User', $report->creator()->first());
    }

    /** @test */
    public function it_belongs_to_a_requester()
    {
        $report = factory('App\IncidentReport')->create();

        $this->assertInstanceOf('App\User', $report->requester()->first());
    }
    
    /** @test */
    public function it_belongs_to_a_supervisor()
    {
        $report = factory('App\IncidentReport')->create();

        $this->assertInstanceOf('App\User', $report->supervisor()->first());
    }
    
    
}
