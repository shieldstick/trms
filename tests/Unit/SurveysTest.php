<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Notifications\SurveyWasCreated;
use Illuminate\Support\Facades\Notification;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class SurveysTest extends TestCase
{
    use DatabaseMigrations;

    public function setUp()
    {
        parent::setUp();

        Notification::fake();
    }

    /** @test */
    public function it_belongs_to_a_ticket()
    {
        $user = factory('App\User')->create();
        $this->be($user);

        $ticket = factory('App\Ticket')->create(['creator_id' => $user->id]);
        $survey1 = factory('App\Survey')->create(['ticket_id' => $ticket->id, 'user_id' => $user->id]);

         $this->assertInstanceOf('App\Ticket', $survey1->ticket()->first());
    }

    /** @test */
    public function it_has_an_owner()
    {
        $user = factory('App\User')->create();
        $this->be($user);

        $ticket = factory('App\Ticket')->create(['creator_id' => $user->id]);
        $survey1 = factory('App\Survey')->create(['ticket_id' => $ticket->id, 'user_id' => $user->id]);

         $this->assertInstanceOf('App\User', $survey1->owner()->first());
    }

    /** @test */
    public function it_is_not_complete_once_created()
    {
        $user = factory('App\User')->create();
        $this->be($user);

        $ticket = factory('App\Ticket')->create(['creator_id' => $user->id]);
        $survey1 = factory('App\Survey')->create(['ticket_id' => $ticket->id, 'user_id' => $user->id]);

         $this->assertFalse($survey1->completed);
    }
    
}
