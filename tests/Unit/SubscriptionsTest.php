<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Notification;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class SubscriptionsTest extends TestCase
{
	use DatabaseMigrations, DatabaseTransactions;

    public function setUp()
    {
        parent::setUp();

        Notification::fake();
    }
    
    /** @test */
    public function collaborators_can_be_subscribed_to_tickets()
    {
    	// Given
    	$creator        = factory('App\User')->create();
        $requester      = factory('App\User')->create();
        $supervisor     = factory('App\User')->create();
        $developer      = factory('App\User')->create();
        $qa             = factory('App\User')->create();

        $this->be($creator);

    	// When
    	$ticket = factory('App\Ticket')
    				->create([
    					'creator_id'	=> $creator->id,
    					'requester_id'	=> $requester->id,
    					'supervisor_id'	=> $supervisor->id,
    					'developer_id'	=> $developer->id,
    					'qa_id'	        => $qa->id,
    					]);
    	
    	// Then
    	$this->assertDatabaseHas('ticket_subscriptions', [
    			'user_id' => $creator->id,
    			'ticket_id'	=> $ticket->id,
    			]);
    }

    /** @test */
    public function collaborators_without_developer_and_qa_can_be_subscribed_to_tickets()
    {
    	// Given
    	$creator    = factory('App\User')->create();
        $requester   = factory('App\User')->create();
        $supervisor  = factory('App\User')->create();
    	
        $this->be($creator);

    	// When
    	$ticket = factory('App\Ticket')
    				->create([
    					'creator_id'	=> $creator->id,
    					'requester_id'	=> $requester->id,
    					'supervisor_id'	=> $supervisor->id,
    					]);
    	
    	// Then
    	$this->assertDatabaseHas('ticket_subscriptions', [
    			'user_id' => $creator->id,
    			'ticket_id'	=> $ticket->id,
    			]);

        $this->assertEquals(3, $ticket->subscriptions()->count());
    }

    /** @test */
    public function developer_and_qa_can_be_subscribed_to_existing_ticket()
    {
        $creator      = factory('App\User')->create();
        $requester    = factory('App\User')->create();
        $supervisor   = factory('App\User')->create();

        $this->be($creator);

        $ticket = factory('App\Ticket')
                    ->create([
                        'creator_id'    => $creator->id,
                        'requester_id'  => $requester->id,
                        'supervisor_id' => $supervisor->id,
                        ]);

        $this->assertEquals(3, $ticket->subscriptions()->count());

        $developer     = factory('App\User')->create();
        $qa            = factory('App\User')->create();

        $ticket->update([
                'developer_id'   => $developer->id,
                'qa_id'          => $qa->id,
            ]);

        $this->assertEquals(5, $ticket->fresh()->subscriptions()->count());
    }
    
    
}
