<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="/css/styles.css">
        <title>Ticket Request &amp; Management System</title>
        <!-- Scripts -->
        <script>
            window.App = {!! json_encode([
                'csrfToken' => csrf_token(),
                'signedIn' => Auth::check()
            ]) !!};
        </script>
    </head>
    <body>
    <div id="application">
        <div class="container" id="app">
            <div class="col-md-6 col-md-offset-3">
                <div class="row" style="text-align:center;">
                    <img src="/images/comgateway_logo1.jpg"/>
                </div>
            </div>
            <div class="row">&nbsp;</div>
            <div class="row">&nbsp;</div>
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <div class="panel panel-default">
                        <div class="panel-heading" style="text-align: center;">
                            <strong>Ticket Request &amp; Management System</strong>
                        </div>
                        <div class="panel-body">

                            <form class="form-horizontal" method="POST" action="/login">
                                {{ csrf_field() }}
                                <div class="input-group m-b-1">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
                                    <input type="email" class="form-control" id="email" name="email" placeholder="Email" value="{{ old('email') }}" required>
                                </div>

                                <div class="input-group">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                                    <input type="password" class="form-control" id="pwd" name="password" placeholder="Password" required>
                                </div>

                                <div class="level">&nbsp;</div>

                                <div class="level">
                                    <a href="/forgot-password" class="flex subtle-link">Forgot Password?</a>
                                    <button type="submit" class="btn btn-danger">Login</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">&nbsp;</div>
            <div class="row">&nbsp;</div>
            <div class="row">&nbsp;</div>
            <div class="row">&nbsp;</div>
            <div class="row">&nbsp;</div>
            <div class="col-md-6 col-md-offset-3">
                <div class="row" style="text-align: center; color: #999; font-size: 9pt;">
                    Copyright &copy; {{ (Carbon::now())->format('Y') }} - comGateway Information Technology
                </div>
            </div>
            <flash message="{{ session('flash_message') }}" level="{{ session('flash_level') }}"></flash>
        </div>
    </div>
    </body>
        <script>window.Laravel={!! json_encode(['csrfToken' => csrf_token()]) !!}</script>
        <script src="https://code.jquery.com/jquery-3.2.1.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script src="/js/app.js"></script>
</html>
