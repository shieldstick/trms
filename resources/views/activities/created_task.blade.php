<div class="col-md-12 activity-list">
	<span>
		<strong>{{$activity->creator->first_name . ' ' . $activity->creator->last_name}}</strong>
	</span>
	<span>
		created a task on 
		<i> 
			{{ $activity->created_at->timezone(auth()->user()->timezone)->format('M d Y - h:i a') }}
		</i>
	</span>
</div>