<div class="col-md-12 activity-list">
	<span>
		<strong>{{$activity->creator->first_name . ' ' . $activity->creator->last_name}}</strong>
	</span>
	<span>
		added a comment on 
		<i> 
			{{ $activity->created_at->timezone(auth()->user()->timezone)->format('M d Y - h:i a') }}
		</i>
	</span>
	@if($activity->after)
		<p style="margin: 10px; padding-left: 20px; border-left: 3px solid #aaa;">
			<span>
			<i> 
				{!! $activity->after !!}
			</i>
			</span>
		</p>
	@endif
</div>