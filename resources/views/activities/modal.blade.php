<!-- Modal -->
<div class="modal fade" id="activities-modal" role="dialog">
  <div class="modal-dialog">
  <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span></button>
        <h4 class="modal-title" style="color:#d43f3a;">
          Activity Logs
        </h4>
      </div>

      <div class="modal-body" style="overflow-y: scroll; height: 450px;">
        @if(empty($ticket->activity))
        <span style="color: #aaaaaa">Nothing to show yet..</span>
        @else
          @foreach($ticket->activity as $activity)
            @include('activities.'.$activity->type)
          @endforeach
        @endif
      </div>

      <div class="modal-footer">
        
      </div>
    </div>

  </div>
</div>