<div class="col-md-12 activity-list">
	<span>
		<strong>{{$activity->creator->first_name . ' ' . $activity->creator->last_name}}</strong>
	</span>
	<span>
		updated <strong>QA</strong> on 
		<i> 
			{{ $activity->created_at->timezone(auth()->user()->timezone)->format('M d Y - h:i a') }}
		</i>
	</span>
	
	@if($activity->after)
		<p style="margin: 10px; padding-left: 20px; border-left: 3px solid #aaa;">
			<span>
				<strong>New: </strong>
			</span>
			<span>
			<i> 
				{!! $activity->after !!}
			</i>
			</span>
		</p>
	@endif

	@if($activity->before)
		<p style="margin: 10px; padding-left: 20px; border-left: 3px solid #aaa;">
			<span>
				<strong>Old: </strong>
			</span>
			<span>
				<i>{!! $activity->before !!}</i>
			</span>
		</p>
	@endif
</div>