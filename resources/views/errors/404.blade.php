<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="/css/styles.css">
        <title>Ticket Request &amp; Management System</title>
    </head>
    <body>
        <div class="container" id="app">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <div class="panel panel-danger">
                        <div class="panel-body">
                            <h2 style="text-align:center;">
                            	<strong style="color:red;">Error 404!</strong>&nbsp;&nbsp; Page not found!
                            </h2>
                            <br/>
                            <br/>
                            <h5 style="color: #aaa; text-align:center;">
                                If you were not expecting this issue, please contact your system administrator.
                            </h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
        <script src="https://code.jquery.com/jquery-3.2.1.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script src="/js/app.js"></script>
</html>