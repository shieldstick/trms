<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link rel="stylesheet" href="/css/app.css">
        <link rel="stylesheet" href="/css/fileinput.css">
        <link rel="stylesheet" href="/css/datetimepicker.css">
        <link rel="stylesheet" href="/css/styles.css">
        <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.3.1/css/buttons.dataTables.min.css">
        <title>Ticket Request &amp; Management System</title>
        <script>
            window.App = {!! json_encode([
                'csrfToken'             => csrf_token(),
                'auth_user_id'          => auth()->user()->id,
                'auth_user_name'        => auth()->user()->first_name,
                'auth_user_timezone'    =>  auth()->user()->timezone,
                'signedIn'              => Auth::check()
            ]) !!};
        </script>
    </head>
    <body>
        <div id="application">
           <!-- Navbar -->
            @include('partials.navbar')
            <div id="app">
                 <!-- Sidebar -->
                @include('partials.sidebar')
                <div id="content">
                    @yield('content')
                </div>
                <flash message="{{ session('flash_message') }}" level="{{ session('flash_level') }}"></flash>
            </div>
        </div>
         <script src="/js/app.js"></script>
        <script src="/js/fileinput.js"></script>
        <script src="/js/selectpicker.js"></script>
        <script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.15/js/dataTables.bootstrap.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.3.1/js/dataTables.buttons.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.3.1/js/buttons.html5.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.3.1/js/buttons.flash.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
        {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script> --}}
        <script src="/js/scripts.js"></script>
    </body>
</html>
