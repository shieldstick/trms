<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container-fluid">
        <!-- Logo -->
        @include('partials.nav-brand')
        <!-- Menu Items -->
        @include('partials.nav-menu')
    </div>
</nav>