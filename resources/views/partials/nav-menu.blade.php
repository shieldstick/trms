<div class="collapse navbar-collapse" id="main_navbar" style="padding-right: 20px;">
    <ul class="nav navbar-nav navbar-right">
        <!-- Dropdown Menu -->
        <notifications></notifications>
        <li>
            <a href="/logout" class="menu-items">
                <span class="glyphicon glyphicon-off"></span> Logout
            </a>
        </li>
    </ul>
</div>