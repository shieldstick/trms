<li>
    <a href="#" data-toggle="collapse" class="sidebar-title">
        <span class="glyphicon glyphicon-file"></span>&nbsp;&nbsp;Incident Reports
    </a>
    <div id="collapse1" class="collapse-group">
        <ul class="list-group">
            <li class="sidebar-list-collapse">
                <a href="/incident-reports/create">
                    <span class="glyphicon glyphicon-plus-sign" style="text-align:center;"></span>
                    &nbsp;&nbsp;Create Incident Report
                </a>
            </li>
        </ul>
    </div>
</li>