<li>
    <a href="#" data-toggle="collapse" class="sidebar-title">
        <span class="glyphicon glyphicon-file"></span>&nbsp;&nbsp;Tickets
    </a>
    <div id="collapse1" class="collapse-group">
        <ul class="list-group">
            <li class="sidebar-list-collapse">
                <a href="/tickets/create">
                    <span class="glyphicon glyphicon-plus-sign" style="text-align:center;"></span>
                    &nbsp;&nbsp;Create A Ticket
                </a>
            </li>
            <li class="sidebar-list-collapse">
                <a href="/tickets">
                    <span class="glyphicon glyphicon-th-list" style="text-align:center;"></span>
                    &nbsp;&nbsp;All Tickets
                </a>
            </li>
            <li class="sidebar-list-collapse">
                <a href="/tickets?open=true">
                    <span class="glyphicon glyphicon-screenshot" style="text-align:center;"></span>
                    &nbsp;&nbsp;Open Tickets
                </a>
            </li>
            <li class="sidebar-list-collapse">
                <a href="/tickets?resolved=true">
                    <span class="glyphicon glyphicon-ok"></span>
                    &nbsp;&nbsp;Resolved Tickets
                </a>
            </li>
            <li class="sidebar-list-collapse">
                <a href="/tickets?involved={{ auth()->id() }}">
                    <span class="glyphicon glyphicon-tasks"></span>
                    &nbsp;&nbsp;My Tickets
                </a>
            </li>
        </ul>
    </div>
</li>