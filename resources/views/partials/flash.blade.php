<div class="col-lg-12 flash-message">
	<div class="alert {{ Session::has('flash_message_error') ? 'alert-danger' : 'alert-success'}} fade in" style="width: 100%;">
		{{ session('flash_message') }}
		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	</div>
</div>