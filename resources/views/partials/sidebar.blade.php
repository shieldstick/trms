<div id="sidebar-wrapper">
	<ul class="sidebar-nav">
	    @include('partials.sidebars.tickets')
        @include('partials.sidebars.incident-reports')
        @include('partials.sidebars.surveys')
        @include('partials.sidebars.users')
    </ul>
</div>

