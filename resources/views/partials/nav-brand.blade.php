<div class="navbar-header">
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#main_navbar">
        <span class="glyphicon glyphicon-cog"></span>
    </button>
    <a type="button" class="btn btn-default navbar-btn pull-left btn-sm" id="sidebar-toggle">
        <span class="glyphicon glyphicon-menu-hamburger"></span>
    </a>
    <a href="/tickets" class="navbar-brand">
    	TRMS
    </a>
</div>