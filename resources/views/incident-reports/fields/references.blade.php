<label class="control-label col-md-2" for="reference">Reference / URL / Suite ID:</label>
<div class="col-md-10">
    <textarea name="reference" 
              class="form-control" 
              rows="3"
              placeholder="Add some reference, urls and/or suite IDs..">{{ old('reference') ?? '' }}</textarea>
    {!! $errors->first('reference', '<span class="help-block">:message</span>') !!}
</div>