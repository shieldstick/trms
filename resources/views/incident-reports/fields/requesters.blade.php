<label class="control-label col-md-2" for="requester">Requester: *</label>
<div class="col-md-4">
    <select class="selectpicker form-control" data-live-search="true" data-dropup-auto="false" style="width:100%;" id="requester" name="requester">
    <option value="">&nbsp;</option>
        @foreach($users as $requester)
            <option value="{{ $requester->id }}" {{ old('requester') == $requester->id ? 'selected' : '' }}>
                {{ $requester->first_name . ' ' . $requester->last_name }}
            </option>
        @endforeach
    </select>
    {!! $errors->first('requester', '<span class="help-block">:message</span>') !!}
</div>