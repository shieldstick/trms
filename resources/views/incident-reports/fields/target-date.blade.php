<label class="control-label col-md-2" for="target-date">Target Date: </label>
<div class="col-md-4">
    <div id="target-date" class="input-group date">
        <input class="form-control" 
               type="text"
               id="target_date"
               value="{{ old('target_date') ?? '' }}"
               name="target_date"/>
            <span class="input-group-addon">
            <span class="glyphicon glyphicon-calendar"></span>
        </span>
    </div>
</div>