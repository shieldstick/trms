<label class="control-label col-md-2" for="client">Client: *</label>
<div class="col-md-4">
    <select class="selectpicker form-control" data-live-search="true" data-dropup-auto="false" style="width:100%;" id="client" name="client" required>
    <option value="">&nbsp;</option>
        @foreach($clients as $client)
            <option value="{{ $client->id }}" {{ old('client') == $client->id ? 'selected' : '' }}>
                {{ $client->title }}
            </option>
        @endforeach
    </select>
    {!! $errors->first('client', '<span class="help-block">:message</span>') !!}
</div>