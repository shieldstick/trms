<label class="control-label col-md-2" for="subject">Subject: *</label>
<div class="col-md-10">
    <input type="text" 
    		class="form-control" 
    		id="subject" 
    		name="subject" 
    		value="{{ old('subject') ?? '' }}" 
    		placeholder="Add a subject to your report.." 
    		required>
    {!! $errors->first('subject', '<span class="help-block">:message</span>') !!}
</div>