<label class="control-label col-md-2" for="details">Details: *</label>
<div class="col-md-10">
    <textarea name="details" 
              class="form-control" 
              rows="5"
              required
              placeholder="Add details to your your report here..">{{ old('details') ?? '' }}</textarea>
    {!! $errors->first('details', '<span class="help-block">:message</span>') !!}
</div>