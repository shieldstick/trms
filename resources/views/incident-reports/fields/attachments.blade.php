<label class="control-label col-md-2" for="attachments">Attachments:</label>
<div class="col-md-10">
    <input  type="file" 
            id="attachments"
            name="attachments[]"
            data-show-upload="false"
            data-show-preview="false"
            multiple
            class="form-control file file-loading">
    <div class="hint">
        <br><span>**You can select multiple files by holding CTRL key on your keyboard**</span>
    </div>
    {!! $errors->first('attachments', '<span class="help-block">:message</span>') !!}
</div>