<label class="control-label col-md-2" for="start-time">Start Time: *</label>
<div class="col-md-4">
    <div id="start-time" class="input-group date">
        <input class="form-control" 
               type="text"
               id="start_time"
               value="{{ old('start_time') ?? '' }}"
               name="start_time"/>
            <span class="input-group-addon">
            <span class="glyphicon glyphicon-time"></span>
        </span>
    </div>
</div>