<label class="control-label col-md-2" for="access-level">Priority: *</label>
<div class="col-md-4">
    <select class="selectpicker form-control" data-live-search="true" data-dropup-auto="false" style="width:100%;" id="priority" name="priority" required>
        <option value="">&nbsp;</option>
        @foreach($priorities as $priority)
            <option value="{{ $priority->id }}"{{ old('priority') == $priority->id ? ' selected ' : '' }}>
            	{{ $priority->title }}
            </option>
        @endforeach
    </select>
    {!! $errors->first('priority', '<span class="help-block">:message</span>') !!}
</div>