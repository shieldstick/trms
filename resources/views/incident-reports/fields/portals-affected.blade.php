<label class="control-label col-md-2" for="portals_affected">Affected portals and tools: *</label>
<div class="col-md-10">
    <textarea name="portals_affected" 
              class="form-control" 
              rows="3"
              required
              placeholder="Enumerate the portals and/or tools that were affected..">{{ old('portals_affected') ?? '' }}</textarea>
    {!! $errors->first('portals_affected', '<span class="help-block">:message</span>') !!}
</div>