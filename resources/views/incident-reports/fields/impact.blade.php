<label class="control-label col-md-2" for="impact">Impact to Business: *</label>
<div class="col-md-10">
    <textarea name="impact" 
              class="form-control" 
              rows="3"
              required
              placeholder="What was the impact to the business of this issue?">{{ old('impact') ?? '' }}</textarea>
    {!! $errors->first('impact', '<span class="help-block">:message</span>') !!}
</div>