<label class="control-label col-md-2" for="module">Module: *</label>
<div class="col-md-4">
    <select class="selectpicker form-control" data-live-search="true" data-dropup-auto="false" style="width:100%;" id="module" name="module" required>
    <option value="">&nbsp;</option>
        @foreach($modules as $module)
            <option value="{{ $module->id }}" {{ old('module') == $module->id ? 'selected' : '' }}>
                {{ $module->title }}
            </option>
        @endforeach
    </select>
    {!! $errors->first('module', '<span class="help-block">:message</span>') !!}
</div>