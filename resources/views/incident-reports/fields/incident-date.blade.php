<label class="control-label col-md-2" for="incident-date">Date of Incident: *</label>
<div class="col-md-4">
    <div id="incident-date" class="input-group date">
        <input class="form-control" 
               type="text"
               id="incident_date"
               value="{{ old('incident_date') ?? '' }}"
               name="incident_date"/>
            <span class="input-group-addon">
            <span class="glyphicon glyphicon-calendar"></span>
        </span>
    </div>
</div>