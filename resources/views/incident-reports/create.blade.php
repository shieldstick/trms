@extends('partials.master')
@section('content')

<div class="container col-md-8 col-md-offset-2">
	<div class="panel panel-danger ticket-creation-panel">
        <div class="panel-heading level ticket-creation-panel-heading">
            <span class="flex">
                <strong>Creating an Incident Report</strong>
            </span>
            <a href="#" style="color:white;">Back to Incident Reports List</a>
		</div>
        <div class="panel-body ticket-creation-panel-body">
	        @include('incident-reports.form')
		</div>
	</div>
</div>

@endsection