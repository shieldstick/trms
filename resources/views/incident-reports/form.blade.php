<form class="form-horizontal" method="POST" action="/incident-reports/create" enctype="multipart/form-data">
    {{ csrf_field() }}

    <div class="form-group">
    	@include('incident-reports.fields.subject')
    </div>

    <div class="form-group">
    	@include('incident-reports.fields.priorities')
    </div>

    <div class="form-group">
    	@include('incident-reports.fields.clients')
    	@include('incident-reports.fields.modules')
    </div>

    <div class="form-group">
    	@include('incident-reports.fields.requesters')
    	@include('incident-reports.fields.target-date')
    </div>

    <hr>

    <div class="form-group">
    	@include('incident-reports.fields.incident-date')
    	@include('incident-reports.fields.start-time')
    </div>

    <div class="form-group">
    	@include('incident-reports.fields.portals-affected')
    </div>

    <hr>

    <div class="form-group">
    	@include('incident-reports.fields.details')
    </div>

    <hr>

    <div class="form-group">
    	@include('incident-reports.fields.impact')
    </div>
    <div class="form-group">
    	@include('incident-reports.fields.references')
    </div>

    <div class="form-group">
    	@include('incident-reports.fields.attachments')
    </div>

    <div class="level">
        <span class="flex">&nbsp;</span>
        <a href="/tickets" type="submit" class="btn btn-default">Cancel</a>
        &nbsp;&nbsp;
        <button type="submit" class="btn btn-danger">Create</button>
    </div>
</form>