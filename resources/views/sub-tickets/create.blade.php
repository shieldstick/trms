@extends('partials.master')
@section('content')

<div class="container col-md-8 col-md-offset-2">
	<div class="panel panel-danger ticket-creation-panel">
        <div class="panel-heading level ticket-creation-panel-heading">
            <span class="flex">
                <strong>Creating a Sub-Ticket for Ticket# {{ $ticket->identification }}</strong>
            </span>
            <a href="/tickets/{{ $ticket->id }}" style="color:white;">Back to Main Ticket</a>
		</div>
        <div class="panel-body ticket-creation-panel-body">
            <form class="form-horizontal" 
                  method="POST" 
                  action="/tickets/{{ $ticket->id }}/sub-tickets/create" 
                  enctype="multipart/form-data">

                {{ csrf_field() }}

                <div class="form-group">
                    <label class="control-label col-md-2" for="subject">Subject: *</label>
                    <div class="col-md-10">
                        <input type="text" 
                                   class="form-control" 
                                   id="subject" 
                                   name="subject"
                                   value="{{ $ticket->subject }}"
                                   placeholder="Add a subject to your ticket.." 
                                   disabled>
                        
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-md-2" for="access-level">Priority: *</label>
                    <div class="col-md-4">
                        <select class="selectpicker form-control" style="width:100%;" id="priority" name="priority" disabled>
                            <option value="{{ $ticket->priority_id }}">{{ $ticket->priority->title }}</option>
                        </select>
                    </div>
                    <label class="control-label col-md-2" for="access-level">Request type: *</label>
                    <div class="col-md-4">
                        <select class="selectpicker form-control" style="width:100%;" id="request-type" name="request_type" disabled>
                            <option value="{{ $ticket->request_type_id }}">{{ $ticket->requestType->title }}</option>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-md-2" for="client">Client: *</label>
                    <div class="col-md-4">
                        <select class="selectpicker form-control" style="width:100%;" id="client" name="client" disabled>
                            <option value="{{ $ticket->client_id }}">
                                {{ $ticket->client->title }}
                            </option>
                        </select>
                    </div>
                    <label class="control-label col-md-2 {{ $errors->has('module') ? 'has-error' : '' }}" for="module">Module: *</label>
                    <div class="col-md-4">
                        <select class="selectpicker form-control" data-live-search="true" data-dropup-auto="false" style="width:100%;" id="module" name="module" required>
                        <option value=""> &nbsp; </option>
                            @foreach($modules as $module)
                                <option value="{{ $module->id }}"
                                        {{ old('module') == $module->id ? 'selected' : '' }}>
                                    {{ $module->title }}
                                </option>
                            @endforeach
                        </select>
                        {!! $errors->first('module', '<span class="help-block">:message</span>') !!}
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-2" for="requester">Requester: *</label>
                    <div class="col-md-4">
                        <select class="selectpicker form-control" style="width:100%;" id="requester" name="requester" disabled>
                            <option value="{{ $ticket->requester_id }}">
                                {{ $ticket->requester->full_name }}
                            </option>
                        </select>
                    </div>
                    <label class="control-label col-md-2" for="target-date">Target Date: </label>
                    <div class="col-md-4">
                        <div id="target-date-value" data-value="{{ !is_null($ticket->target_date) ? $ticket->target_date->toDateString() : '' }}"></div>
                        <div id="target-date" class="input-group date">
                            <input class="form-control" 
                                   type="text"
                                   id="target_date"
                                   name="target_date"
                                   disabled/>
                                <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                    </div>
                </div>

                <div class="form-group {{ $errors->has('description') ? 'has-error' : '' }}">
                    <label class="control-label col-md-2" for="client">Details: *</label>
                    <div class="col-md-10">
                        <textarea name="description" 
                                  class="form-control" 
                                  rows="5"
                                  required
                                  placeholder="Add some details to your ticket..">{{ old('description') ?? '' }}</textarea>
                        {!! $errors->first('description', '<span class="help-block">:message</span>') !!}
                    </div>
                </div>
                <div class="form-group {{ $errors->has('attachments') ? 'has-error' : '' }}">
                    <label class="control-label col-md-2" for="attachments">Attachments:</label>
                    <div class="col-md-10">
                        <input  type="file" 
                                id="attachments"
                                name="attachments[]"
                                data-show-preview="false"
                                data-show-upload="false"
                                multiple
                                class="form-control file file-loading">
                        <div class="hint">
                            <br><span>**You can select multiple files by holding CTRL key on your keyboard**</span>
                        </div>
                        {!! $errors->first('attachments', '<span class="help-block">:message</span>') !!}
                    </div>
                </div>

                <div class="level">
                    <span class="flex">&nbsp;</span>
                    <a href="/tickets/{{ $ticket->id }}" class="btn btn-default">Cancel</a>
                    &nbsp;&nbsp;
                    <button type="submit" class="btn btn-danger">Create</button>
                </div>
            </form>
        </div>
	</div>
</div>

@endsection