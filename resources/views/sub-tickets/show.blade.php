@extends('partials.master')
@section('content')

<div class="container col-md-8">
    @if($ticket->status->title == 'Resolved' || $ticket->status->title == 'Cancelled')
        @include('sub-tickets.panels.locked')
    @else
        @include('sub-tickets.panels.form')
    @endif
</div>

<div class="container col-md-4">
	@include('sub-tickets.panels.attachments')
    <comments :data="{{ $ticket->comments }}"></comments>
</div>

@include('activities.modal')

<div class="container level">&nbsp;</div>
@endsection