@extends('partials.master')
@section('content')

<div class="container col-md-12">

	<div class="level">&nbsp;</div>
    <div class="level">&nbsp;</div>
    <div class="table-survey">
	<table id="surveys-table" class="display table table-hover" width="100%">
	    <thead>
	        <tr>
	            <th style="text-align: center; width: 8%;">Ticket #</th>
	            <th style="text-align: center; width: 12%;">Ticket Subject</th>
	            <th style="text-align: center; width: 15%;">Survey Owned By</th>
	            <th style="text-align: center; width: 20%; word-wrap: normal;">Q1: Was your ticket completed on time?</th>
	            <th style="text-align: center; width: 20%; word-wrap: normal;">Q2: Were there no issues found when the ticket was endorsed to you?</th>
	            <th style="text-align: center; width: 20%; word-wrap: normal;">Q3: Was a developer assigned to your request within 2 weeks?</th>
	            <th style="text-align: center; width: 5%;">Survey Completed</th>
	        </tr>
	    </thead>
	    <tbody>
	    	@foreach($surveys as $survey)
	    		@can('view', $survey)
	    		<tr class="{{ ($survey->completed) ? 'success' : 'danger' }}">
	    			<td style="text-align: left;">{{ $survey->ticket->identification }}</td>
	    			<td style="text-align: left;">
	    				<a href="/surveys/{{$survey->id}}">{{ $survey->ticket->subject }}</a>
	    			</td>
	    			<td style="text-align: center;">{{ $survey->owner->first_name . ' ' . $survey->owner->last_name }}</td>
	    			<td style="text-align: center;">{{ ($survey->q1) ? 'Yes' : 'No' }}</td>
	    			<td style="text-align: center;">{{ ($survey->q2) ? 'Yes' : 'No' }}</td>
	    			<td style="text-align: center;">{{ ($survey->q3) ? 'Yes' : 'No' }}</td>
	    			<td style="text-align: center;">{{ ($survey->completed) ? 'Yes' : 'No' }}</td>
	    		</tr>
	    		@endcan
	    	@endforeach
		</tbody>
	</table>
    </div>
</div>

@endsection