@extends('partials.master')
@section('content')

<div class="container col-md-7">
    @include('tickets.panels.locked', ['ticket' => $survey->ticket])
</div>

<div class="container col-md-5">
	@if(!$survey->completed)
    	@include('surveys.survey-form')
    @else
    	@include('surveys.survey-panel')
    @endif
</div>

@endsection