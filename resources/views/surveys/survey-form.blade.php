<div class="panel panel-primary">
    <div class="panel-heading level">
    	<div class="flex">
    		<span class="glyphicon glyphicon-question-sign"></span> Survey
    	</div>
    	<div>
    		<a href="/surveys" style="color:white;">Surveys List</a>
    	</div>
    </div>
    <div class="panel-body">
    	<div class="level">&nbsp;</div>
    	<div class="level">Hey {{$survey->owner->first_name}}! Please answer this quick survey below: </div>
    	<div class="level">&nbsp;</div>
    	<form method="POST" action="/surveys/{{ $survey->id }}/update">
    		{{ csrf_field() }}
	    	<table class="table table-bordered">
	    		<tr>
	    			<td style="background-color:#eee" colspan="2">Q1: Was your request completed on time? *</td>
	    		</tr>
	    		<tr>
	    			<td><input type="radio" name="q1" value="1" required> Yes</td>
	    			<td><input type="radio" name="q1" value="0" required> No</td>
	    		</tr>
	    		<tr>
	    			<td style="background-color:#eee" colspan="2">Q2: Were there no issues found when the ticket was endorsed to you? *</td>
	    		</tr>
	    		<tr>
	    			<td><input type="radio" name="q2" value="1" required> Yes</td>
	    			<td><input type="radio" name="q2" value="0" required> No</td>
	    		</tr>
	    		<tr>
	    			<td style="background-color:#eee" colspan="2">Q3: Was a developer assigned to your request within 2 weeks? *</td>
	    		</tr>
	    		<tr>
	    			<td><input type="radio" name="q3" value="1" required> Yes</td>
	    			<td><input type="radio" name="q3" value="0" required> No</td>
	    		</tr>
	    	</table>
	    	<div class="level">
                <span class="flex">&nbsp;</span>
                <a href="/surveys" type="submit" class="btn btn-default">Cancel</a>
                &nbsp;&nbsp;
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
    	</form>
	</div>
</div>