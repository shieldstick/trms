<div class="panel panel-primary">
    <div class="panel-heading level">
    	<div class="flex">
    		<span class="glyphicon glyphicon-question-sign"></span> Survey
    	</div>
    	<div>
    		<a href="/surveys" style="color:white;">Surveys List</a>
    	</div>
    </div>
    <div class="panel-body">
    	<div class="level">&nbsp;</div>
    	<div class="level">Hey {{$survey->owner->first_name}}! Thank you for answering this survey! </div>
    	<div class="level">&nbsp;</div>
    	<table class="display table table-bordered table-hover">
            <tbody>
        		<tr>
        			<td style="background-color:#eee" >Q1: Was your request completed on time? *</td>
        		</tr>
        		<tr>
        			<td><strong>Your answer:</strong> {{ ($ticket->q1) ? 'Yes' : 'No' }}</td>
        		</tr>
        		<tr>
        			<td style="background-color:#eee" >Q2: Were there no issues found when the ticket was endorsed to you? *</td>
        		</tr>
        		<tr>
        			<td><strong>Your answer:</strong> {{ ($ticket->q2) ? 'Yes' : 'No' }}</td>
        		</tr>
        		<tr>
        			<td style="background-color:#eee" >Q3: Was a developer assigned to your request within 2 weeks? *</td>
        		</tr>
        		<tr>
        			<td><strong>Your answer:</strong> {{ ($ticket->q2) ? 'Yes' : 'No' }}</td>
        		</tr>
            </tbody>
    	</table>
	</div>
</div>