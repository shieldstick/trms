@extends('partials.master')
@section('content')

<div class="container col-md-8 col-md-offset-2">

	<div class="panel panel-default">
		<div class="panel-heading level">
        	<span class="flex"><strong>Create User Account</strong></span>
        	&nbsp; <a href="/users">Back to list</a> &nbsp;
		</div>
		<div class="panel-body">
			<form method="post" action="/users/create">
                {{ csrf_field() }}
                <div class="form-group row">
                	<div class="col-md-6 {{ $errors->has('first_name') ? 'has-error' : '' }}">
	                	<label for="first_name">First name: *</label>
	                    <input type="first_name" 
	                           class="form-control" 
	                           id="first_name" 
	                           name="first_name"
	                           value="{{ old('first_name') }}" 
	                           required
	                    >
	                    {!! $errors->first('first_name', '<span class="help-block">:message</span>') !!}
                    </div>
                    <div class="col-md-6 {{ $errors->has('last_name') ? 'has-error' : '' }}">
	                    <label for="last_name">Last name: *</label>
	                    <input type="last_name" 
	                           class="form-control" 
	                           id="last_name" 
	                           name="last_name"
	                           value="{{ old('last_name') }}" 
	                           required
	                    >
	                    {!! $errors->first('last_name', '<span class="help-block">:message</span>') !!}
                    </div>
				</div>
				<div class="form-group row">
                	<div class="col-md-6 {{ $errors->has('email') ? 'has-error' : '' }}">
	                    <label for="email">Email address: *</label>
	                    <input type="email" 
	                    	   class="form-control" 
	                    	   id="email" 
	                    	   name="email"
	                    	   value="{{ old('email') }}" 
	                    	   required
	                    >
	                    {!! $errors->first('email', '<span class="help-block">:message</span>') !!}
                    </div>
                    <div class="col-md-6 {{ $errors->has('timezone') ? 'has-error' : '' }}">
                    	@include('users.timezone-select')
                    	{!! $errors->first('timezone', '<span class="help-block">:message</span>') !!}
					</div>
                </div>
                <div class="form-group row">
                	<div class="col-md-12 {{ $errors->has('password') ? 'has-error' : '' }}">
	                    <label for="password">Password: *</label>
	                    <input type="password" 
	                    	   class="form-control" 
	                    	   id="password" 
	                    	   name="password"
	                    	   required
	                    >
	                    {!! $errors->first('password', '<span class="help-block">:message</span>') !!}
                    </div>
                </div>
                <div class="form-group row">
                	<div class="col-md-12">
	                    <label for="confirm-password">Confirm Password: *</label>
	                    <input type="password" 
	                    	   class="form-control" 
	                    	   id="confirm-password" 
	                    	   name="password_confirmation"
	                    	   required
	                    >
                    </div>
                </div>
                <div class="form-group row">
                	<div class="col-md-6 {{ $errors->has('access_level') ? 'has-error' : '' }}">
	                	<label for="access_level">Access level: *</label>
	                    <select class="form-control" 
	                    		id="access_level" 
	                    		name="access_level"
	                    >
	                    	<option value=""> &nbsp; </option>
	                    	@foreach($accessLevel as $level)
								<option value="{{ $level->id }}" 
										{{ old('access_level') == $level->id ? 'selected' : '' }}
								>
									{{ $level->title }}
								</option>
							@endforeach
					  	</select>
					  	{!! $errors->first('access_level', '<span class="help-block">:message</span>') !!}
                    </div>
                    <div class="col-md-6 {{ $errors->has('user_group') ? 'has-error' : '' }}">
	                    <label for="user_group">User group: *</label>
	                    <select class="form-control" 
	                    		id="user_group" 
	                    		name="user_group"
	                    >
	                    	<option value=""> &nbsp; </option>
	                    	@foreach($userGroup as $group)
								<option value="{{ $group->id }}"
										{{ old('user_group') == $group->id ? 'selected' : '' }}
								>
									{{ $group->title }}
								</option>
							@endforeach
					  	</select>
					  	{!! $errors->first('user_group', '<span class="help-block">:message</span>') !!}
                    </div>
				</div>
				<div class="level">
                    <span class="flex">&nbsp;</span>
                    <a href="/users" type="submit" class="btn btn-default">Cancel</a>
                    &nbsp;&nbsp;
                    <button type="submit" class="btn btn-primary">Create</button>
                </div>
            </form>
		</div>
	</div>

</div>

@endsection