<div class="modal-content">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <h4 class="modal-title" style="color:red">
      Deactivate {{ $user->first_name . ' ' . $user->last_name }}'s account!
    </h4>
  </div>
  <div class="modal-body">
    <p>Are you sure you want to deactivate this user account?</p>
  </div>
  <div class="modal-footer">
    <form method="post" action="/users/{{ $user->id }}/delete">
    {{ csrf_field() }}
    {{ method_field('delete') }}
    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
    <button type="submit" class="btn btn-danger">Yes</button>
    </form>
  </div>
</div>