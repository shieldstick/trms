@extends('partials.master')
@section('content')

<div class="container col-md-12">

    <div class="level">
        <span class="flex">&nbsp;</span>
        <a href="/users/create" class="btn btn-danger">
            <span class="glyphicon glyphicon-plus-sign"></span>
            &nbsp;&nbsp;
            Create new user
        </a>
    </div>

    <div class="level">&nbsp;</div>
    <div class="table-users">
    	<table id="users-table" class="table table-striped table-hover">
            <thead>
        		<tr>
        			<th>First Name</th>
                    <th>Last Name</th>
        			<th>Email</th>
        			<th>Access Level</th>
        			<th>User Group</th>
        			<th>Active</th>
        		</tr>
            </thead>

            <tbody>
            	@foreach($users as $user)
        		<tr class="clickable-row" data-href="{{ url('/users/' . $user->id) }}">
        			<td>{{ $user->first_name  }}</td>
                    <td>{{ $user->last_name }}</td>
        			<td>{{ $user->email }}</td>
        			<td>{{ $user->accessLevel->title }}</td>
        			<td>{{ $user->userGroup->title }}</td>
        			<td>{{ !$user->trashed() ? 'Yes' : 'No' }}</td>
        		</tr>
            	@endforeach
            </tbody>
    	</table>
    </div>
</div>

@endsection