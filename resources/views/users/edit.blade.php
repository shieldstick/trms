@extends('partials.master')
@section('content')

<div class="container col-md-8 col-md-offset-2">

	<div class="panel panel-default">
		<div class="panel-heading level">
        	<span class="flex"><strong>Edit User Account</strong></span>
        	&nbsp; <a href="/users">Back to list</a> &nbsp;
		</div>
		<div class="panel-body">
			<form method="post" action="/users/{{ $user->id }}/update">
                {{ csrf_field() }}
                {{ method_field('patch') }}
                <div class="form-group row">
                	<div class="col-md-6">
	                	<label for="first_name">First name: *</label>
	                    <input type="first_name" 
	                           class="form-control" 
	                           id="first_name" 
	                           name="first_name"
	                           value="{{ $user->first_name }}" 
	                           required
	                    >
	                    {!! $errors->first('first_name', '<span class="help-block">:message</span>') !!}
                    </div>
                    <div class="col-md-6">
	                    <label for="last_name">Last name: *</label>
	                    <input type="last_name" 
	                           class="form-control" 
	                           id="last_name" 
	                           name="last_name"
	                           value="{{ $user->last_name }}" 
	                           required
	                    >
	                    {!! $errors->first('last_name', '<span class="help-block">:message</span>') !!}
                    </div>
				</div>
                <div class="form-group row">
                	<div class="col-md-6">
	                    <label for="email">Email address: *</label>
	                    <input type="email" 
	                    	   class="form-control" 
	                    	   id="email" 
	                    	   name="email"
	                    	   disabled="disabled"
	                    	   value="{{ $user->email }}" 
	                    	   required
	                    >
	                    {!! $errors->first('email', '<span class="help-block">:message</span>') !!}
                    </div>
                    <div class="col-md-6 {{ $errors->has('timezone') ? 'has-error' : '' }}">
                    	@include('users.timezone-select')
                    	{!! $errors->first('timezone', '<span class="help-block">:message</span>') !!}
					</div>
                </div>
                <div class="form-group row">
                	<div class="col-md-6">
	                	<label for="access_level">Access level: *</label>
	                    <select class="form-control" id="access_level" name="access_level">
	                    	@foreach($accessLevel as $level)
								<option value="{{ $level->id }}" 
										{{ $user->access_level == $level->id ? 'selected' : '' }}
								>
									{{ $level->title }}
								</option>
							@endforeach
					  	</select>
					  	{!! $errors->first('access_level', '<span class="help-block">:message</span>') !!}
                    </div>
                    <div class="col-md-6">
	                    <label for="user_group">User group: *</label>
	                    <select class="form-control" id="user_group" name="user_group">
	                    	@foreach($userGroup as $group)
								<option value="{{ $group->id }}" {{ $user->user_group == $group->id ? 'selected' : '' }}>
									{{ $group->title }}
								</option>
							@endforeach
					  	</select>
					  	{!! $errors->first('user_group', '<span class="help-block">:message</span>') !!}
                    </div>
				</div>
				<div class="level">
                    <span class="flex">&nbsp;</span>
                    <a href="/users/{{ $user->id }}" type="submit" class="btn btn-default">Cancel</a>
                    &nbsp;&nbsp;
                    <button type="submit" class="btn btn-primary">Update</button>
                </div>
            </form>
		</div>
	</div>

</div>

@endsection