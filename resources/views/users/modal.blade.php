<!-- Modal -->
<div class="modal fade" id="user-modal" role="dialog">
  <div class="modal-dialog">

  <!-- Modal content-->
  @if(is_null($user->deleted_at))

    @include('users.modal-deactivate', $user)

  @else

    @include('users.modal-reactivate', $user)

  @endif
  

  </div>
</div>