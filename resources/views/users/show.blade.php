@extends('partials.master')
@section('content')

<div class="container col-md-8 col-md-offset-2">

	<div class="panel panel-default">
        <div class="panel-heading level">
        	<span class="flex"><strong>{{ $user->first_name }} {{ $user->last_name }}</strong></span>

            @can('index', App\User::class)
        	   &nbsp; <a href="/users">Back to list</a> &nbsp; 
            @endcan
            
        </div>
        <div class="panel-body">
        	<div class="level">
                <span class="flex">{{ $user->email }}</span>
                @if(is_null($user->deleted_at))
                    <span style="color:green;"><strong>Active</strong></span>
                @else
                    <span style="color:red;"><strong>Inactive</strong></span>
                @endif
            </div>
        	<div class="level">&nbsp;</div>
            
            	<div class="level">{{ $user->accessLevel->title }}</div>
            	<div class="level">{{ $user->userGroup->title }}</div>
            	<div class="level">&nbsp;</div>
            	<div class="level">
                <div class="flex">
                    &nbsp;
                    @if(!$user->trashed())
                        @can('edit', App\User::class)
                    		<a href="/users/{{ $user->id }}/edit">
                    			<span class="glyphicon glyphicon-pencil"></span>&nbsp;Edit User Profile
                    		</a>
                        @endcan
                    @endif
                </div>
                    @if(!$user->trashed())
                        @can('delete', App\User::class)
                        <div class="level">
                            <button class="btn btn-danger" data-toggle="modal" data-target="#user-modal">
                                <span class="glyphicon glyphicon-remove-circle"></span>&nbsp;Deactivate Account
                            </button>
                        </div>
                        @endcan
                    @else
                        @can('restore', App\User::class)
                        <div class="level">
                            <button class="btn btn-success" data-toggle="modal" data-target="#user-modal">
                                <span class="glyphicon glyphicon-ok-circle"></span>&nbsp;Reactivate Account
                            </a>
                        </div>
                        @endcan
                    @endif
            	</div>
            
		</div>
    </div>

    @include('users.modal', $user)

</div>

@endsection