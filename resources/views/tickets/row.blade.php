@foreach($tickets as $ticket)

@if($ticket->status->title == "Resolved")
    <tr class="success">
@elseif(($ticket->status->title != "Resolved" 
        || $ticket->status->title != "Cancelled") 
        && !is_null($ticket->target_date) 
        && $ticket->target_date <= Carbon::now())
    <tr class="danger">
@elseif($ticket->status->title == "New" 
        || $ticket->status->title == "Cancelled")
    <tr class="default">
@else
    <tr class="info">
@endif
        <td>
            {{ $ticket->identification }}
        </td>
        <td>
            @if(is_null($ticket->ticket_id))
                <a href="/tickets/{{ $ticket->id }}">
                    {{ str_limit($ticket->subject, 50) }}
                </a>
            @else
                <a href="/sub-tickets/{{ $ticket->id }}">
                    {{ str_limit($ticket->subject, 50) }}
                </a>
            @endif
        </td>
        <td>
            {{ $ticket->creator->first_name . ' ' . $ticket->creator->last_name }}
        </td>
        <td>
            {{ $ticket->requester->first_name . ' ' . $ticket->requester->last_name }}
        </td>
        <td>{{ $ticket->client->title }}</td>
        <td>{{ $ticket->module->title }}</td>
        <td>{{ $ticket->first_developer_assignment_by ?: '' }}</td>
        <td>{{ $ticket->first_developer_assignment_at 
                ? $ticket->first_developer_assignment_at->toFormattedDateString() 
                : '' }}
        </td>
        <td>{{ $ticket->first_developer_assignment ?: '' }}</td>
        <td>
            {{ !is_null($ticket->developer_id) 
                ? $ticket->developer->first_name . ' ' . $ticket->developer->last_name
                : '' }}
        </td>
        <td>{{ $ticket->first_qa_assignment_by ?: '' }}</td>
        <td>{{ $ticket->first_qa_assignment_at 
                ? $ticket->first_qa_assignment_at->toFormattedDateString() 
                : '' }}
        </td>
        <td>{{ $ticket->first_qa_assignment ?: '' }}</td>
        <td>
            {{ !is_null($ticket->qa_id) 
                ? $ticket->qa->first_name . ' ' . $ticket->qa->last_name
                : '' }}
        </td>
        <td>
            {{ $ticket->created_at->toFormattedDateString() }}
        </td>
        <td>
            {{ !is_null($ticket->target_date) 
                ? $ticket->target_date->toFormattedDateString() 
                : '' }}
        </td>
        <td>
            {{ !is_null($ticket->resolved_date) 
                ? $ticket->resolved_date->toFormattedDateString() 
                : '' }}
        </td>
        <td>
            {{ $ticket->priority->title }}
        </td>
        <td>
            {{ $ticket->status->title }}
        </td>
    </tr>

    @if($ticket->children_count != 0)
        @include('tickets.row', ['tickets' => $ticket->children])
    @endif
@endforeach