@extends('partials.master')
@section('content')
<div class="container col-md-12 level">
    <div class="flex">
        <button class="btn btn-danger" data-toggle="modal" data-target="#filter-modal">
            <span class="glyphicon glyphicon-filter"></span>
        </button>
    </div>
</div>

@include('tickets.modal-filter')
<div class="col-md-12">
    <div class="level">&nbsp;</div>
    <div class="table-responsive">
    	<table id="tickets-table" class="table table-hover">
            <thead>
                <tr>
                    <th>Ticket #</th>
                    <th>Subject</th>
                    <th>Submitted By</th>
                    <th>Requested By</th>
                    <th>Client</th>
                    <th>Module</th>
                    <th>First Developer Assignment By</th>
                    <th>First Developer Assignment At</th>
                    <th>First Developer Assigned</th>
                    <th>Developer</th>
                    <th>First QA Assignment By</th>
                    <th>First QA Assignment At</th>
                    <th>First QA Assigned</th>
                    <th>QA</th>
                    <th>Date Created</th>
                    <th>Target Date</th>
                    <th>Resolved Date</th>
                    <th>Priority</th>
                    <th>Status</th>
                </tr>
            </thead>
            <tbody>
                @include('tickets.row')
            </tbody>
    	</table>
    </div>
</div>
@endsection