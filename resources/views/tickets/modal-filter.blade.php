<!-- Modal -->
<div class="modal fade" id="filter-modal" role="dialog">
  <div class="modal-dialog" style="width: 922px">
  <!-- Modal content-->
    <div class="modal-content">
      <form class="form-horizontal" method="GET" action="/tickets">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span></button>
        <h4 class="modal-title" style="color:#d43f3a;">
          Filter Tickets
        </h4>
      </div>

      <div class="modal-body">

        <div class="form-group">
          <label class="control-label col-md-2" for="developer">Developer:</label>
          <div class="col-md-4">
              <select class="form-control" id="developer" name="developer">
                <option value="">&nbsp;</option>
                @foreach($users->where('user_group', 2) as $developer)
                    <option value="{{ $developer->id }}">
                        {{ $developer->first_name . ' ' . $developer->last_name }}
                    </option>
                @endforeach
              </select>
          </div>
          <label class="control-label col-md-2" for="qa">QA:</label>
          <div class="col-md-4">
              <select class="form-control" id="qa" name="qa">
                <option value="">&nbsp;</option>
                @foreach($users->where('user_group', 2) as $qa)
                    <option value="{{ $qa->id }}">
                        {{ $qa->first_name . ' ' . $qa->last_name }}
                    </option>
                @endforeach
              </select>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-2" for="creator">Submitted by:</label>
          <div class="col-md-4">
              <select class="form-control" id="creator" name="creator">
                <option value="">&nbsp;</option>
                @foreach($users as $creator)
                    <option value="{{ $creator->id }}">
                        {{ $creator->first_name . ' ' . $creator->last_name }}
                    </option>
                @endforeach
              </select>
          </div>
          <label class="control-label col-md-2" for="requester">Requested by:</label>
          <div class="col-md-4">
              <select class="form-control" id="requester" name="requester">
                  <option value="">&nbsp;</option>
                @foreach($users as $requester)
                    <option value="{{ $requester->id }}"
                    {{ old('requester') == $requester->id ? 'selected' : '' }}>
                        {{ $requester->first_name . ' ' . $requester->last_name }}
                    </option>
                @endforeach
              </select>
          </div>
        </div>
        <hr>
        <div class="form-group">
            <label class="control-label col-md-2" for="client">Client: *</label>
            <div class="col-md-4">
                <select class="form-control" id="client" name="client">
                  <option value="">&nbsp;</option>
                  @foreach($clients as $client)
                      <option value="{{ $client->id }}">
                          {{ $client->title }}
                      </option>
                  @endforeach
                </select>
            </div>
            <label class="control-label col-md-2" for="module">Module: *</label>
            <div class="col-md-4">
                <select class="form-control" id="module" name="module">
                  <option value="">&nbsp;</option>
                  @foreach($modules as $module)
                      <option value="{{ $module->id }}">
                          {{ $module->title }}
                      </option>
                  @endforeach
                </select>
            </div>
        </div>
        <hr>
        <div class="form-group">
          <label class="control-label col-md-2" for="status">Status: *</label>
            <div class="col-md-4">
              <select class="form-control" id="status" name="status">
                  <option value="">&nbsp;</option>
                  @foreach($statuses as $status)
                      <option value="{{ $status->id }}">{{ $status->title }}</option>
                  @endforeach
              </select>
            </div>
          <label class="control-label col-md-2" for="access-level">Priority: *</label>
            <div class="col-md-4">
                <select class="form-control" id="priority" name="priority">
                    <option value="">&nbsp;</option>
                    @foreach($priorities as $priority)
                        <option value="{{ $priority->id }}"
                                {{ old('priority') == $priority->id ? 'selected' : '' }}>
                            {{ $priority->title }}
                        </option>
                    @endforeach
                </select>
                {!! $errors->first('priority', '<span class="help-block">:message</span>') !!}
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <button type="submit" class="btn btn-danger">Search</button>
      </div>

      </form>

    </div>

  </div>
</div>