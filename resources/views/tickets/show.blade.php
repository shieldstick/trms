@extends('partials.master')
@section('content')

<div class="container col-md-8">

    @if($ticket->status->title == 'Resolved' || $ticket->status->title == 'Cancelled')
        @include('tickets.panels.locked')
    @else
        @include('tickets.panels.form')
    @endif

</div>

<div class="container col-md-4">
	@include('tickets.panels.attachments')
    <comments :data="{{ $ticket->comments }}"></comments>
</div>

@if(is_null($ticket->ticket_id))
    @include('tickets.panels.sub-tickets')
@endif

<div class="container level">&nbsp;</div>
@endsection