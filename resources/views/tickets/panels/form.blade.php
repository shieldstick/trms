<div class="panel panel-danger ticket-creation-panel">
    <div class="panel-heading level ticket-creation-panel-heading">
        <span class="flex">
            <strong>Viewing ticket# {{ $ticket->identification }}</strong>
            [created by: {{ $ticket->creator->full_name }}]
        </span>
        <a href="/tickets?open=true" style="color:white;">Back to List</a>
    </div>
    <div class="panel-body ticket-creation-panel-body">
        <form class="form-horizontal" method="POST" action="/tickets/{{ $ticket->id }}/update" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="form-group">
                <label class="control-label col-md-2" for="subject" >Subject: *</label>
                <div class="col-md-10">
                    <input type="text" 
                               class="form-control" 
                               id="subject" 
                               name="subject"
                               value="{{ $ticket->subject }}" 
                               required
                               readonly
                        >
                    {!! $errors->first('subject', '<span class="help-block">:message</span>') !!}
                </div>
            </div>

            @include('tickets.inputs.status')

            <div class="form-group">
                <label class="control-label col-md-2" for="access-level">Priority: *</label>
                <div class="col-md-4">
                    <select class="form-control" id="priority" name="priority">
                        @foreach($priorities as $priority)
                            <option value="{{ $priority->id }}" {{ $ticket->priority_id == $priority->id ? 'selected' : '' }}>
                                {{ $priority->title }}
                            </option>
                        @endforeach
                    </select>
                    {!! $errors->first('priority', '<span class="help-block">:message</span>') !!}
                </div>
                <label class="control-label col-md-2" for="access-level">Request type: *</label>
                <div class="col-md-4">
                    <select class="form-control" id="request-type" name="request_type" required>
                        @foreach($requestTypes as $requestType)
                            <option value="{{ $requestType->id }}"
                                    {{ !is_null($requestType->deleted_at) ? 'disabled' : '' }}
                                    {{ $ticket->request_type_id == $requestType->id ? 'selected' : '' }}>
                                {{ $requestType->title }}
                            </option>
                        @endforeach
                    </select>
                    {!! $errors->first('request_type', '<span class="help-block">:message</span>') !!}
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-md-2" for="client">Client: *</label>
                <div class="col-md-4">
                    <select class="form-control" id="client" disabled="true">
                        @foreach($clients as $client)
                            <option value="{{ $client->id }}" 
                                    {{ $ticket->client_id == $client->id ? 'selected' : '' }}
                            >
                                {{ $client->title }}
                            </option>
                        @endforeach
                        <input type="hidden" name="client" value="{{ $ticket->client_id }}">
                    </select>
                    {!! $errors->first('client', '<span class="help-block">:message</span>') !!}
                </div>
                <label class="control-label col-md-2" for="module">Module: *</label>
                <div class="col-md-4">
                    <select class="form-control" id="module" name="module">
                        @foreach($modules as $module)
                            <option value="{{ $module->id }}" 
                                    {{ $ticket->module_id == $module->id ? 'selected' : '' }}
                            >
                                {{ $module->title }}
                            </option>
                        @endforeach
                    </select>
                    {!! $errors->first('module', '<span class="help-block">:message</span>') !!}
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-md-2" for="requester">Requester: *</label>
                <div class="col-md-4">
                    <select class="form-control" id="requester" disabled="true">
                        @foreach($users as $requester)
                            <option value="{{ $requester->id }}" 
                                    {{ $ticket->requester_id == $requester->id ? 'selected' : '' }}
                            >
                                {{ $requester->first_name . ' ' . $requester->last_name }}
                            </option>
                        @endforeach
                    </select>
                    <input type="hidden" name="requester" value="{{ $ticket->requester_id }}">
                    {!! $errors->first('requester', '<span class="help-block">:message</span>') !!}
                </div>

                <label class="control-label col-md-2" for="target-date">Target Date: </label>
                <div class="col-md-4">
                    <div id="target-date-value" data-value="{{ !is_null($ticket->target_date) ? $ticket->target_date->toDateString() : '' }}"></div>
                    <div id="target-date" class="input-group date">
                        <input class="form-control"
                               type="text" 
                               name="target_date"
                               id="target_date">
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-md-2" for="description">Details: *</label>
                <div class="col-md-10">
                    <textarea name="description"
                              id="description" 
                              class="form-control" 
                              rows="5"
                              @if((auth()->user()->userGroup->title == 'IT') && 
                                 !(auth()->user()->accessLevel->title == 'Administrator'))
                                readonly 
                              @endif 
                              placeholder="Add some details to your ticket..">{{ $ticket->description }}</textarea>
                </div>
            </div>

            @if(auth()->user()->userGroup->title == 'IT')
                @include('tickets.inputs.assignments')
            @endif
            
            <hr>

            <div class="form-group {{ $errors->has('attachments') ? 'has-error' : '' }}">
                <label class="control-label col-md-2" for="attachments">Attachments: *</label>
                <div class="col-md-10">
                    <input class="form-control file" 
                            type="file" 
                            id="attachments"
                            name="attachments[]"
                            data-show-preview="false"
                            data-show-upload="false"
                            multiple>
                    <div class="hint">
                            <br><span>**You can select multiple files by holding CTRL key on your keyboard**</span>
                    </div>
                    {!! $errors->first('attachments', '<span class="help-block">:message</span>') !!}
                </div>
            </div>
            <div class="level">&nbsp;</div>
                <div class="level">
                    <span class="flex">&nbsp;</span>
                    <button type="submit" class="btn btn-danger">Save</button>
                </div>
        </form>
    </div>
    <div class="panel-footer level ticket-creation-panel-footer">
        <a href="#" data-toggle="modal" data-target="#activities-modal">
            <span class="glyphicon glyphicon-list-alt"></span>
            &nbsp;&nbsp;View Activity Logs
        </a>
    </div>
</div>

@include('activities.modal')