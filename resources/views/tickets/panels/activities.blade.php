<div class="panel panel-info">
    <div class="panel-heading"><span class="glyphicon glyphicon-list-alt"></span> Activities</div>
    <div class="panel-body">
    	@if(empty($ticket->activity))
    		<span style="color: #aaaaaa">Nothing to show yet..</span>
    	@else
    		@foreach($ticket->activity as $activity)
    			@include('tickets.activities.'.$activity->type)
    		@endforeach
    	@endif
    </div>
</div>