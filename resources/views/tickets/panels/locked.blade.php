<div class="panel panel-danger ticket-creation-panel">
    <div class="panel-heading level ticket-creation-panel-heading">
        <span class="flex">
            <strong>Viewing Locked Ticket# {{ $ticket->identification }}</strong>
            created by: {{ $ticket->creator->full_name }}
        </span>
        <a href="/tickets?open=true" style="color:white;">Tickets List</a>
    </div>
    <div class="panel-body ticket-creation-panel-body">
        <table class="table table-bordered">
            <tr>
                <td style="width: 15%; background-color:#eee">Subject:</td>
                <td colspan="3">{{ $ticket->subject }}</td>
            </tr>
            <tr>
                <td style="width: 15%; background-color:#eee">Status:</td>
                <td>{{ $ticket->status->title }}</td>
                <td style="width: 15%; background-color:#eee">Request type:</td>
                <td>{{ $ticket->requestType->title }}</td>
            </tr>
            <tr>
                <td style="width: 15%; background-color:#eee">Submitted by:</td>
                <td>{{ $ticket->creator->first_name . " " . $ticket->creator->last_name }}</td>
                <td style="width: 15%; background-color:#eee">Requested by:</td>
                <td>{{ $ticket->requester->first_name . " " . $ticket->requester->last_name }}</td>
            </tr>
            <tr>
                <td style="width: 15%; background-color:#eee">Client:</td>
                <td>{{ $ticket->client->title }}</td>
                <td style="width: 15%; background-color:#eee">Module:</td>
                <td>{{ $ticket->module->title }}</td>
            </tr>
            <tr>
                <td style="width: 15%; background-color:#eee">Details:</td>
                <td colspan="3" style="width: 85%;">{!! nl2br($ticket->description) !!}</td>
            </tr>
        </table>
        
        <table class="table table-bordered">
            <tr>
                <td width="15%" style="background-color:#eee">Developer:</td>
                <td>
                    {{ !is_null($ticket->developer_id) ? $ticket->developer->first_name . " " . $ticket->developer->last_name : '' }}
                </td>
                <td width="15%" style="background-color:#eee">Start Date:</td>
                <td>
                    {{ !is_null($ticket->dev_start_date) ? $ticket->dev_start_date->toFormattedDateString() : '' }}
                </td>
                <td width="20%" style="background-color:#eee">Completion Date:</td>
                <td>
                    {{ !is_null($ticket->dev_completion_date) ? $ticket->dev_completion_date->toFormattedDateString() : '' }}
                </td>
            </tr>
            <tr>
                <td width="15%" style="background-color:#eee">QA:</td>
                <td>
                    {{ !is_null($ticket->qa_id) ? $ticket->qa->first_name . " " . $ticket->qa->last_name : '' }}
                </td>
                <td width="15%" style="background-color:#eee">Start Date:</td>
                <td>
                    {{ !is_null($ticket->qa_start_date) ? $ticket->qa_start_date->toFormattedDateString() : '' }}
                </td>
                <td width="20%" style="background-color:#eee">Completion Date:</td>
                <td>
                    {{ !is_null($ticket->qa_completion_date) ? $ticket->qa_completion_date->toFormattedDateString() : '' }}
                </td>
            </tr>
        </table>
        <table class="table table-bordered">
            <tr>
                <td style="width: 15%; background-color:#eee">Date created:</td>
                <td>{{ $ticket->created_at->toFormattedDateString() }}</td>
                <td style="width: 15%; background-color:#eee">Target date:</td>
                <td>{{ !is_null($ticket->target_date) ? $ticket->target_date->toFormattedDateString() : 'Not specified' }}</td>
                <td style="width: 15%; background-color:#eee">Resolved date:</td>
                <td>{{ !is_null($ticket->resolved_date) ? $ticket->resolved_date->toFormattedDateString() : 'Not specified' }}</td>
            </tr>
        </table>
	</div>
    <div class="panel-footer level ticket-creation-panel-footer">
        <a href="#" data-toggle="modal" data-target="#activities-modal" >
            <span class="glyphicon glyphicon-list-alt"></span>
            &nbsp;&nbsp;View Activity Logs
        </a>
    </div>
</div>

@include('activities.modal')