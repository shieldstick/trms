<div class="panel panel-info">
    <div class="panel-heading level">
    	<div class="flex">
    		<span class="glyphicon glyphicon-tasks"></span>&nbsp;&nbsp;Tasks
    	</div>
    	<div>
    		<a href="#" data-toggle="modal" data-target="#task-create-modal">
    			<span class="glyphicon glyphicon-plus"></span>&nbsp;&nbsp;Add Tasks
    		</a>
    	</div>
    </div>
    <div class="panel-body">
    	@if($ticket->tasks->count() == 0)
    		<span style="color: #aaaaaa">No tasks created yet..</span>
    	@else
	    	<table id="task-table" class="display table table-hover">
			    <thead>
			        <tr>
			        	<th style="text-align: center; width:5%"></th>
			            <th style="text-align: center;">Task ID</th>
			            <th style="text-align: center;">Subject</th>
			            <th style="text-align: center;">Assignee</th>
			            <th style="text-align: center;">Due Date</th>
			            <th style="text-align: center;">Completed</th>
			        </tr>
			    </thead>
			    <tbody>
			    	@foreach($ticket->tasks as $task)
			    		<tr>
			    			<td>
			    				<input type="checkbox" value="" style="width: 100%;" data-toggle="modal" data-target="#task-confirm-modal">
			    				@include('tasks.task-confirm-modal')
			    			</td>
			    			<td style="text-align: center;">
			    				<a href="#" data-toggle="modal" data-target="#task-show-modal">
			    					#{{ sprintf("%06s", $ticket->id) }} - {{ $task->number }}
			    				</a>
			    			</td>
			    			<td style="text-align: center;">
			    				<a href="#" data-toggle="modal" data-target="#task-show-modal">
			    					{{ $task->subject }}
			    				</a>
			    			</td>
			    			<td style="text-align: center;">{{ $task->assignee->first_name . ' ' . $task->assignee->last_name }}</td>
			    			<td style="text-align: center;">{{ $task->due_date->timezone(auth()->user()->timezone)->format('M d Y') }}</td>
			    			<td style="text-align: center;">{{ ($task->completed) ? 'Yes' : 'No' }}</td>
			    		</tr>
			    	@endforeach
				</tbody>
			</table>
    	@endif
    </div>
</div>

@include('tasks.task-create-modal')
@include('tasks.task-confirm-modal')