<div class="col-md-12" id="sub-tickets">
    <div class="panel panel-danger ticket-creation-panel sub-tickets-panel">

        <div class="panel-heading level ticket-creation-panel-heading">
            <span class="flex"><strong>Sub-Tickets</strong></span>
            <a href="/tickets/{{ $ticket->id }}/sub-tickets/create" style="color:white;">
                <span class="glyphicon glyphicon-plus"></span>
                &nbsp;&nbsp;Add a Sub-Ticket
            </a>
        </div>

        <div class="panel-body ticket-creation-panel-body">
            
            <table id="sub-tickets-table" class="display table table-hover">

                <thead>
                    <tr>
                        <th>Ticket #</th>
                        <th>Subject</th>
                        <th>Submitted By</th>
                        <th>Requested By</th>
                        <th>Client</th>
                        <th>Module</th>
                        <th>First Developer Assignment By</th>
                        <th>First Developer Assignment At</th>
                        <th>First Developer Assigned</th>
                        <th>Developer</th>
                        <th>First QA Assignment By</th>
                        <th>First QA Assignment At</th>
                        <th>First QA Assigned</th>
                        <th>QA</th>
                        <th>Date Created</th>
                        <th>Target Date</th>
                        <th>Resolved Date</th>
                        <th>Priority</th>
                        <th>Status</th>
                    </tr>
                </thead>
                
                <tbody>
                @include('tickets.row', 
                        ['tickets' => $ticket->children])
                </tbody>
            </table>
        </div>

    </div>
</div>