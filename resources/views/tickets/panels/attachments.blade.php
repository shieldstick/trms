<div class="panel panel-danger ticket-creation-panel">
    <div class="panel-heading level ticket-creation-panel-heading">
        <span class="glyphicon glyphicon-file"></span>&nbsp;&nbsp;Attachments
    </div>
    <div class="panel-body ticket-creation-panel-body">
        @forelse($ticket->attachments as $attachment)
            <div class="level p-l-2 p-r-2">
                <div class="flex">
                    <a href="/tickets/{{ $ticket->id }}/attachment/{{ $attachment->id }}">
                        <span class="glyphicon glyphicon-cloud-download"></span>&nbsp;&nbsp;{{ $attachment->file_name }}
                    </a>
                </div>
                <div>
                    <a href="/tickets/{{ $ticket->id }}/attachment/{{ $attachment->id }}/delete">
                        <span class="glyphicon glyphicon-remove-circle"></span>
                    </a>
                </div>
            </div>
        @empty
            <span style="color: #aaaaaa">There are no attachments for this ticket..</span>
        @endforelse
     </div>
</div>