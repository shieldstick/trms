<div class="form-group">
    <label class="control-label col-md-2" for="status">Status: *</label>
    <div class="col-md-4">
        {!! App\Status::select($ticket, auth()->user()) !!}
        {!! $errors->first('status', '<span class="help-block">:message</span>') !!}
    </div>
    <div class="col-md-6">
        <div class="hint">
        	@if($ticket->status_id == App\Status::endorsedToRequester()->id)
        		@if($ticket->requester_id == auth()->user()->id || $ticket->creator_id == auth()->user()->id)
        			<span>Set the status to "Needs Rework" if we missed something on your ticket. 
        			Otherwise, "For Deployment" or "Resolved".</span>
        		@else
        			<span>Waiting for requester response...</span>
        		@endif
        	@elseif($ticket->status_id < App\Status::endorsedToRequester()->id)
        		<span>IT is still working on your ticket. Once done, they will set the status to 'Endorsed to requester'.
        		{{$ticket->requester->first_name}} will receive an email with instructions for next steps. :)</span>
        	@elseif($ticket->status_id == App\Status::needsRework()->id)
        		<span>Thank you! IT will now re-check the ticket and confirm the things we missed..</span>
        	@elseif($ticket->status_id == App\Status::forDeployment()->id)
        		<span>We are now deploying the work. Once complete, we will mark the ticket as "Resolved".</span>
        	@endif
        </div>
    </div>
</div>