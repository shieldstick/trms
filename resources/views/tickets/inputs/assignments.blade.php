<hr>
<div class="form-group">
    <label class="control-label col-md-2" for="developer">Developer: </label>
    <div class="col-md-4">
        <select class="form-control" id="developer" name="developer">
            @if(is_null($ticket->developer_id))
                <option value=""></option>
            @endif
            @foreach($developers as $developer)
                <option value="{{ $developer->id }}" {{ $ticket->developer_id == $developer->id ? 'selected' : '' }}>
                    {{ $developer->first_name . ' ' . $developer->last_name }}
                </option>
            @endforeach
        </select>
        {!! $errors->first('client', '<span class="help-block">:message</span>') !!}
    </div>
    <label class="control-label col-md-2" for="qa">QA: </label>
    <div class="col-md-4">
        <select class="form-control" id="qa" name="qa">
            @if(is_null($ticket->qa_id))
                <option value=""></option>
            @endif
            @foreach($qas as $qa)
                <option value="{{ $qa->id }}" {{ $ticket->qa_id == $qa->id ? 'selected' : '' }}>
                    {{ $qa->first_name . ' ' . $qa->last_name }}
                </option>
            @endforeach
        </select>
        {!! $errors->first('client', '<span class="help-block">:message</span>') !!}
    </div>
</div>

<div class="form-group">
<label class="control-label col-md-3" for="dev-start-date">Dev - Start Date:</label>
    <div class="col-md-3">
        <div id="dev-start-date-value" data-value="{{ !is_null($ticket->dev_start_date) ? $ticket->dev_start_date->toDateString() : '' }}"></div>
        <div id="dev-start-date" class="input-group date">
            <input class="form-control" 
                   type="text"
                   name="dev_start_date"/>
            <span class="input-group-addon">
                <span class="glyphicon glyphicon-calendar"></span>
            </span>
        </div>
    </div>
    <label class="control-label col-md-3" for="qa-start-date">QA - Start Date:</label>
    <div class="col-md-3">
        <div id="qa-start-date-value" data-value="{{ !is_null($ticket->qa_start_date) ? $ticket->qa_start_date->toDateString() : '' }}"></div>
        <div id="qa-start-date" class="input-group date">
            <input class="form-control" 
                   type="text"
                   name="qa_start_date"/>
            <span class="input-group-addon">
                <span class="glyphicon glyphicon-calendar"></span>
            </span>
        </div>
    </div>
</div>

<div class="form-group">
<label class="control-label col-md-3" for="dev-completion-date">Dev - Completion Date:</label>
    <div class="col-md-3">
        <div id="dev-completion-date-value" data-value="{{ !is_null($ticket->dev_completion_date) ? $ticket->dev_completion_date->toDateString() : '' }}"></div>
        <div id="dev-completion-date" class="input-group date">
            <input class="form-control" 
                   type="text"
                   name="dev_completion_date"/>
            <span class="input-group-addon">
                <span class="glyphicon glyphicon-calendar"></span>
            </span>
        </div>
    </div>
    <label class="control-label col-md-3" for="dev-completion-date">QA - Completion Date:</label>
    <div class="col-md-3">
        <div id="qa-completion-date-value" data-value="{{ !is_null($ticket->qa_completion_date) ? $ticket->qa_completion_date->toDateString() : '' }}"></div>
        <div id="qa-completion-date" class="input-group date">
            <input class="form-control" 
                   type="text"
                   name="qa_completion_date"/>
            <span class="input-group-addon">
                <span class="glyphicon glyphicon-calendar"></span>
            </span>
        </div>
    </div>
</div>