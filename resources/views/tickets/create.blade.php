@extends('partials.master')
@section('content')


<div class="container col-md-8 col-md-offset-2">
	<div class="panel panel-danger ticket-creation-panel">
        <div class="panel-heading level ticket-creation-panel-heading">
            <span class="flex">
                <strong>Creating a ticket</strong>
            </span>
            <a href="/tickets?open=true" style="color:white;">Back to list</a>
		</div>
        <div class="panel-body ticket-creation-panel-body">
            <form class="form-horizontal" method="POST" action="/tickets/create" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="form-group {{ $errors->has('subject') ? 'has-error' : '' }}">
                    <label class="control-label col-md-2" for="subject">Subject: *</label>
                    <div class="col-md-10">
                        <input type="text" 
                                   class="form-control" 
                                   id="subject" 
                                   name="subject"
                                   value="{{ old('subject') ?? '' }}"
                                   placeholder="Add a subject to your ticket.." 
                                   required
                            >
                        {!! $errors->first('subject', '<span class="help-block">:message</span>') !!}
                    </div>
                </div>
                <div class="form-group {{ $errors->has('priority') ? 'has-error' : '' }}">
                    <label class="control-label col-md-2" for="access-level">Priority: *</label>
                    <div class="col-md-4">
                        <select class="selectpicker form-control" data-live-search="true" data-dropup-auto="false" style="width:100%;" id="priority" name="priority" required>
                            <option value="">&nbsp;</option>
                            @foreach($priorities as $priority)
                                <option value="{{ $priority->id }}"{{ old('priority') == $priority->id ? ' selected ' : '' }}>{{ $priority->title }}</option>
                            @endforeach
                        </select>
                        {!! $errors->first('priority', '<span class="help-block">:message</span>') !!}
                    </div>
                    <label class="control-label col-md-2" for="access-level">Request type: *</label>
                    <div class="col-md-4">
                        <select class="selectpicker form-control" data-live-search="true" data-dropup-auto="false" style="width:100%;" id="request-type" name="request_type" required>
                            <option value=""> &nbsp; </option>
                            @foreach($requestTypes as $requestType)
                                <option value="{{ $requestType->id }}"
                                        {{ old('requestType') == $requestType->id ? 'selected' : '' }}>
                                    {{ $requestType->title }}
                                </option>
                            @endforeach
                        </select>
                        {!! $errors->first('request_type', '<span class="help-block">:message</span>') !!}
                    </div>
                </div>
                <div class="form-group {{ $errors->has('client') ? 'has-error' : '' }}">
                    <label class="control-label col-md-2" for="client">Client: *</label>
                    <div class="col-md-4">
                        <select class="selectpicker form-control" data-live-search="true" data-dropup-auto="false" style="width:100%;" id="client" name="client" required>
                        <option value=""> &nbsp; </option>
                            @foreach($clients as $client)
                                <option value="{{ $client->id }}"
                                        {{ old('client') == $client->id ? 'selected' : '' }}>
                                    {{ $client->title }}
                                </option>
                            @endforeach
                        </select>
                        {!! $errors->first('client', '<span class="help-block">:message</span>') !!}
                    </div>
                    <label class="control-label col-md-2 {{ $errors->has('module') ? 'has-error' : '' }}" for="module">Module: *</label>
                    <div class="col-md-4">
                        <select class="selectpicker form-control" data-live-search="true" data-dropup-auto="false" style="width:100%;" id="module" name="module" required>
                        <option value=""> &nbsp; </option>
                            @foreach($modules as $module)
                                <option value="{{ $module->id }}"
                                        {{ old('module') == $module->id ? 'selected' : '' }}>
                                    {{ $module->title }}
                                </option>
                            @endforeach
                        </select>
                        {!! $errors->first('module', '<span class="help-block">:message</span>') !!}
                    </div>
                </div>
                <div class="form-group {{ $errors->has('requester') ? 'has-error' : '' }}">
                    <label class="control-label col-md-2" for="requester">Requester: *</label>
                    <div class="col-md-4">
                        <select class="selectpicker form-control" data-live-search="true" data-dropup-auto="false" style="width:100%;" id="requester" name="requester">
                        <option value=""> &nbsp; </option>
                            @foreach($users as $requester)
                                <option value="{{ $requester->id }}"
                                        {{ old('requester') == $requester->id ? 'selected' : '' }}>
                                    {{ $requester->first_name . ' ' . $requester->last_name }}
                                </option>
                            @endforeach
                        </select>
                        {!! $errors->first('requester', '<span class="help-block">:message</span>') !!}
                    </div>
                    <label class="control-label col-md-2" for="target-date">Target Date: </label>
                    <div class="col-md-4">
                        <div id="target-date" class="input-group date">
                            <input class="form-control" 
                                   type="text"
                                   id="target_date"
                                   value="{{ old('target_date') ?? '' }}"
                                   name="target_date"/>
                                <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="form-group {{ $errors->has('description') ? 'has-error' : '' }}">
                    <label class="control-label col-md-2" for="client">Details: *</label>
                    <div class="col-md-10">
                        <textarea name="description" 
                                  class="form-control" 
                                  rows="5"
                                  required
                                  placeholder="Add some details to your ticket..">{{ old('description') ?? '' }}</textarea>
                        {!! $errors->first('description', '<span class="help-block">:message</span>') !!}
                    </div>
                </div>
                <div class="form-group {{ $errors->has('attachments') ? 'has-error' : '' }}">
                    <label class="control-label col-md-2" for="attachments">Attachments:</label>
                    <div class="col-md-10">
                        <input  type="file" 
                                id="attachments"
                                name="attachments[]"
                                data-show-upload="false"
                                data-show-preview="false"
                                multiple
                                class="form-control file file-loading">
                        <div class="hint">
                            <br><span>**You can select multiple files by holding CTRL key on your keyboard**</span>
                        </div>
                        {!! $errors->first('attachments', '<span class="help-block">:message</span>') !!}
                    </div>
                </div>
                <div class="level">
                    <span class="flex">&nbsp;</span>
                    <a href="/tickets" type="submit" class="btn btn-default">Cancel</a>
                    &nbsp;&nbsp;
                    <button type="submit" class="btn btn-danger">Create</button>
                </div>
            </form>
        </div>
	</div>
</div>

@endsection