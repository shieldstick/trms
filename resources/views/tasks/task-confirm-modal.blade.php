<!-- Modal -->
<div class="modal fade" id="task-confirm-modal" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<!-- Modal Header-->
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title" style="color:green">
					Complete task
				</h4>
			</div>
			<!-- Modal Header-->
			<form method="POST" action="/tasks/{{$task->id}}/complete" class="form-horizontal">
			{{ csrf_field() }}
			<!-- Modal Body-->
			<div class="modal-body">
				<h5>Are you sure you want to complete this task?</h5>
			</div>
			<!-- Modal Body-->

			<!-- Modal Footer-->
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
				<button type="submit" class="btn btn-success">Confirm</button>
			</div>
			<!-- Modal Footer-->
			</form>
		</div>
	</div>
</div>