@extends('partials.master')
@section('content')

<div class="container col-md-8">

	<div class="panel panel-danger ticket-creation-panel">
	    <div class="panel-heading level ticket-creation-panel-heading">
	    	<div class="flex">
	    		<span class="glyphicon glyphicon-tasks"></span>&nbsp;&nbsp;
	    		@if(Request::has('assigned'))
	    			My Assigned Tasks
	    		@elseif(Request::has('delegated'))
	    			My Delegated Tasks
	    		@else
	    			My Tasks
	    		@endif
	    	</div>
	    </div>
	    <div class="panel-body ticket-creation-panel-body">
	    	@if($tasks->count() == 0)
	    		<span style="color: #aaaaaa">No tasks created yet..</span>
	    	@else
		    	<table id="tasks-table" class="display table table-hover">
				    <thead>
				        <tr>
				            <th style="text-align: center;">Task ID</th>
				            <th style="text-align: center;">Subject</th>
				            <th style="text-align: center;">Assignee</th>
				            <th style="text-align: center;">Creator</th>
				            <th style="text-align: center;">Due Date</th>
				            <th style="text-align: center;">Completed</th>
				        </tr>
				    </thead>
				    <tbody>
				    	@foreach($tasks as $task)
				    		@can('view', $task)
				    		<tr>
				    			<td style="text-align: center;">
				    				<a href="/tickets/{{$task->ticket_id}}#tasks-table">
				    					#{{ sprintf("%06s", $task->ticket_id) }} - {{ $task->number }}
				    				</a>
				    			</td>
				    			<td style="text-align: center;">
				    				<a href="/tickets/{{$task->ticket_id}}#tasks-table">
				    					{{ $task->subject }}
				    				</a>
				    			</td>
				    			<td style="text-align: center;">{{ $task->assignee->full_name }}</td>
				    			<td style="text-align: center;">{{ $task->creator->full_name }}</td>
				    			<td style="text-align: center;">{{ $task->due_date->timezone(auth()->user()->timezone)->format('M d Y') }}</td>
				    			<td style="text-align: center;">{{ ($task->completed) ? 'Yes' : 'No' }}</td>
				    		</tr>
				    		@endcan
				    	@endforeach
					</tbody>
				</table>
	    	@endif
	    </div>
	</div>

</div>

<div class="container col-md-4">
	@include('tasks.task-stats-panel')
</div>

@endsection