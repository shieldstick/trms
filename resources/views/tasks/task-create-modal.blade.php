<!-- Modal -->
<div class="modal fade" id="task-create-modal" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<!-- Modal Header-->
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title" style="color:#31708f;">
					Create a task
				</h4>
			</div>
			<!-- Modal Header-->
			<form method="POST" action="/tickets/{{ $ticket->id }}/tasks/create" class="form-horizontal">
			{{ csrf_field() }}
			<!-- Modal Body-->
			<div class="modal-body">
				<div class="row">
					<div class="form-group {{ $errors->has('subject') ? 'has-error' : '' }}">
	                    <label class="control-label col-md-3" for="subject">Subject: *</label>
	                    <div class="col-md-8">
	                        <input type="text" 
	                                   class="form-control" 
	                                   id="subject" 
	                                   name="subject"
	                                   value="{{ old('subject') ?? '' }}"
	                                   placeholder="Add a subject.." 
	                                   required>
	                        {!! $errors->first('subject', '<span class="help-block">:message</span>') !!}
	                    </div>
	                </div>
	                <div class="form-group">
					    <label class="control-label col-md-3" for="assignee">Assignee: *</label>
					    <div class="col-md-8">
					        <select class="form-control" id="assignee" name="assignee" required>
				                <option value=""></option>
					            @foreach($users as $user)
					            	@if($user->user_group == App\UserGroup::it()->id)
						                <option value="{{ $user->id }}">
						                    {{ $user->first_name . ' ' . $user->last_name }}
						                </option>
					               @endif
					            @endforeach
					        </select>
					        {!! $errors->first('assignee', '<span class="help-block">:message</span>') !!}
					    </div>
					</div>
					<div class="form-group">
					    <label class="control-label col-md-3" for="due-date">Due Date: *</label>
					    <div class="col-md-8">
					        <div id="due-date" class="input-group date">
					            <input class="form-control" 
					                   type="text"
					                   value=""  
					                   name="due_date"/>
					            <span class="input-group-addon">
					                <span class="glyphicon glyphicon-calendar"></span>
					            </span>
					        </div>
				   		 </div>
					</div>
					<div class="form-group">
					    <label class="control-label col-md-3" for="details">Details: *</label>
					    <div class="col-md-8">
					        <textarea name="details"
                              id="details" 
                              class="form-control" 
                              rows="4"
                              required
                              placeholder="Add some details to your task.."></textarea>
				   		 </div>
					</div>
				</div>
			</div>
			{{-- <div class="level">&nbsp;</div> --}}
			<!-- Modal Body-->

			<!-- Modal Footer-->
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
				<button type="submit" class="btn btn-info">Create Task</button>
			</div>
			<!-- Modal Footer-->
			</form>
		</div>
	</div>
</div>