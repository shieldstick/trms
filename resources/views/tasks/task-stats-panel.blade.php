<div class="panel panel-info">
	<div class="panel panel-heading">
		Tasks Stats
	</div>
	<div class="panel-body">
        @if(Request::has('assigned'))
		<table class="table table-bordered">
			<tr>
                <td style="width:50%; text-align: center; background-color: #eee;" colspan="4">
                	<strong>Assigned Tasks</strong>
                </td>
            </tr>
            <tr>
                <td style="width:25%; background-color: #eee;">
                	Open 
                </td>
         		<td style="width:25%; text-align: center;">
                	{{ 
						$tasks->filter(function($task){ 
								return $task->assignee_id == auth()->id() && $task->completed == false; 
						})->count() 
					}} 
         		</td>
         		<td style="width:25%; background-color: #eee;">
                	Completed 
                </td>
         		<td style="width:25%; text-align: center;">
                	{{ 
						$tasks->filter(function($task){ 
								return $task->assignee_id == auth()->id() && $task->completed == true; 
						})->count() 
					}} 
         		</td>
            </tr>
        </table>
        
        <div class="level">&nbsp;</div>

        @elseif(Request::has('delegated'))
            <table class="table table-bordered">
    			<tr>
                    <td style="width:50%; text-align: center; background-color: #eee;" colspan="4">
                    	<strong>Delegated Tasks</strong>
                    </td>
                </tr>
                <tr>
                    <td style="width:25%; background-color: #eee;">
                    	Open 
                    </td>
             		<td style="width:25%; text-align: center;">
                    	{{ 
    						$tasks->filter(function($task){ 
    								return $task->creator_id == auth()->id() && $task->completed == false; 
    						})->count() 
    					}} 
             		</td>
             		<td style="width:25%; background-color: #eee;">
                    	Completed 
                    </td>
             		<td style="width:25%; text-align: center;">
                    	{{ 
    						$tasks->filter(function($task){ 
    								return $task->creator_id == auth()->id() && $task->completed == true; 
    						})->count() 
    					}} 
             		</td>
                </tr>
            </table>

        <div class="level">&nbsp;</div>
        @else
            <table class="table table-bordered">
                <tr>
                    <td style="width:50%; text-align: center; background-color: #eee;" colspan="4">
                        <strong>All Tasks</strong>
                    </td>
                </tr>
                <tr>
                    <td style="width:25%; background-color: #eee;">
                        Open 
                    </td>
                    <td style="width:25%; text-align: center;">
                        {{ 
                            $tasks->filter(function($task){ 
                                    return ($task->creator_id == auth()->id() || $task->assignee_id == auth()->id()) && $task->completed == false; 
                            })->count() 
                        }} 
                    </td>
                    <td style="width:25%; background-color: #eee;">
                        Completed 
                    </td>
                    <td style="width:25%; text-align: center;">
                        {{ 
                            $tasks->filter(function($task){ 
                                    return ($task->creator_id == auth()->id() || $task->assignee_id == auth()->id()) && $task->completed == true; 
                            })->count() 
                        }} 
                    </td>
                </tr>
            </table>
        @endif
	</div>
</div>