/**
 * Sidebar toggling
 *
 * @param  event
 * @return void
 */
$("#sidebar-toggle").click(function(e){
    e.preventDefault();
    $("#app").toggleClass("sidebar-displayed");
});


$(document).ready(function() {

    $('.clickable-row').click(function() {
        window.location = $(this).data('href');
    }); 

    $('#tickets-table').DataTable({
        "lengthMenu": [[-1, 10, 25, 50, 100], ["All", 10, 25, 50, 100]],
        "order": [],
        "oLanguage": {
            "sInfo": "Total of _END_ entries on this page",
            "sLengthMenu": "Show _MENU_ entries",
        },
        "columnDefs": [
            {
                "targets": [ 6 ],
                "visible": false,
                "searchable": false
            },
            {
                "targets": [ 7 ],
                "visible": false,
                "searchable": false
            },
            {
                "targets": [ 8 ],
                "visible": false,
                "searchable": false
            },
            {
                "targets": [ 10 ],
                "visible": false,
                "searchable": false
            },
            {
                "targets": [ 11 ],
                "visible": false,
                "searchable": false
            },
            {
                "targets": [ 12 ],
                "visible": false,
                "searchable": false
            },
        ],
        drawCallback: function(settings) {
            var pagination = $(this).closest('.dataTables_wrapper').find('.dataTables_paginate');
            pagination.toggle(this.api().page.info().pages > 1);
        },
        dom: '<"top level" l <"m-l-1"B> <"level flex">  f>rtip',
        buttons: [
            {
                extend: 'excelHtml5',
                text: 'Export to Excel',
                title: moment(new Date).format('MM-DD-YYYY') + ' TRMS - Tickets Excel Export',
                exportOptions: {
                    modifier: {
                        page: 'current'
                    }
                }
            }, 
        ]
    });

    $('#sub-tickets-table').DataTable({
        "lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
        "order": [],
        "columnDefs": [
            {
                "targets": [ 6 ],
                "visible": false,
                "searchable": false
            },
            {
                "targets": [ 7 ],
                "visible": false,
                "searchable": false
            },
            {
                "targets": [ 8 ],
                "visible": false,
                "searchable": false
            },
            {
                "targets": [ 10 ],
                "visible": false,
                "searchable": false
            },
            {
                "targets": [ 11 ],
                "visible": false,
                "searchable": false
            },
            {
                "targets": [ 12 ],
                "visible": false,
                "searchable": false
            },
        ],
        dom: '<"top level" l <"m-l-1"B> <"level flex">  f>rtip',
        buttons: [
            {
                extend: 'excelHtml5',
                text: 'Export to Excel',
                title: moment(new Date).format('MM-DD-YYYY') + ' TRMS - Sub-tickets Excel Export',
                exportOptions: {
                    modifier: {
                        page: 'current'
                    }
                }
            }, 
        ]
    });

    $('#users-table').DataTable({
        "lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
    });

    $('#surveys-table').DataTable({
        "lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
        "order": [],
        dom: '<"top level" l <"m-l-1"B> <"level flex">  f>rtip',
        buttons: [
            {
                extend: 'excelHtml5',
                text: 'Export to Excel',
                title: moment(new Date).format('MM-DD-YYYY') + ' TRMS - Surveys Excel Export',
                exportOptions: {
                    modifier: {
                        page: 'current'
                    }
                }
            }, 
        ]
    });

    var target_date_value = $('#target-date-value').attr('data-value');
    $('#target-date').datetimepicker({
        format: 'll',
        date: new Date(target_date_value),
    });

    var dev_start_date_value = $('#dev-start-date-value').attr('data-value');
    $('#dev-start-date').datetimepicker({
        format: 'll',
        date: new Date(dev_start_date_value),
    });

    var qa_start_date_value = $('#qa-start-date-value').attr('data-value');
    $('#qa-start-date').datetimepicker({
        format: 'll',
        date: new Date(qa_start_date_value),
    });

    var dev_completion_date_value = $('#dev-completion-date-value').attr('data-value');
    $('#dev-completion-date').datetimepicker({
        format: 'll',
        date: new Date(dev_completion_date_value),
    });

    var qa_completion_date_value = $('#qa-completion-date-value').attr('data-value');
    $('#qa-completion-date').datetimepicker({
        format: 'll',
        date: new Date(qa_completion_date_value),
    });

    $('#incident-date').datetimepicker({
        format: 'll',
        date: new Date(qa_completion_date_value),
    });

    $('#start-time').datetimepicker({
        format: 'LT',
        date: new Date(qa_completion_date_value),
    });
    
});


/**
 * Priority affecting target date field
 *
 * @param  event
 * @return void
 */
$("#priority").on("change", function(){

    var priorityText = $(this).children("option").filter(":selected").text().trim();

    switch(priorityText)
    {
        case "Normal":
            $('input[id="target_date"]').removeAttr("required");
            $('label[for="target-date"]').text("Target Date:");
            $("#target-date").data("DateTimePicker").clear();
            $("#target-date").data("DateTimePicker").disable();
        break;

        case "High":
            $("#target-date").data("DateTimePicker").clear();
            $("#target-date").data("DateTimePicker").enable();
            $('input[id="target_date"]').attr("required", true);
            $('label[for="target-date"]').text("Target Date: *");
        break;

        case "Quick Fix":
            // var date = moment(new Date()).format("ll");
            $('input[id="target_date"]').removeAttr("required");
            $('label[for="target-date"]').text("Target Date:");
            $("#target-date").data("DateTimePicker").date(new Date);
            $("#target-date").data("DateTimePicker").disable();
        break;

        case "Priority 0":
            $('input[id="target_date"]').removeAttr("required");
            $('label[for="target-date"]').text("Target Date:");
            $("#target-date").data("DateTimePicker").date(new Date);
            $("#target-date").data("DateTimePicker").disable();
        break;
    }
});