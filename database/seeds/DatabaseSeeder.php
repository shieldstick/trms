<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(UserGroupsTableSeeder::class);
        $this->call(UserGroupsTwoTableSeeder::class);
        $this->call(AccessLevelsTableSeeder::class);
        $this->call(UserSupervisorsTableSeeder::class);
        $this->call(ClientsTableSeeder::class);
        $this->call(ModulesTableSeeder::class);
        $this->call(ModulesTwoTableSeeder::class);
        $this->call(PrioritiesTableSeeder::class);
        $this->call(StatusesTableSeeder::class);
        $this->call(RequestTypesTableSeeder::class);
        $this->call(RequestTypesTwoTableSeeder::class);
    }
}
