<?php

use Illuminate\Database\Seeder;

class UserSupervisorsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('user_supervisors')->insert([
         	[
         		'supervisor_id'		=> '3',
         		'user_id'			=> '1',
         		'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
         	],
         	[
         		'supervisor_id'		=> '3',
         		'user_id'			=> '2',
         		'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
         	],
         	[
         		'supervisor_id'		=> '3',
         		'user_id'			=> '4',
         		'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
         	],
         	[
         		'supervisor_id'		=> '3',
         		'user_id'			=> '13',
         		'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
         	],
         	[
         		'supervisor_id'		=> '3',
         		'user_id'			=> '14',
         		'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
         	],
         	[
         		'supervisor_id'		=> '8',
         		'user_id'			=> '17',
         		'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
         	],
         	[
         		'supervisor_id'		=> '8',
         		'user_id'			=> '20',
         		'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
         	],
         	[
         		'supervisor_id'		=> '8',
         		'user_id'			=> '21',
         		'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
         	],
         	[
         		'supervisor_id'		=> '8',
         		'user_id'			=> '22',
         		'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
         	],
         	[
         		'supervisor_id'		=> '8',
         		'user_id'			=> '24',
         		'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
         	],
         	[
         		'supervisor_id'		=> '8',
         		'user_id'			=> '27',
         		'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
         	],
         ]);
    }
}
