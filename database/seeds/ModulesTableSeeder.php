<?php

use Illuminate\Database\Seeder;

class ModulesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('modules')->insert([
        	[
                'title'	=> 'FrontEnd',
                'created_at'    => date('Y-m-d H:i:s'),
                'updated_at'    => date('Y-m-d H:i:s'),
            ],
        	[
                'title'	=> 'Sugar',
                'created_at'    => date('Y-m-d H:i:s'),
                'updated_at'    => date('Y-m-d H:i:s'),
            ],
        	[
                'title'	=> 'Flow',
                'created_at'    => date('Y-m-d H:i:s'),
                'updated_at'    => date('Y-m-d H:i:s'),
            ],
        	[
                'title'	=> 'LiveChat',
                'created_at'    => date('Y-m-d H:i:s'),
                'updated_at'    => date('Y-m-d H:i:s'),
            ],
        	[
                'title'	=> 'LiveQuote',
                'created_at'    => date('Y-m-d H:i:s'),
                'updated_at'    => date('Y-m-d H:i:s'),
            ],
            // [
            //     'title'  => 'CMS',
            //     'created_at'    => date('Y-m-d H:i:s'),
            //     'updated_at'    => date('Y-m-d H:i:s'),
            // ],
            // [
            //     'title'  => 'WinSCP',
            //     'created_at'    => date('Y-m-d H:i:s'),
            //     'updated_at'    => date('Y-m-d H:i:s'),
            // ],
		]);
    }
}
