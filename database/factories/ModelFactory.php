<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {

    return [
		'first_name'	=> $faker->firstName,
		'last_name'		=> $faker->lastName,
		'email' 		=> $faker->unique()->safeEmail,
		'password'		=> bcrypt('password123'),
		'access_level'	=> rand(1,4),
		'user_group'	=> rand(1,4),
		'timezone'		=> 'Asia/Singapore',
    ];

});


$factory->define(App\Priority::class, function(Faker\Generator $faker){

	return [
		'title'	=>	$faker->word,
	];
});

$factory->define(App\Ticket::class, function (Faker\Generator $faker) {

	return [
		'subject'			=> $faker->sentence,
		'description'		=> $faker->paragraph,
		'creator_id'		=> 1,
		'requester_id'		=> 3,
		'supervisor_id'		=> 26,
		'developer_id'		=> 5,
		'qa_id'				=> 28,
		'client_id'			=> rand(1,2),
		'module_id'			=> rand(1,5),
		'priority_id'		=> factory('App\Priority')->create(),
		'request_type_id'	=> rand(1,2),
		'status_id'			=> 1,
		'target_date'		=> \Carbon::now(),
	];

});

$factory->define(App\Survey::class, function(Faker\Generator $faker){

	return [
		'ticket_id'	=>	factory('App\Ticket')->create()->id,
		'user_id'	=>	factory('App\User')->create()->id,
		'completed'	=>	false,
	];
});

$factory->define(App\IncidentReport::class, function(Faker\Generator $faker){
	return [
		'subject'			=> $faker->sentence,
		'creator_id'		=> factory('App\User')->create()->id,
		'requester_id'		=> factory('App\User')->create()->id,
		'supervisor_id'		=> factory('App\User')->create()->id,
		'client_id'			=> rand(1,2),
		'module_id'			=> rand(1,2),
		'priority_id'		=> rand(1,2),
		'target_date'		=> \Carbon::now(),
		'incident_date'		=> \Carbon::now(),
		'start_time'		=> \Carbon::now(),
		'portals_affected'	=> $faker->paragraph,
		'details'			=> $faker->paragraph,
		'impact'			=> $faker->paragraph,
		'references'		=> $faker->paragraph,
		'cause'				=> $faker->paragraph,
		'actions'			=> $faker->paragraph,
	];
});