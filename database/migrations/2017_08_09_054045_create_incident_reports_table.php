<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIncidentReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('incident_reports', function (Blueprint $table) {
            $table->increments('id');
            $table->string('subject');
            $table->integer('creator_id')->unsigned();
            $table->integer('requester_id')->unsigned()->nullable();
            $table->integer('supervisor_id')->unsigned()->nullable();
            $table->integer('client_id')->unsigned();
            $table->integer('module_id')->unsigned();
            $table->integer('priority_id')->unsigned();
            $table->timestamp('target_date')->nullable();
            $table->timestamp('incident_date')->nullable();
            $table->time('start_time')->nullable();
            $table->text('portals_affected')->nullable();
            $table->text('details');
            $table->text('impact');
            $table->text('references')->nullable();
            $table->text('cause')->nullable();
            $table->text('actions')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('incident_reports');
    }
}
