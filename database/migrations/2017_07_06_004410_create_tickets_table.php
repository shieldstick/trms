<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tickets', function (Blueprint $table) {
            $table->increments('id');
            $table->string('subject');
            $table->text('description');
            $table->integer('creator_id')->unsigned();
            $table->integer('requester_id')->unsigned()->nullable();
            $table->integer('supervisor_id')->unsigned()->nullable();
            $table->integer('developer_id')->unsigned()->nullable();
            $table->integer('qa_id')->unsigned()->nullable();
            $table->timestamp('dev_start_date')->nullable();
            $table->timestamp('dev_completion_date')->nullable();
            $table->timestamp('qa_start_date')->nullable();
            $table->timestamp('qa_completion_date')->nullable();
            $table->integer('client_id')->unsigned();
            $table->integer('module_id')->unsigned();
            $table->integer('priority_id')->unsigned();
            $table->integer('status_id')->unsigned()->default(1);
            $table->integer('request_type_id')->unsigned();
            $table->timestamp('target_date')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tickets');
    }
}
